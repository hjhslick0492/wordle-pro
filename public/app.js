// localStorage. clear()

// Identifies existing elements for reference:
const board = document.querySelector('board')
const keyboard = document.querySelector('.keyboardContainer')
const rulesButton = document.getElementById('rules')
const statsButton = document.getElementById('stats')
const rundownTurns = document.getElementById('turnsRemaining')
const displayedWords = document.querySelector('.guessedWords')
const gainedPoints = document.querySelector('.wordPoints')
const totalPointContainer = document.querySelector('totalWeeklyContainer')
const days = document.querySelector('.days')
const pointsOfWeek = document.querySelector('.points')

const keyboardKeys = [
    ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
    ['SPACER','A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L','SPACER'],
    ['ENTER', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'BACKSPACE']
]

// Daily answer bank:
const answers = ['MIGHT', 'ERODE', 'CRAZY', 'SNARE', 'AUDIT', 'THEIF']

const allWords = {
    AAHED:false, AALII:false, AARGH:false, AARTI:false, ABACA:false, ABACI:false, ABACK:false, ABACS:false, ABAFT:false, ABAKA:false,
    ABAMP:false, ABAND:false, ABASE:false, ABASH:false, ABASK:false, ABATE:false, ABAYA:false, ABBAS:false, ABBED:false, ABBES:false,
    ABBEY:false, ABBOT:false, ABCEE:false, ABEAM:false, ABEAR:false, ABELE:false, ABERS:false, ABETS:false, ABHOR:false, ABIDE:false,
    ABIES:false, ABLED:false, ABLER:false, ABLES:false, ABLET:false, ABLOW:false, ABMHO:false, ABODE:false, ABOHM:false, ABOIL:false,
    ABOMA:false, ABOON:false, ABORD:false, ABORE:false, ABORT:false, ABOUT:false, ABOVE:false, ABRAM:false, ABRAY:false, ABRIM:false,
    ABRIN:false, ABRIS:false, ABSEY:false, ABSIT:false, ABUNA:false, ABUNE:false, ABUSE:false, ABUTS:false, ABUZZ:false, ABYES:false,
    ABYSM:false, ABYSS:false, ACAIS:false, ACARI:false, ACCAS:false, ACCEL:false, ACCOY:false, ACERB:false, ACERS:false, ACETA:false,
    ACHAR:false, ACHED:false, ACHES:false, ACHOO:false, ACIDS:false, ACIDY:false, ACING:false, ACINI:false, ACKEE:false, ACKER:false,
    ACMES:false, ACMIC:false, ACNED:false, ACNES:false, ACOCK:false, ACOLD:false, ACORN:false, ACRED:false, ACRES:false, ACRID:false,
    ACROS:false, ACTED:false, ACTIN:false, ACTON:false, ACTOR:false, ACUTE:false, ACYLS:false, ADAGE:false, ADAPT:false, ADAWS:false,
    ADAYS:false, ADBOT:false, ADDAX:false, ADDED:false, ADDER:false, ADDIO:false, ADDLE:false, ADEEM:false, ADEPT:false, ADHAN:false,
    ADIEU:false, ADIOS:false, ADITS:false, ADMAN:false, ADMEN:false, ADMIN:false, ADMIT:false, ADMIX:false, ADOBE:false, ADOBO:false,
    ADOPT:false, ADORE:false, ADORN:false, ADOWN:false, ADOZE:false, ADRAD:false, ADRED:false, ADSUM:false, ADUKI:false, ADULT:false,
    ADUNC:false, ADUST:false, ADVEW:false, ADYTA:false, ADZED:false, ADZES:false, AECIA:false, AEDES:false, AEGIS:false, AEONS:false,
    AERIE:false, AEROS:false, AESIR:false, AFALD:false, AFARA:false, AFARS:false, AFEAR:false, AFFIX:false, AFIRE:false, AFLAJ:false,
    AFOOT:false, AFORE:false, AFOUL:false, AFRIT:false, AFROS:false, AFTER:false, AGAIN:false, AGAMA:false, AGAMI:false, AGAPE:false,
    AGARS:false, AGAST:false, AGATE:false, AGAVE:false, AGAZE:false, AGENE:false, AGENT:false, AGERS:false, AGGER:false, AGGIE:false,
    AGGRI:false, AGGRO:false, AGGRY:false, AGHAS:false, AGILA:false, AGILE:false, AGING:false, AGIOS:false, AGISM:false, AGIST:false,
    AGITA:false, AGLEE:false, AGLET:false, AGLEY:false, AGLOO:false, AGLOW:false, AGLUS:false, AGMAS:false, AGOGE:false, AGONE:false,
    AGONS:false, AGONY:false, AGOOD:false, AGORA:false, AGREE:false, AGRIA:false, AGRIN:false, AGROS:false, AGUED:false, AGUES:false,
    AGUNA:false, AGUTI:false, AHEAD:false, AHEAP:false, AHENT:false, AHIGH:false, AHIND:false, AHING:false, AHINT:false, AHOLD:false,
    AHULL:false, AHURU:false, AIDAS:false, AIDED:false, AIDER:false, AIDES:false, AIDOI:false, AIDOS:false, AIERY:false, AIGAS:false,
    AIGHT:false, AILED:false, AIMED:false, AIMER:false, AINEE:false, AINGA:false, AIOLI:false, AIRED:false, AIRER:false, AIRNS:false,
    AIRTH:false, AIRTS:false, AISLE:false, AITCH:false, AITUS:false, AIVER:false, AIYEE:false, AIZLE:false, AJIES:false, AJIVA:false,
    AJUGA:false, AJWAN:false, AKEES:false, AKELA:false, AKENE:false, AKING:false, AKITA:false, AKKAS:false, ALAAP:false, ALACK:false,
    ALAMO:false, ALAND:false, ALANE:false, ALANG:false, ALANS:false, ALANT:false, ALAPA:false, ALAPS:false, ALARM:false, ALARY:false,
    ALATE:false, ALAYS:false, ALBAS:false, ALBEE:false, ALBUM:false, ALCID:false, ALCOS:false, ALDEA:false, ALDER:false, ALDOL:false,
    ALECK:false, ALECS:false, ALEFS:false, ALEFT:false, ALEPH:false, ALERT:false, ALEWS:false, ALEYE:false, ALFAS:false, ALGAE:false,
    ALGAL:false, ALGAS:false, ALGID:false, ALGIN:false, ALGOR:false, ALGUM:false, ALIAS:false, ALIBI:false, ALIEN:false, ALIFS:false,
    ALIGN:false, ALIKE:false, ALINE:false, ALIST:false, ALIVE:false, ALIYA:false, ALKIE:false, ALKOS:false, ALKYD:false, ALKYL:false,
    ALLAY:false, ALLEE:false, ALLEL:false, ALLEY:false, ALLIS:false, ALLOD:false, ALLOT:false, ALLOW:false, ALLOY:false, ALLYL:false,
    ALMAH:false, ALMAS:false, ALMEH:false, ALMES:false, ALMUD:false, ALMUG:false, ALODS:false, ALOED:false, ALOES:false, ALOFT:false,
    ALOHA:false, ALOIN:false, ALONE:false, ALONG:false, ALOOF:false, ALOOS:false, ALOUD:false, ALOWE:false, ALPHA:false, ALTAR:false,
    ALTER:false, ALTHO:false, ALTOS:false, ALULA:false, ALUMS:false, ALURE:false, ALVAR:false, ALWAY:false, AMAHS:false, AMAIN:false,
    AMASS:false, AMATE:false, AMAUT:false, AMAZE:false, AMBAN:false, AMBER:false, AMBIT:false, AMBLE:false, AMBOS:false, AMBRY:false,
    AMEBA:false, AMEER:false, AMEND:false, AMENE:false, AMENS:false, AMENT:false, AMIAS:false, AMICE:false, AMICI:false, AMIDE:false,
    AMIDO:false, AMIDS:false, AMIES:false, AMIGA:false, AMIGO:false, AMINE:false, AMINO:false, AMINS:false, AMIRS:false, AMISS:false,
    AMITY:false, AMLAS:false, AMMAN:false, AMMON:false, AMMOS:false, AMNIA:false, AMNIC:false, AMNIO:false, AMOKS:false, AMOLE:false,
    AMONG:false, AMORT:false, AMOUR:false, AMOVE:false, AMOWT:false, AMPED:false, AMPLE:false, AMPLY:false, AMPUL:false, AMRIT:false,
    AMUCK:false, AMUSE:false, AMYLS:false, ANANA:false, ANATA:false, ANCHO:false, ANCLE:false, ANCON:false, ANDRO:false, ANEAR:false,
    ANELE:false, ANENT:false, ANGAS:false, ANGEL:false, ANGER:false, ANGLE:false, ANGLO:false, ANGRY:false, ANGST:false, ANIGH:false,
    ANILE:false, ANILS:false, ANIMA:false, ANIME:false, ANIMI:false, ANION:false, ANISE:false, ANKER:false, ANKHS:false, ANKLE:false,
    ANKUS:false, ANLAS:false, ANNAL:false, ANNAS:false, ANNAT:false, ANNEX:false, ANNOY:false, ANNUL:false, ANOAS:false, ANODE:false,
    ANOLE:false, ANOMY:false, ANSAE:false, ANTAE:false, ANTAR:false, ANTAS:false, ANTED:false, ANTES:false, ANTIC:false, ANTIS:false,
    ANTRA:false, ANTRE:false, ANTSY:false, ANURA:false, ANVIL:false, ANYON:false, AORTA:false, APACE:false, APAGE:false, APAID:false,
    APART:false, APAYD:false, APAYS:false, APEAK:false, APEEK:false, APERS:false, APERT:false, APERY:false, APGAR:false, APHID:false,
    APHIS:false, APIAN:false, APING:false, APIOL:false, APISH:false, APISM:false, APNEA:false, APODE:false, APODS:false, APOOP:false,
    APORT:false, APPAL:false, APPAY:false, APPEL:false, APPLE:false, APPLY:false, APPRO:false, APPUI:false, APPUY:false, APRES:false,
    APRON:false, APSES:false, APSIS:false, APSOS:false, APTED:false, APTER:false, APTLY:false, AQUAE:false, AQUAS:false, ARABA:false,
    ARAKS:false, ARAME:false, ARARS:false, ARBAS:false, ARBOR:false, ARCED:false, ARCHI:false, ARCOS:false, ARCUS:false, ARDEB:false,
    ARDOR:false, ARDRI:false, AREAD:false, AREAE:false, AREAL:false, AREAR:false, AREAS:false, ARECA:false, AREDD:false, AREDE:false,
    AREFY:false, AREIC:false, ARENA:false, ARENE:false, AREPA:false, ARERE:false, ARETE:false, ARETS:false, ARETT:false, ARGAL:false,
    ARGAN:false, ARGIL:false, ARGLE:false, ARGOL:false, ARGON:false, ARGOT:false, ARGUE:false, ARGUS:false, ARHAT:false, ARIAS:false,
    ARIEL:false, ARIKI:false, ARILS:false, ARIOT:false, ARISE:false, ARISH:false, ARKED:false, ARLED:false, ARLES:false, ARMED:false,
    ARMER:false, ARMET:false, ARMIL:false, ARMOR:false, ARNAS:false, ARNUT:false, AROBA:false, AROHA:false, AROID:false, AROMA:false,
    AROSE:false, ARPAS:false, ARPEN:false, ARRAH:false, ARRAS:false, ARRAY:false, ARRET:false, ARRIS:false, ARROW:false, ARROZ:false,
    ARSED:false, ARSES:false, ARSEY:false, ARSIS:false, ARSON:false, ARTAL:false, ARTEL:false, ARTIC:false, ARTIS:false, ARTSY:false,
    ARUHE:false, ARUMS:false, ARVAL:false, ARVEE:false, ARVOS:false, ARYLS:false, ASANA:false, ASCON:false, ASCOT:false, ASCUS:false,
    ASDIC:false, ASHED:false, ASHEN:false, ASHES:false, ASHET:false, ASIDE:false, ASKED:false, ASKER:false, ASKEW:false, ASKOI:false,
    ASKOS:false, ASPEN:false, ASPER:false, ASPIC:false, ASPIE:false, ASPIS:false, ASPRO:false, ASSAI:false, ASSAM:false, ASSAY:false,
    ASSES:false, ASSET:false, ASSEZ:false, ASSOT:false, ASTER:false, ASTIR:false, ASTUN:false, ASURA:false, ASWAY:false, ASWIM:false,
    ASYLA:false, ATAPS:false, ATAXY:false, ATIGI:false, ATILT:false, ATIMY:false, ATLAS:false, ATMAN:false, ATMAS:false, ATMOS:false,
    ATOCS:false, ATOKE:false, ATOKS:false, ATOLL:false, ATOMS:false, ATOMY:false, ATONE:false, ATONY:false, ATOPY:false, ATRIA:false,
    ATRIP:false, ATTAP:false, ATTAR:false, ATTIC:false, ATUAS:false, AUDAD:false, AUDIO:false, AUDIT:false, AUGER:false, AUGHT:false,
    AUGUR:false, AULAS:false, AULIC:false, AULOI:false, AULOS:false, AUMIL:false, AUNES:false, AUNTS:false, AUNTY:false, AURAE:false,
    AURAL:false, AURAR:false, AURAS:false, AUREI:false, AURES:false, AURIC:false, AURIS:false, AURUM:false, AUTOS:false, AUXIN:false,
    AVAIL:false, AVALE:false, AVANT:false, AVAST:false, AVELS:false, AVENS:false, AVERS:false, AVERT:false, AVGAS:false, AVIAN:false,
    AVINE:false, AVION:false, AVISE:false, AVISO:false, AVIZE:false, AVOID:false, AVOWS:false, AVYZE:false, AWAIT:false, AWAKE:false,
    AWARD:false, AWARE:false, AWARN:false, AWASH:false, AWATO:false, AWAVE:false, AWAYS:false, AWDLS:false, AWEEL:false, AWETO:false,
    AWFUL:false, AWING:false, AWMRY:false, AWNED:false, AWNER:false, AWOKE:false, AWOLS:false, AWORK:false, AXELS:false, AXIAL:false,
    AXILE:false, AXILS:false, AXING:false, AXIOM:false, AXION:false, AXITE:false, AXLED:false, AXLES:false, AXMAN:false, AXMEN:false,
    AXOID:false, AXONE:false, AXONS:false, AYAHS:false, AYAYA:false, AYELP:false, AYGRE:false, AYINS:false, AYONT:false, AYRES:false,
    AYRIE:false, AZANS:false, AZIDE:false, AZIDO:false, AZINE:false, AZLON:false, AZOIC:false, AZOLE:false, AZONS:false, AZOTE:false,
    AZOTH:false, AZUKI:false, AZURE:false, AZURN:false, AZURY:false, AZYGY:false, AZYME:false, AZYMS:false, BAAED:false, BAALS:false,
    BABAS:false, BABEL:false, BABES:false, BABKA:false, BABOO:false, BABUL:false, BABUS:false, BACCA:false, BACCO:false, BACCY:false,
    BACHA:false, BACHS:false, BACKS:false, BACON:false, BADDY:false, BADGE:false, BADLY:false, BAELS:false, BAFFS:false, BAFFY:false,
    BAFTS:false, BAGEL:false, BAGGY:false, BAGHS:false, BAGIE:false, BAHTS:false, BAHUS:false, BAHUT:false, BAILS:false, BAIRN:false,
    BAISA:false, BAITH:false, BAITS:false, BAIZA:false, BAIZE:false, BAJAN:false, BAJRA:false, BAJRI:false, BAJUS:false, BAKED:false,
    BAKEN:false, BAKER:false, BAKES:false, BAKRA:false, BALAS:false, BALDS:false, BALDY:false, BALED:false, BALER:false, BALES:false,
    BALKS:false, BALKY:false, BALLS:false, BALLY:false, BALMS:false, BALMY:false, BALOO:false, BALSA:false, BALTI:false, BALUN:false,
    BALUS:false, BAMBI:false, BANAK:false, BANAL:false, BANCO:false, BANCS:false, BANDA:false, BANDH:false, BANDS:false, BANDY:false,
    BANED:false, BANES:false, BANGS:false, BANIA:false, BANJO:false, BANKS:false, BANNS:false, BANTS:false, BANTU:false, BANTY:false,
    BANYA:false, BAPUS:false, BARBE:false, BARBS:false, BARBY:false, BARCA:false, BARDE:false, BARDO:false, BARDS:false, BARDY:false,
    BARED:false, BARER:false, BARES:false, BARFI:false, BARFS:false, BARGE:false, BARIC:false, BARKS:false, BARKY:false, BARMS:false,
    BARMY:false, BARNS:false, BARNY:false, BARON:false, BARPS:false, BARRA:false, BARRE:false, BARRO:false, BARRY:false, BARYE:false,
    BASAL:false, BASAN:false, BASED:false, BASEN:false, BASER:false, BASES:false, BASHO:false, BASIC:false, BASIJ:false, BASIL:false,
    BASIN:false, BASIS:false, BASKS:false, BASON:false, BASSE:false, BASSI:false, BASSO:false, BASSY:false, BASTA:false, BASTE:false,
    BASTI:false, BASTO:false, BASTS:false, BATCH:false, BATED:false, BATES:false, BATHE:false, BATHS:false, BATIK:false, BATON:false,
    BATTA:false, BATTS:false, BATTU:false, BATTY:false, BAUDS:false, BAUKS:false, BAULK:false, BAURS:false, BAVIN:false, BAWDS:false,
    BAWDY:false, BAWKS:false, BAWLS:false, BAWNS:false, BAWRS:false, BAWTY:false, BAYED:false, BAYER:false, BAYES:false, BAYLE:false,
    BAYOU:false, BAYTS:false, BAZAR:false, BAZOO:false, BEACH:false, BEADS:false, BEADY:false, BEAKS:false, BEAKY:false, BEALS:false,
    BEAMS:false, BEAMY:false, BEANO:false, BEANS:false, BEANY:false, BEARD:false, BEARE:false, BEARS:false, BEAST:false, BEATH:false,
    BEATS:false, BEATY:false, BEAUS:false, BEAUT:false, BEAUX:false, BEBOP:false, BECAP:false, BECKE:false, BECKS:false, BEDAD:false,
    BEDEL:false, BEDES:false, BEDEW:false, BEDIM:false, BEDYE:false, BEECH:false, BEEDI:false, BEEFS:false, BEEFY:false, BEEPS:false,
    BEERS:false, BEERY:false, BEETS:false, BEFIT:false, BEFOG:false, BEGAD:false, BEGAN:false, BEGAR:false, BEGAT:false, BEGEM:false,
    BEGET:false, BEGIN:false, BEGOT:false, BEGUM:false, BEGUN:false, BEIGE:false, BEIGY:false, BEING:false, BEINS:false, BEKAH:false,
    BELAH:false, BELAR:false, BELAY:false, BELCH:false, BELEE:false, BELGA:false, BELIE:false, BELLE:false, BELLS:false, BELLY:false,
    BELON:false, BELOW:false, BELTS:false, BEMAD:false, BEMAS:false, BEMIX:false, BEMUD:false, BENCH:false, BENDS:false, BENDY:false,
    BENES:false, BENET:false, BENGA:false, BENIS:false, BENNE:false, BENNI:false, BENNY:false, BENTO:false, BENTS:false, BENTY:false,
    BEPAT:false, BERAY:false, BERES:false, BERET:false, BERGS:false, BERKO:false, BERKS:false, BERME:false, BERMS:false, BEROB:false,
    BERRY:false, BERTH:false, BERYL:false, BESAT:false, BESAW:false, BESEE:false, BESES:false, BESET:false, BESIT:false, BESOM:false,
    BESOT:false, BESTI:false, BESTS:false, BETAS:false, BETED:false, BETEL:false, BETES:false, BETHS:false, BETID:false, BETON:false,
    BETTA:false, BETTY:false, BEVEL:false, BEVER:false, BEVOR:false, BEVUE:false, BEVVY:false, BEWET:false, BEWIG:false, BEZEL:false,
    BEZES:false, BEZIL:false, BEZZY:false, BHAIS:false, BHAJI:false, BHANG:false, BHATS:false, BHELS:false, BHOOT:false, BHUNA:false,
    BHUTS:false, BIACH:false, BIALI:false, BIALY:false, BIBBS:false, BIBES:false, BIBLE:false, BICCY:false, BICEP:false, BICES:false,
    BIDDY:false, BIDED:false, BIDER:false, BIDES:false, BIDET:false, BIDIS:false, BIDON:false, BIELD:false, BIERS:false, BIFFO:false,
    BIFFS:false, BIFFY:false, BIFID:false, BIGAE:false, BIGGS:false, BIGGY:false, BIGHA:false, BIGHT:false, BIGLY:false, BIGOS:false,
    BIGOT:false, BIJOU:false, BIKED:false, BIKER:false, BIKES:false, BIKIE:false, BILBO:false, BILBY:false, BILED:false, BILES:false,
    BILGE:false, BILGY:false, BILKS:false, BILLS:false, BILLY:false, BIMAH:false, BIMAS:false, BIMBO:false, BINAL:false, BINDI:false,
    BINDS:false, BINER:false, BINES:false, BINGE:false, BINGO:false, BINGS:false, BINGY:false, BINIT:false, BINKS:false, BINTS:false,
    BIOGS:false, BIOME:false, BIONT:false, BIOTA:false, BIPED:false, BIPOD:false, BIRCH:false, BIRDS:false, BIRKS:false, BIRLE:false,
    BIRLS:false, BIROS:false, BIRRS:false, BIRSE:false, BIRSY:false, BIRTH:false, BISES:false, BISKS:false, BISOM:false, BISON:false,
    BITCH:false, BITER:false, BITES:false, BITOS:false, BITOU:false, BITSY:false, BITTE:false, BITTS:false, BITTY:false, BIVIA:false,
    BIVVY:false, BIZES:false, BIZZO:false, BIZZY:false, BLABS:false, BLACK:false, BLADE:false, BLADS:false, BLADY:false, BLAER:false,
    BLAES:false, BLAFF:false, BLAGS:false, BLAHS:false, BLAIN:false, BLAME:false, BLAMS:false, BLAND:false, BLANK:false, BLARE:false,
    BLART:false, BLASE:false, BLASH:false, BLAST:false, BLATE:false, BLATS:false, BLATT:false, BLAUD:false, BLAWN:false, BLAWS:false,
    BLAYS:false, BLAZE:false, BLEAK:false, BLEAR:false, BLEAT:false, BLEBS:false, BLECH:false, BLEED:false, BLEEP:false, BLEES:false,
    BLEND:false, BLENT:false, BLERT:false, BLESS:false, BLEST:false, BLETS:false, BLEYS:false, BLIMP:false, BLIMY:false, BLIND:false,
    BLING:false, BLINI:false, BLINK:false, BLINS:false, BLINY:false, BLIPS:false, BLISS:false, BLIST:false, BLITE:false, BLITS:false,
    BLITZ:false, BLIVE:false, BLOAT:false, BLOBS:false, BLOCK:false, BLOCS:false, BLOGS:false, BLOKE:false, BLOND:false, BLOOD:false,
    BLOOK:false, BLOOM:false, BLOOP:false, BLORE:false, BLOTS:false, BLOWN:false, BLOWS:false, BLOWY:false, BLUBS:false, BLUDE:false,
    BLUDS:false, BLUDY:false, BLUED:false, BLUER:false, BLUES:false, BLUET:false, BLUEY:false, BLUFF:false, BLUID:false, BLUME:false,
    BLUNK:false, BLUNT:false, BLURB:false, BLURS:false, BLURT:false, BLUSH:false, BLYPE:false, BOABS:false, BOAKS:false, BOARD:false,
    BOARS:false, BOART:false, BOAST:false, BOATS:false, BOBAC:false, BOBAK:false, BOBAS:false, BOBBY:false, BOBOL:false, BOBOS:false,
    BOCCA:false, BOCCE:false, BOCCI:false, BOCHE:false, BOCKS:false, BODED:false, BODES:false, BODGE:false, BODHI:false, BODLE:false,
    BOEPS:false, BOETS:false, BOEUF:false, BOFFO:false, BOFFS:false, BOGAN:false, BOGEY:false, BOGGY:false, BOGIE:false, BOGLE:false,
    BOGUE:false, BOGUS:false, BOHEA:false, BOHOS:false, BOILS:false, BOING:false, BOINK:false, BOITE:false, BOKED:false, BOKEH:false,
    BOKES:false, BOKOS:false, BOLAR:false, BOLAS:false, BOLDS:false, BOLES:false, BOLIX:false, BOLLS:false, BOLOS:false, BOLTS:false,
    BOLUS:false, BOMAS:false, BOMBE:false, BOMBO:false, BOMBS:false, BONCE:false, BONDS:false, BONED:false, BONER:false, BONES:false,
    BONEY:false, BONGO:false, BONGS:false, BONIE:false, BONKS:false, BONNE:false, BONNY:false, BONUS:false, BONZA:false, BONZE:false,
    BOOAI:false, BOOAY:false, BOOBS:false, BOOBY:false, BOODY:false, BOOED:false, BOOFY:false, BOOGY:false, BOOHS:false, BOOKS:false,
    BOOKY:false, BOOLS:false, BOOMS:false, BOOMY:false, BOONG:false, BOONS:false, BOORD:false, BOORS:false, BOOSE:false, BOOST:false,
    BOOTH:false, BOOTS:false, BOOTY:false, BOOZE:false, BOOZY:false, BOPPY:false, BORAK:false, BORAL:false, BORAS:false, BORAX:false,
    BORDE:false, BORDS:false, BORED:false, BOREE:false, BOREL:false, BORER:false, BORES:false, BORGO:false, BORIC:false, BORKS:false,
    BORMS:false, BORNA:false, BORNE:false, BORON:false, BORTS:false, BORTY:false, BORTZ:false, BOSIE:false, BOSKS:false, BOSKY:false,
    BOSOM:false, BOSON:false, BOSSY:false, BOSUN:false, BOTAS:false, BOTCH:false, BOTEL:false, BOTES:false, BOTHY:false, BOTTE:false,
    BOTTS:false, BOTTY:false, BOUGE:false, BOUGH:false, BOUKS:false, BOULE:false, BOULT:false, BOUND:false, BOUNS:false, BOURD:false,
    BOURG:false, BOURN:false, BOUSE:false, BOUSY:false, BOUTS:false, BOVID:false, BOWAT:false, BOWED:false, BOWEL:false, BOWER:false,
    BOWES:false, BOWET:false, BOWIE:false, BOWLS:false, BOWNE:false, BOWRS:false, BOWSE:false, BOXED:false, BOXEN:false, BOXER:false,
    BOXES:false, BOXLA:false, BOXTY:false, BOYAR:false, BOYAU:false, BOYED:false, BOYFS:false, BOYGS:false, BOYLA:false, BOYOS:false,
    BOYSY:false, BOZOS:false, BRAAI:false, BRACE:false, BRACH:false, BRACK:false, BRACT:false, BRADS:false, BRAES:false, BRAGS:false,
    BRAID:false, BRAIL:false, BRAIN:false, BRAKE:false, BRAKS:false, BRAKY:false, BRAME:false, BRAND:false, BRANE:false, BRANK:false,
    BRANS:false, BRANT:false, BRASH:false, BRASS:false, BRAST:false, BRATS:false, BRAVA:false, BRAVE:false, BRAVI:false, BRAVO:false,
    BRAWL:false, BRAWN:false, BRAWS:false, BRAXY:false, BRAYS:false, BRAZA:false, BRAZE:false, BREAD:false, BREAK:false, BREAM:false,
    BREDE:false, BREDS:false, BREED:false, BREEM:false, BREER:false, BREES:false, BREID:false, BREIS:false, BREME:false, BRENS:false,
    BRENT:false, BRERE:false, BRERS:false, BREVE:false, BREWS:false, BREYS:false, BRIAR:false, BRIBE:false, BRICK:false, BRIDE:false,
    BRIEF:false, BRIER:false, BRIES:false, BRIGS:false, BRIKI:false, BRIKS:false, BRILL:false, BRIMS:false, BRINE:false, BRING:false,
    BRINK:false, BRINS:false, BRINY:false, BRIOS:false, BRISE:false, BRISK:false, BRISS:false, BRITH:false, BRITS:false, BRITT:false,
    BRIZE:false, BROAD:false, BROCH:false, BROCK:false, BRODS:false, BROGH:false, BROGS:false, BROIL:false, BROKE:false, BROME:false,
    BROMO:false, BRONC:false, BROND:false, BROOD:false, BROOK:false, BROOL:false, BROOM:false, BROOS:false, BROSE:false, BROSY:false,
    BROTH:false, BROWN:false, BROWS:false, BRUGH:false, BRUIN:false, BRUIT:false, BRULE:false, BRUME:false, BRUNG:false, BRUNT:false,
    BRUSH:false, BRUSK:false, BRUST:false, BRUTE:false, BRUTS:false, BUATS:false, BUAZE:false, BUBAL:false, BUBAS:false, BUBBA:false,
    BUBBE:false, BUBBY:false, BUBUS:false, BUCHU:false, BUCKO:false, BUCKS:false, BUCKU:false, BUDAS:false, BUDDY:false, BUDGE:false,
    BUDIS:false, BUDOS:false, BUFFA:false, BUFFE:false, BUFFI:false, BUFFO:false, BUFFS:false, BUFFY:false, BUFOS:false, BUFTY:false,
    BUGGY:false, BUGLE:false, BUHLS:false, BUHRS:false, BUIKS:false, BUILD:false, BUILT:false, BUIST:false, BUKES:false, BULBS:false,
    BULGE:false, BULGY:false, BULKS:false, BULKY:false, BULLA:false, BULLS:false, BULLY:false, BULSE:false, BUMBO:false, BUMFS:false,
    BUMPH:false, BUMPS:false, BUMPY:false, BUNAS:false, BUNCE:false, BUNCH:false, BUNCO:false, BUNDE:false, BUNDH:false, BUNDS:false,
    BUNDT:false, BUNDU:false, BUNDY:false, BUNGS:false, BUNGY:false, BUNIA:false, BUNJE:false, BUNJY:false, BUNKO:false, BUNKS:false,
    BUNNS:false, BUNNY:false, BUNTS:false, BUNTY:false, BUNYA:false, BUOYS:false, BUPPY:false, BURAN:false, BURAS:false, BURBS:false,
    BURDS:false, BURET:false, BURFI:false, BURGH:false, BURGS:false, BURIN:false, BURKA:false, BURKE:false, BURKS:false, BURLS:false,
    BURLY:false, BURNS:false, BURNT:false, BUROO:false, BURPS:false, BURQA:false, BURRO:false, BURRS:false, BURRY:false, BURSA:false,
    BURSE:false, BURST:false, BUSBY:false, BUSED:false, BUSES:false, BUSHY:false, BUSKS:false, BUSKY:false, BUSSU:false, BUSTI:false,
    BUSTS:false, BUSTY:false, BUTCH:false, BUTEO:false, BUTES:false, BUTLE:false, BUTOH:false, BUTTE:false, BUTTS:false, BUTTY:false,
    BUTUT:false, BUTYL:false, BUXOM:false, BUYER:false, BUZZY:false, BWANA:false, BWAZI:false, BYDED:false, BYDES:false, BYKED:false,
    BYKES:false, BYLAW:false, BYRES:false, BYRLS:false, BYSSI:false, BYTES:false, BYWAY:false, CAAED:false, CABAL:false, CABAS:false,
    CABBY:false, CABER:false, CABIN:false, CABLE:false, CABOB:false, CABOC:false, CABRE:false, CACAO:false, CACAS:false, CACHE:false,
    CACKS:false, CACKY:false, CACTI:false, CADDY:false, CADEE:false, CADES:false, CADET:false, CADGE:false, CADGY:false, CADIE:false,
    CADIS:false, CADRE:false, CAECA:false, CAESE:false, CAFES:false, CAFFS:false, CAGED:false, CAGER:false, CAGES:false, CAGEY:false,
    CAGOT:false, CAHOW:false, CAIDS:false, CAINS:false, CAIRD:false, CAIRN:false, CAJON:false, CAJUN:false, CAKED:false, CAKES:false,
    CAKEY:false, CALFS:false, CALID:false, CALIF:false, CALIX:false, CALKS:false, CALLA:false, CALLS:false, CALMS:false, CALMY:false,
    CALOS:false, CALPA:false, CALPS:false, CALVE:false, CALYX:false, CAMAN:false, CAMAS:false, CAMEL:false, CAMEO:false, CAMES:false,
    CAMIS:false, CAMOS:false, CAMPI:false, CAMPO:false, CAMPS:false, CAMPY:false, CAMUS:false, CANAL:false, CANDY:false, CANED:false,
    CANEH:false, CANER:false, CANES:false, CANGS:false, CANID:false, CANNA:false, CANNS:false, CANNY:false, CANOE:false, CANON:false,
    CANSO:false, CANST:false, CANTO:false, CANTS:false, CANTY:false, CAPAS:false, CAPED:false, CAPER:false, CAPES:false, CAPEX:false,
    CAPHS:false, CAPIZ:false, CAPLE:false, CAPON:false, CAPOS:false, CAPOT:false, CAPRI:false, CAPUL:false, CAPUT:false, CARAP:false,
    CARAT:false, CARBO:false, CARBS:false, CARBY:false, CARDI:false, CARDS:false, CARDY:false, CARED:false, CARER:false, CARES:false,
    CARET:false, CAREX:false, CARGO:false, CARKS:false, CARLE:false, CARLS:false, CARNS:false, CARNY:false, CAROB:false, CAROL:false,
    CAROM:false, CARON:false, CARPI:false, CARPS:false, CARRS:false, CARRY:false, CARSE:false, CARTA:false, CARTE:false, CARTS:false,
    CARVE:false, CARVY:false, CASAS:false, CASCO:false, CASED:false, CASES:false, CASKS:false, CASKY:false, CASTE:false, CASTS:false,
    CASUS:false, CATCH:false, CATER:false, CATES:false, CATTY:false, CAUDA:false, CAUKS:false, CAULD:false, CAULK:false, CAULS:false,
    CAUMS:false, CAUPS:false, CAURI:false, CAUSA:false, CAUSE:false, CAVAS:false, CAVED:false, CAVEL:false, CAVER:false, CAVES:false,
    CAVIE:false, CAVIL:false, CAWED:false, CAWKS:false, CAXON:false, CEASE:false, CEAZE:false, CEBID:false, CECAL:false, CECUM:false,
    CEDAR:false, CEDED:false, CEDER:false, CEDES:false, CEDIS:false, CEIBA:false, CEILI:false, CEILS:false, CELEB:false, CELLA:false,
    CELLI:false, CELLO:false, CELLS:false, CELOM:false, CELTS:false, CENSE:false, CENTO:false, CENTS:false, CENTU:false, CEORL:false,
    CEPES:false, CERCI:false, CERED:false, CERES:false, CERGE:false, CERIA:false, CERIC:false, CERNE:false, CEROC:false, CEROS:false,
    CERTS:false, CERTY:false, CESSE:false, CESTA:false, CESTI:false, CETES:false, CETYL:false, CEZVE:false, CHACE:false, CHACK:false,
    CHACO:false, CHADO:false, CHADS:false, CHAFE:false, CHAFF:false, CHAFT:false, CHAIN:false, CHAIR:false, CHAIS:false, CHALK:false,
    CHALS:false, CHAMP:false, CHAMS:false, CHANA:false, CHANG:false, CHANK:false, CHANT:false, CHAOS:false, CHAPE:false, CHAPS:false,
    CHAPT:false, CHARA:false, CHARD:false, CHARE:false, CHARK:false, CHARM:false, CHARR:false, CHARS:false, CHART:false, CHARY:false,
    CHASE:false, CHASM:false, CHATS:false, CHAVE:false, CHAVS:false, CHAWK:false, CHAWS:false, CHAYA:false, CHAYS:false, CHEAP:false,
    CHEAT:false, CHECK:false, CHEEK:false, CHEEP:false, CHEER:false, CHEFS:false, CHEKA:false, CHELA:false, CHELP:false, CHEMO:false,
    CHEMS:false, CHERE:false, CHERT:false, CHESS:false, CHEST:false, CHETH:false, CHEVY:false, CHEWS:false, CHEWY:false, CHIAO:false,
    CHIAS:false, CHIBS:false, CHICA:false, CHICH:false, CHICK:false, CHICO:false, CHICS:false, CHIDE:false, CHIEF:false, CHIEL:false,
    CHIKS:false, CHILD:false, CHILE:false, CHILI:false, CHILL:false, CHIMB:false, CHIME:false, CHIMO:false, CHIMP:false, CHINA:false,
    CHINE:false, CHING:false, CHINK:false, CHINO:false, CHINS:false, CHIPS:false, CHIRK:false, CHIRL:false, CHIRM:false, CHIRO:false,
    CHIRP:false, CHIRR:false, CHIRT:false, CHIRU:false, CHITS:false, CHIVE:false, CHIVS:false, CHIVY:false, CHIZZ:false, CHOCK:false,
    CHOCO:false, CHOCS:false, CHODE:false, CHOGS:false, CHOIL:false, CHOIR:false, CHOKE:false, CHOKO:false, CHOKY:false, CHOLA:false,
    CHOLI:false, CHOLO:false, CHOMP:false, CHONS:false, CHOOF:false, CHOOK:false, CHOOM:false, CHOON:false, CHOPS:false, CHORD:false,
    CHORE:false, CHOSE:false, CHOTA:false, CHOTT:false, CHOUT:false, CHOUX:false, CHOWK:false, CHOWS:false, CHUBS:false, CHUCK:false,
    CHUFA:false, CHUFF:false, CHUGS:false, CHUMP:false, CHUMS:false, CHUNK:false, CHURL:false, CHURN:false, CHURR:false, CHUSE:false,
    CHUTE:false, CHUTS:false, CHYLE:false, CHYME:false, CHYND:false, CIBOL:false, CIDED:false, CIDER:false, CIDES:false, CIELS:false,
    CIGAR:false, CIGGY:false, CILIA:false, CILLS:false, CIMAR:false, CIMEX:false, CINCH:false, CINCT:false, CINES:false, CINQS:false,
    CIONS:false, CIPPI:false, CIRCA:false, CIRCS:false, CIRES:false, CIRLS:false, CIRRI:false, CISCO:false, CISSY:false, CISTS:false,
    CITAL:false, CITED:false, CITER:false, CITES:false, CIVES:false, CIVET:false, CIVIC:false, CIVIE:false, CIVIL:false, CIVVY:false,
    CLACH:false, CLACK:false, CLADE:false, CLADS:false, CLAES:false, CLAGS:false, CLAIM:false, CLAME:false, CLAMP:false, CLAMS:false,
    CLANG:false, CLANK:false, CLANS:false, CLAPS:false, CLAPT:false, CLARO:false, CLART:false, CLARY:false, CLASH:false, CLASP:false,
    CLASS:false, CLAST:false, CLATS:false, CLAUT:false, CLAVE:false, CLAVI:false, CLAWS:false, CLAYS:false, CLEAN:false, CLEAR:false,
    CLEAT:false, CLECK:false, CLEEK:false, CLEEP:false, CLEFS:false, CLEFT:false, CLEGS:false, CLEIK:false, CLEMS:false, CLEPE:false,
    CLEPT:false, CLERK:false, CLEVE:false, CLEWS:false, CLICK:false, CLIED:false, CLIES:false, CLIFF:false, CLIFT:false, CLIMB:false,
    CLIME:false, CLINE:false, CLING:false, CLINK:false, CLINT:false, CLIPE:false, CLIPS:false, CLIPT:false, CLITS:false, CLOAK:false,
    CLOAM:false, CLOCK:false, CLODS:false, CLOFF:false, CLOGS:false, CLOKE:false, CLOMB:false, CLOMP:false, CLONE:false, CLONK:false,
    CLONS:false, CLOOP:false, CLOOT:false, CLOPS:false, CLOSE:false, CLOTE:false, CLOTH:false, CLOTS:false, CLOUD:false, CLOUR:false,
    CLOUS:false, CLOUT:false, CLOVE:false, CLOWN:false, CLOWS:false, CLOYE:false, CLOYS:false, CLOZE:false, CLUBS:false, CLUCK:false,
    CLUED:false, CLUES:false, CLUEY:false, CLUMP:false, CLUNG:false, CLUNK:false, CLYPE:false, CNIDA:false, COACH:false, COACT:false,
    COADY:false, COALA:false, COALS:false, COALY:false, COAPT:false, COARB:false, COAST:false, COATE:false, COATI:false, COATS:false,
    COBBS:false, COBBY:false, COBIA:false, COBLE:false, COBRA:false, COBZA:false, COCAS:false, COCCI:false, COCCO:false, COCKS:false,
    COCKY:false, COCOA:false, COCOS:false, CODAS:false, CODEC:false, CODED:false, CODEN:false, CODER:false, CODES:false, CODEX:false,
    CODON:false, COEDS:false, COFFS:false, COGIE:false, COGON:false, COGUE:false, COHAB:false, COHEN:false, COHOE:false, COHOG:false,
    COHOS:false, COIFS:false, COIGN:false, COILS:false, COINS:false, COIRS:false, COITS:false, COKED:false, COKES:false, COLAS:false,
    COLBY:false, COLDS:false, COLED:false, COLES:false, COLEY:false, COLIC:false, COLIN:false, COLLS:false, COLLY:false, COLOG:false,
    COLON:false, COLOR:false, COLTS:false, COLZA:false, COMAE:false, COMAL:false, COMAS:false, COMBE:false, COMBI:false, COMBO:false,
    COMBS:false, COMBY:false, COMER:false, COMES:false, COMET:false, COMFY:false, COMIC:false, COMIX:false, COMMA:false, COMMO:false,
    COMMS:false, COMMY:false, COMPO:false, COMPS:false, COMPT:false, COMTE:false, COMUS:false, CONCH:false, CONDO:false, CONED:false,
    CONES:false, CONEY:false, CONFS:false, CONGA:false, CONGE:false, CONGO:false, CONIA:false, CONIC:false, CONIN:false, CONKS:false,
    CONKY:false, CONNE:false, CONNS:false, CONTE:false, CONTO:false, CONUS:false, CONVO:false, COOCH:false, COOED:false, COOEE:false,
    COOER:false, COOEY:false, COOFS:false, COOKS:false, COOKY:false, COOLS:false, COOLY:false, COOMB:false, COOMS:false, COOMY:false,
    COONS:false, COOPS:false, COOPT:false, COOST:false, COOTS:false, COOZE:false, COPAL:false, COPAY:false, COPED:false, COPEN:false,
    COPER:false, COPES:false, COPPY:false, COPRA:false, COPSE:false, COPSY:false, COQUI:false, CORAL:false, CORAM:false, CORBE:false,
    CORBY:false, CORDS:false, CORED:false, CORER:false, CORES:false, COREY:false, CORGI:false, CORIA:false, CORKS:false, CORKY:false,
    CORMS:false, CORNI:false, CORNO:false, CORNS:false, CORNU:false, CORNY:false, CORPS:false, CORSE:false, CORSO:false, COSEC:false,
    COSED:false, COSES:false, COSET:false, COSEY:false, COSIE:false, COSTA:false, COSTE:false, COSTS:false, COTAN:false, COTED:false,
    COTES:false, COTHS:false, COTTA:false, COTTS:false, COUCH:false, COUDE:false, COUGH:false, COULD:false, COUNT:false, COUPE:false,
    COUPS:false, COURB:false, COURD:false, COURE:false, COURS:false, COURT:false, COUTA:false, COUTH:false, COVED:false, COVEN:false,
    COVER:false, COVES:false, COVET:false, COVEY:false, COVIN:false, COWAL:false, COWAN:false, COWED:false, COWER:false, COWKS:false,
    COWLS:false, COWPS:false, COWRY:false, COXAE:false, COXAL:false, COXED:false, COXES:false, COXIB:false, COYAU:false, COYED:false,
    COYER:false, COYLY:false, COYPU:false, COZED:false, COZEN:false, COZES:false, COZEY:false, COZIE:false, CRAAL:false, CRABS:false,
    CRACK:false, CRAFT:false, CRAGS:false, CRAIC:false, CRAIG:false, CRAKE:false, CRAME:false, CRAMP:false, CRAMS:false, CRANE:false,
    CRANK:false, CRANS:false, CRAPE:false, CRAPS:false, CRAPY:false, CRARE:false, CRASH:false, CRASS:false, CRATE:false, CRAVE:false,
    CRAWL:false, CRAWS:false, CRAYS:false, CRAZE:false, CRAZY:false, CREAK:false, CREAM:false, CREDO:false, CREDS:false, CREED:false,
    CREEK:false, CREEL:false, CREEP:false, CREES:false, CREME:false, CREMS:false, CRENA:false, CREPE:false, CREPS:false, CREPT:false,
    CREPY:false, CRESS:false, CREST:false, CREWE:false, CREWS:false, CRIAS:false, CRIBS:false, CRICK:false, CRIED:false, CRIER:false,
    CRIES:false, CRIME:false, CRIMP:false, CRIMS:false, CRINE:false, CRIOS:false, CRIPE:false, CRIPS:false, CRISE:false, CRISP:false,
    CRITH:false, CRITS:false, CROAK:false, CROCI:false, CROCK:false, CROCS:false, CROFT:false, CROGS:false, CROMB:false, CROME:false,
    CRONE:false, CRONK:false, CRONS:false, CRONY:false, CROOK:false, CROOL:false, CROON:false, CROPS:false, CRORE:false, CROSS:false,
    CROST:false, CROUP:false, CROUT:false, CROWD:false, CROWN:false, CROWS:false, CROZE:false, CRUCK:false, CRUDE:false, CRUDO:false,
    CRUDS:false, CRUDY:false, CRUEL:false, CRUES:false, CRUET:false, CRUFT:false, CRUMB:false, CRUMP:false, CRUNK:false, CRUOR:false,
    CRURA:false, CRUSE:false, CRUSH:false, CRUST:false, CRUSY:false, CRUVE:false, CRWTH:false, CRYER:false, CRYPT:false, CTENE:false,
    CUBBY:false, CUBEB:false, CUBED:false, CUBER:false, CUBES:false, CUBIC:false, CUBIT:false, CUDDY:false, CUFFO:false, CUFFS:false,
    CUIFS:false, CUING:false, CUISH:false, CUITS:false, CUKES:false, CULCH:false, CULET:false, CULEX:false, CULLS:false, CULLY:false,
    CULMS:false, CULPA:false, CULTI:false, CULTS:false, CULTY:false, CUMEC:false, CUMIN:false, CUNDY:false, CUNEI:false, CUNIT:false,
    CUNTS:false, CUPEL:false, CUPID:false, CUPPA:false, CUPPY:false, CURAT:false, CURBS:false, CURCH:false, CURDS:false, CURDY:false,
    CURED:false, CURER:false, CURES:false, CURET:false, CURFS:false, CURIA:false, CURIE:false, CURIO:false, CURLI:false, CURLS:false,
    CURLY:false, CURNS:false, CURNY:false, CURRS:false, CURRY:false, CURSE:false, CURSI:false, CURST:false, CURVE:false, CURVY:false,
    CUSEC:false, CUSHY:false, CUSKS:false, CUSPS:false, CUSPY:false, CUSSO:false, CUSUM:false, CUTCH:false, CUTER:false, CUTES:false,
    CUTEY:false, CUTIE:false, CUTIN:false, CUTIS:false, CUTTO:false, CUTTY:false, CUTUP:false, CUVEE:false, CUZES:false, CWTCH:false,
    CYANO:false, CYANS:false, CYBER:false, CYCAD:false, CYCAS:false, CYCLE:false, CYCLO:false, CYDER:false, CYLIX:false, CYMAE:false,
    CYMAR:false, CYMAS:false, CYMES:false, CYMOL:false, CYNIC:false, CYSTS:false, CYTES:false, CYTON:false, CZARS:false, DAALS:false,
    DABBA:false, DACES:false, DACHA:false, DACKS:false, DADAH:false, DADAS:false, DADDY:false, DADOS:false, DAFFS:false, DAFFY:false,
    DAGGA:false, DAGGY:false, DAGOS:false, DAHLS:false, DAIKO:false, DAILY:false, DAINE:false, DAINT:false, DAIRY:false, DAISY:false,
    DAKER:false, DALED:false, DALES:false, DALIS:false, DALLE:false, DALLY:false, DALTS:false, DAMAN:false, DAMAR:false, DAMES:false,
    DAMME:false, DAMNS:false, DAMPS:false, DAMPY:false, DANCE:false, DANCY:false, DANDY:false, DANGS:false, DANIO:false, DANKS:false,
    DANNY:false, DANTS:false, DARAF:false, DARBS:false, DARCY:false, DARED:false, DARER:false, DARES:false, DARGA:false, DARGS:false,
    DARIC:false, DARIS:false, DARKS:false, DARKY:false, DARNS:false, DARRE:false, DARTS:false, DARZI:false, DASHI:false, DASHY:false,
    DATAL:false, DATED:false, DATER:false, DATES:false, DATOS:false, DATTO:false, DATUM:false, DAUBE:false, DAUBS:false, DAUBY:false,
    DAUDS:false, DAULT:false, DAUNT:false, DAURS:false, DAUTS:false, DAVEN:false, DAVIT:false, DAWAH:false, DAWDS:false, DAWED:false,
    DAWEN:false, DAWKS:false, DAWNS:false, DAWTS:false, DAYAN:false, DAYCH:false, DAYNT:false, DAZED:false, DAZER:false, DAZES:false,
    DEADS:false, DEAIR:false, DEALS:false, DEALT:false, DEANS:false, DEARE:false, DEARN:false, DEARS:false, DEARY:false, DEASH:false,
    DEATH:false, DEAVE:false, DEAWS:false, DEAWY:false, DEBAG:false, DEBAR:false, DEBBY:false, DEBEL:false, DEBES:false, DEBIT:false,
    DEBTS:false, DEBUD:false, DEBUG:false, DEBUR:false, DEBUS:false, DEBUT:false, DEBYE:false, DECAD:false, DECAF:false, DECAL:false,
    DECAN:false, DECAY:false, DECKO:false, DECKS:false, DECOR:false, DECOS:false, DECOY:false, DECRY:false, DEDAL:false, DEEDS:false,
    DEEDY:false, DEELY:false, DEEMS:false, DEENS:false, DEEPS:false, DEERE:false, DEERS:false, DEETS:false, DEEVE:false, DEEVS:false,
    DEFAT:false, DEFER:false, DEFFO:false, DEFIS:false, DEFOG:false, DEGAS:false, DEGUM:false, DEGUS:false, DEICE:false, DEIDS:false,
    DEIFY:false, DEIGN:false, DEILS:false, DEISM:false, DEIST:false, DEITY:false, DEKED:false, DEKES:false, DEKKO:false, DELAY:false,
    DELED:false, DELES:false, DELFS:false, DELFT:false, DELIS:false, DELLS:false, DELLY:false, DELOS:false, DELPH:false, DELTA:false,
    DELTS:false, DELVE:false, DEMAN:false, DEMES:false, DEMIC:false, DEMIT:false, DEMOB:false, DEMOI:false, DEMON:false, DEMOS:false,
    DEMPT:false, DEMUR:false, DENAR:false, DENAY:false, DENCH:false, DENES:false, DENET:false, DENIM:false, DENIS:false, DENSE:false,
    DENTS:false, DEOXY:false, DEPOT:false, DEPTH:false, DERAT:false, DERAY:false, DERBY:false, DERED:false, DERES:false, DERIG:false,
    DERMA:false, DERMS:false, DERNS:false, DERNY:false, DEROS:false, DERRO:false, DERRY:false, DERTH:false, DERVS:false, DESEX:false,
    DESHI:false, DESIS:false, DESKS:false, DESSE:false, DETER:false, DETOX:false, DEUCE:false, DEVAS:false, DEVEL:false, DEVIL:false,
    DEVIS:false, DEVON:false, DEVOS:false, DEVOT:false, DEWAN:false, DEWAR:false, DEWAX:false, DEWED:false, DEXES:false, DEXIE:false,
    DHABA:false, DHAKS:false, DHALS:false, DHIKR:false, DHOBI:false, DHOLE:false, DHOLL:false, DHOLS:false, DHOTI:false, DHOWS:false,
    DHUTI:false, DIACT:false, DIALS:false, DIANE:false, DIARY:false, DIAZO:false, DIBBS:false, DICED:false, DICER:false, DICES:false,
    DICEY:false, DICHT:false, DICKS:false, DICKY:false, DICOT:false, DICTA:false, DICTS:false, DICTY:false, DIDDY:false, DIDIE:false,
    DIDOS:false, DIDST:false, DIEBS:false, DIELS:false, DIENE:false, DIETS:false, DIFFS:false, DIGHT:false, DIGIT:false, DIKAS:false,
    DIKED:false, DIKER:false, DIKES:false, DIKEY:false, DILDO:false, DILLI:false, DILLS:false, DILLY:false, DIMBO:false, DIMER:false,
    DIMES:false, DIMLY:false, DIMPS:false, DINAR:false, DINED:false, DINER:false, DINES:false, DINGE:false, DINGO:false, DINGS:false,
    DINGY:false, DINIC:false, DINKS:false, DINKY:false, DINNA:false, DINOS:false, DINTS:false, DIODE:false, DIOLS:false, DIOTA:false,
    DIPPY:false, DIPSO:false, DIRAM:false, DIRER:false, DIRGE:false, DIRKE:false, DIRKS:false, DIRLS:false, DIRTS:false, DIRTY:false,
    DISAS:false, DISCI:false, DISCO:false, DISCS:false, DISHY:false, DISKS:false, DISME:false, DITAL:false, DITAS:false, DITCH:false,
    DITED:false, DITES:false, DITSY:false, DITTO:false, DITTS:false, DITTY:false, DITZY:false, DIVAN:false, DIVAS:false, DIVED:false,
    DIVER:false, DIVES:false, DIVIS:false, DIVNA:false, DIVOS:false, DIVOT:false, DIVVY:false, DIWAN:false, DIXIE:false, DIXIT:false,
    DIYAS:false, DIZEN:false, DIZZY:false, DJINN:false, DJINS:false, DOABS:false, DOATS:false, DOBBY:false, DOBES:false, DOBIE:false,
    DOBLA:false, DOBRA:false, DOBRO:false, DOCHT:false, DOCKS:false, DOCOS:false, DOCUS:false, DODDY:false, DODGE:false, DODGY:false,
    DODOS:false, DOEKS:false, DOERS:false, DOEST:false, DOETH:false, DOFFS:false, DOGAN:false, DOGES:false, DOGEY:false, DOGGO:false,
    DOGGY:false, DOGIE:false, DOGMA:false, DOHYO:false, DOILT:false, DOILY:false, DOING:false, DOITS:false, DOJOS:false, DOLCE:false,
    DOLCI:false, DOLED:false, DOLES:false, DOLIA:false, DOLLS:false, DOLLY:false, DOLMA:false, DOLOR:false, DOLOS:false, DOLTS:false,
    DOMAL:false, DOMED:false, DOMES:false, DOMIC:false, DONAH:false, DONAS:false, DONEE:false, DONER:false, DONGA:false, DONGS:false,
    DONKO:false, DONNA:false, DONNE:false, DONNY:false, DONOR:false, DONSY:false, DONUT:false, DOOBS:false, DOOCE:false, DOODY:false,
    DOOKS:false, DOOLE:false, DOOLS:false, DOOLY:false, DOOMS:false, DOOMY:false, DOONA:false, DOORN:false, DOORS:false, DOOZY:false,
    DOPAS:false, DOPED:false, DOPER:false, DOPES:false, DOPEY:false, DORAD:false, DORBA:false, DORBS:false, DOREE:false, DORES:false,
    DORIC:false, DORIS:false, DORKS:false, DORKY:false, DORMS:false, DORMY:false, DORPS:false, DORRS:false, DORSA:false, DORSE:false,
    DORTS:false, DORTY:false, DOSAI:false, DOSAS:false, DOSED:false, DOSEH:false, DOSER:false, DOSES:false, DOSHA:false, DOTAL:false,
    DOTED:false, DOTER:false, DOTES:false, DOTTY:false, DOUAR:false, DOUBT:false, DOUCE:false, DOUCS:false, DOUGH:false, DOUKS:false,
    DOULA:false, DOUMA:false, DOUMS:false, DOUPS:false, DOURA:false, DOUSE:false, DOUTS:false, DOVED:false, DOVEN:false, DOVER:false,
    DOVES:false, DOVIE:false, DOWAR:false, DOWDS:false, DOWDY:false, DOWED:false, DOWEL:false, DOWER:false, DOWIE:false, DOWLE:false,
    DOWLS:false, DOWLY:false, DOWNA:false, DOWNS:false, DOWNY:false, DOWPS:false, DOWRY:false, DOWSE:false, DOWTS:false, DOXED:false,
    DOXES:false, DOXIE:false, DOYEN:false, DOYLY:false, DOZED:false, DOZEN:false, DOZER:false, DOZES:false, DRABS:false, DRACK:false,
    DRACO:false, DRAFF:false, DRAFT:false, DRAGS:false, DRAIL:false, DRAIN:false, DRAKE:false, DRAMA:false, DRAMS:false, DRANK:false,
    DRANT:false, DRAPE:false, DRAPS:false, DRATS:false, DRAVE:false, DRAWL:false, DRAWN:false, DRAWS:false, DRAYS:false, DREAD:false,
    DREAM:false, DREAR:false, DRECK:false, DREED:false, DREER:false, DREES:false, DREGS:false, DREKS:false, DRENT:false, DRERE:false,
    DRESS:false, DREST:false, DREYS:false, DRIBS:false, DRICE:false, DRIED:false, DRIER:false, DRIES:false, DRIFT:false, DRILL:false,
    DRILY:false, DRINK:false, DRIPS:false, DRIPT:false, DRIVE:false, DROID:false, DROIL:false, DROIT:false, DROKE:false, DROLE:false,
    DROLL:false, DROME:false, DRONE:false, DRONY:false, DROOB:false, DROOG:false, DROOK:false, DROOL:false, DROOP:false, DROPS:false,
    DROPT:false, DROSS:false, DROUK:false, DROVE:false, DROWN:false, DROWS:false, DRUBS:false, DRUGS:false, DRUID:false, DRUMS:false,
    DRUNK:false, DRUPE:false, DRUSE:false, DRUSY:false, DRUXY:false, DRYAD:false, DRYAS:false, DRYER:false, DRYLY:false, DSOBO:false,
    DSOMO:false, DUADS:false, DUALS:false, DUANS:false, DUARS:false, DUBBO:false, DUCAL:false, DUCAT:false, DUCES:false, DUCHY:false,
    DUCKS:false, DUCKY:false, DUCTS:false, DUDDY:false, DUDED:false, DUDES:false, DUELS:false, DUETS:false, DUETT:false, DUFFS:false,
    DUFUS:false, DUING:false, DUITS:false, DUKAS:false, DUKED:false, DUKES:false, DUKKA:false, DULCE:false, DULES:false, DULIA:false,
    DULLS:false, DULLY:false, DULSE:false, DUMAS:false, DUMBO:false, DUMBS:false, DUMKA:false, DUMKY:false, DUMMY:false, DUMPS:false,
    DUMPY:false, DUNAM:false, DUNCE:false, DUNCH:false, DUNES:false, DUNGS:false, DUNGY:false, DUNKS:false, DUNNO:false, DUNNY:false,
    DUNSH:false, DUNTS:false, DUOMI:false, DUOMO:false, DUPED:false, DUPER:false, DUPES:false, DUPLE:false, DUPLY:false, DUPPY:false,
    DURAL:false, DURAS:false, DURED:false, DURES:false, DURGY:false, DURNS:false, DUROC:false, DUROS:false, DUROY:false, DURRA:false,
    DURRS:false, DURRY:false, DURST:false, DURUM:false, DURZI:false, DUSKS:false, DUSKY:false, DUSTS:false, DUSTY:false, DUTCH:false,
    DUVET:false, DUXES:false, DWAAL:false, DWALE:false, DWALM:false, DWAMS:false, DWANG:false, DWARF:false, DWAUM:false, DWEEB:false,
    DWELL:false, DWELT:false, DWILE:false, DWINE:false, DYADS:false, DYERS:false, DYING:false, DYKED:false, DYKES:false, DYKEY:false,
    DYKON:false, DYNEL:false, DYNES:false, DZHOS:false, EAGER:false, EAGLE:false, EAGRE:false, EALED:false, EALES:false, EANED:false,
    EARDS:false, EARED:false, EARLS:false, EARLY:false, EARNS:false, EARNT:false, EARST:false, EARTH:false, EASED:false, EASEL:false,
    EASER:false, EASES:false, EASLE:false, EASTS:false, EATEN:false, EATER:false, EATHE:false, EAVED:false, EAVES:false, EBBED:false,
    EBBET:false, EBONS:false, EBONY:false, EBOOK:false, ECADS:false, ECHED:false, ECHES:false, ECHOS:false, ECLAT:false, ECRUS:false,
    EDEMA:false, EDGED:false, EDGER:false, EDGES:false, EDICT:false, EDIFY:false, EDILE:false, EDITS:false, EDUCE:false, EDUCT:false,
    EEJIT:false, EENSY:false, EERIE:false, EEVEN:false, EEVNS:false, EFFED:false, EGADS:false, EGERS:false, EGEST:false, EGGAR:false,
    EGGED:false, EGGER:false, EGMAS:false, EGRET:false, EHING:false, EIDER:false, EIDOS:false, EIGHT:false, EIGNE:false, EIKED:false,
    EIKON:false, EILDS:false, EISEL:false, EJECT:false, EJIDO:false, EKING:false, EKKAS:false, ELAIN:false, ELAND:false, ELANS:false,
    ELATE:false, ELBOW:false, ELCHI:false, ELDER:false, ELDIN:false, ELECT:false, ELEGY:false, ELEMI:false, ELFED:false, ELFIN:false,
    ELIAD:false, ELIDE:false, ELINT:false, ELITE:false, ELMEN:false, ELOGE:false, ELOGY:false, ELOIN:false, ELOPE:false, ELOPS:false,
    ELPEE:false, ELSIN:false, ELUDE:false, ELUTE:false, ELVAN:false, ELVEN:false, ELVER:false, ELVES:false, EMACS:false, EMAIL:false,
    EMBAR:false, EMBAY:false, EMBED:false, EMBER:false, EMBOG:false, EMBOW:false, EMBOX:false, EMBUS:false, EMCEE:false, EMEER:false,
    EMEND:false, EMERG:false, EMERY:false, EMEUS:false, EMICS:false, EMIRS:false, EMITS:false, EMMAS:false, EMMER:false, EMMET:false,
    EMMEW:false, EMMYS:false, EMOJI:false, EMONG:false, EMOTE:false, EMOVE:false, EMPTS:false, EMPTY:false, EMULE:false, EMURE:false,
    EMYDE:false, EMYDS:false, ENACT:false, ENARM:false, ENATE:false, ENDED:false, ENDER:false, ENDEW:false, ENDOW:false, ENDUE:false,
    ENEMA:false, ENEMY:false, ENEWS:false, ENFIX:false, ENIAC:false, ENJOY:false, ENLIT:false, ENMEW:false, ENNOG:false, ENNUI:false,
    ENOKI:false, ENOLS:false, ENORM:false, ENOWS:false, ENROL:false, ENSEW:false, ENSKY:false, ENSUE:false, ENTER:false, ENTIA:false,
    ENTRY:false, ENURE:false, ENURN:false, ENVOI:false, ENVOY:false, ENZYM:false, EORLS:false, EOSIN:false, EPACT:false, EPEES:false,
    EPHAH:false, EPHAS:false, EPHOD:false, EPHOR:false, EPICS:false, EPOCH:false, EPODE:false, EPOPT:false, EPOXY:false, EPRIS:false,
    EQUAL:false, EQUES:false, EQUID:false, EQUIP:false, ERASE:false, ERBIA:false, ERECT:false, EREVS:false, ERGON:false, ERGOS:false,
    ERGOT:false, ERHUS:false, ERICA:false, ERICK:false, ERICS:false, ERING:false, ERNED:false, ERNES:false, ERODE:false, EROSE:false,
    ERRED:false, ERROR:false, ERSES:false, ERUCT:false, ERUGO:false, ERUPT:false, ERUVS:false, ERVEN:false, ERVIL:false, ESCAR:false,
    ESCOT:false, ESILE:false, ESKAR:false, ESKER:false, ESNES:false, ESSAY:false, ESSES:false, ESTER:false, ESTOC:false, ESTOP:false,
    ESTRO:false, ETAGE:false, ETAPE:false, ETATS:false, ETENS:false, ETHAL:false, ETHER:false, ETHIC:false, ETHNE:false, ETHOS:false,
    ETHYL:false, ETICS:false, ETNAS:false, ETTIN:false, ETTLE:false, ETUDE:false, ETUIS:false, ETWEE:false, ETYMA:false, EUGHS:false,
    EUKED:false, EUPAD:false, EUROS:false, EUSOL:false, EVADE:false, EVENS:false, EVENT:false, EVERT:false, EVERY:false, EVETS:false,
    EVHOE:false, EVICT:false, EVILS:false, EVITE:false, EVOHE:false, EVOKE:false, EWERS:false, EWEST:false, EWHOW:false, EWKED:false,
    EXACT:false, EXALT:false, EXAMS:false, EXCEL:false, EXEAT:false, EXECS:false, EXEEM:false, EXEME:false, EXERT:false, EXFIL:false,
    EXIES:false, EXILE:false, EXINE:false, EXING:false, EXIST:false, EXITS:false, EXODE:false, EXOME:false, EXONS:false, EXPAT:false,
    EXPEL:false, EXPOS:false, EXTOL:false, EXTRA:false, EXUDE:false, EXULS:false, EXULT:false, EXURB:false, EYASS:false, EYERS:false,
    EYING:false, EYOTS:false, EYRAS:false, EYRES:false, EYRIE:false, EYRIR:false, EZINE:false, FABBY:false, FABLE:false, FACED:false,
    FACER:false, FACES:false, FACET:false, FACIA:false, FACTA:false, FACTS:false, FADDY:false, FADED:false, FADER:false, FADES:false,
    FADGE:false, FADOS:false, FAENA:false, FAERY:false, FAFFS:false, FAFFY:false, FAGGY:false, FAGIN:false, FAGOT:false, FAIKS:false,
    FAILS:false, FAINE:false, FAINS:false, FAINT:false, FAIRS:false, FAIRY:false, FAITH:false, FAKED:false, FAKER:false, FAKES:false,
    FAKEY:false, FAKIE:false, FAKIR:false, FALAJ:false, FALLS:false, FALSE:false, FAMED:false, FAMES:false, FANAL:false, FANCY:false,
    FANDS:false, FANES:false, FANGA:false, FANGO:false, FANGS:false, FANKS:false, FANNY:false, FANON:false, FANOS:false, FANUM:false,
    FAQIR:false, FARAD:false, FARCE:false, FARCI:false, FARCY:false, FARDS:false, FARED:false, FARER:false, FARES:false, FARLE:false,
    FARLS:false, FARMS:false, FAROS:false, FARRO:false, FARSE:false, FARTS:false, FASCI:false, FASTI:false, FASTS:false, FATAL:false,
    FATED:false, FATES:false, FATLY:false, FATSO:false, FATTY:false, FATWA:false, FAUGH:false, FAULD:false, FAULT:false, FAUNA:false,
    FAUNS:false, FAURD:false, FAUTS:false, FAUVE:false, FAVAS:false, FAVEL:false, FAVER:false, FAVES:false, FAVOR:false, FAVUS:false,
    FAWNS:false, FAWNY:false, FAXED:false, FAXES:false, FAYED:false, FAYER:false, FAYNE:false, FAYRE:false, FAZED:false, FAZES:false,
    FEALS:false, FEARE:false, FEARS:false, FEART:false, FEASE:false, FEAST:false, FEATS:false, FEAZE:false, FECAL:false, FECES:false,
    FECHT:false, FECIT:false, FECKS:false, FEDEX:false, FEEBS:false, FEEDS:false, FEELS:false, FEENS:false, FEERS:false, FEESE:false,
    FEEZE:false, FEHME:false, FEIGN:false, FEINT:false, FEIST:false, FELCH:false, FELID:false, FELLA:false, FELLS:false, FELLY:false,
    FELON:false, FELTS:false, FELTY:false, FEMAL:false, FEMES:false, FEMME:false, FEMMY:false, FEMUR:false, FENCE:false, FENDS:false,
    FENDY:false, FENIS:false, FENKS:false, FENNY:false, FENTS:false, FEODS:false, FEOFF:false, FERAL:false, FERER:false, FERES:false,
    FERIA:false, FERLY:false, FERMI:false, FERMS:false, FERNS:false, FERNY:false, FERRY:false, FESSE:false, FESTA:false, FESTS:false,
    FESTY:false, FETAL:false, FETAS:false, FETCH:false, FETED:false, FETES:false, FETID:false, FETOR:false, FETTA:false, FETTS:false,
    FETUS:false, FETWA:false, FEUAR:false, FEUDS:false, FEUED:false, FEVER:false, FEWER:false, FEYED:false, FEYER:false, FEYLY:false,
    FEZES:false, FEZZY:false, FIARS:false, FIATS:false, FIBER:false, FIBRE:false, FIBRO:false, FICES:false, FICHE:false, FICHU:false,
    FICIN:false, FICOS:false, FICUS:false, FIDES:false, FIDGE:false, FIDOS:false, FIEFS:false, FIELD:false, FIEND:false, FIENT:false,
    FIERE:false, FIERS:false, FIERY:false, FIEST:false, FIFED:false, FIFER:false, FIFES:false, FIFIS:false, FIFTH:false, FIFTY:false,
    FIGGY:false, FIGHT:false, FIGOS:false, FIKED:false, FIKES:false, FILAR:false, FILCH:false, FILED:false, FILER:false, FILES:false,
    FILET:false, FILII:false, FILKS:false, FILLE:false, FILLO:false, FILLS:false, FILLY:false, FILMI:false, FILMS:false, FILMY:false,
    FILOS:false, FILTH:false, FILUM:false, FINAL:false, FINCA:false, FINCH:false, FINDS:false, FINED:false, FINER:false, FINES:false,
    FINIS:false, FINKS:false, FINNY:false, FINOS:false, FIORD:false, FIQHS:false, FIQUE:false, FIRED:false, FIRER:false, FIRES:false,
    FIRIE:false, FIRKS:false, FIRMS:false, FIRNS:false, FIRRY:false, FIRST:false, FIRTH:false, FISCS:false, FISHY:false, FISKS:false,
    FISTS:false, FISTY:false, FITCH:false, FITLY:false, FITNA:false, FITTE:false, FITTS:false, FIVER:false, FIVES:false, FIXED:false,
    FIXER:false, FIXES:false, FIXIT:false, FIZZY:false, FJELD:false, FJORD:false, FLABS:false, FLACK:false, FLAFF:false, FLAGS:false,
    FLAIL:false, FLAIR:false, FLAKE:false, FLAKS:false, FLAKY:false, FLAME:false, FLAMM:false, FLAMS:false, FLAMY:false, FLANE:false,
    FLANK:false, FLANS:false, FLAPS:false, FLARE:false, FLARY:false, FLASH:false, FLASK:false, FLATS:false, FLAVA:false, FLAWN:false,
    FLAWS:false, FLAWY:false, FLAXY:false, FLAYS:false, FLEAM:false, FLEAS:false, FLECK:false, FLEEK:false, FLEER:false, FLEES:false,
    FLEET:false, FLEGS:false, FLEME:false, FLESH:false, FLEUR:false, FLEWS:false, FLEXI:false, FLEXO:false, FLEYS:false, FLICK:false,
    FLICS:false, FLIED:false, FLIER:false, FLIES:false, FLIMP:false, FLIMS:false, FLING:false, FLINT:false, FLIPS:false, FLIRS:false,
    FLIRT:false, FLISK:false, FLITE:false, FLITS:false, FLITT:false, FLOAT:false, FLOBS:false, FLOCK:false, FLOCS:false, FLOES:false,
    FLOGS:false, FLONG:false, FLOOD:false, FLOOR:false, FLOPS:false, FLORA:false, FLORS:false, FLORY:false, FLOSH:false, FLOSS:false,
    FLOTA:false, FLOTE:false, FLOUR:false, FLOUT:false, FLOWN:false, FLOWS:false, FLUBS:false, FLUED:false, FLUES:false, FLUEY:false,
    FLUFF:false, FLUID:false, FLUKE:false, FLUKY:false, FLUME:false, FLUMP:false, FLUNG:false, FLUNK:false, FLUOR:false, FLURR:false,
    FLUSH:false, FLUTE:false, FLUTY:false, FLUYT:false, FLYBY:false, FLYER:false, FLYPE:false, FLYTE:false, FOALS:false, FOAMS:false,
    FOAMY:false, FOCAL:false, FOCUS:false, FOEHN:false, FOGEY:false, FOGGY:false, FOGIE:false, FOGLE:false, FOGOU:false, FOHNS:false,
    FOIDS:false, FOILS:false, FOINS:false, FOIST:false, FOLDS:false, FOLEY:false, FOLIA:false, FOLIC:false, FOLIE:false, FOLIO:false,
    FOLKS:false, FOLKY:false, FOLLY:false, FOMES:false, FONDA:false, FONDS:false, FONDU:false, FONES:false, FONLY:false, FONTS:false,
    FOODS:false, FOODY:false, FOOLS:false, FOOTS:false, FOOTY:false, FORAM:false, FORAY:false, FORBS:false, FORBY:false, FORCE:false,
    FORDO:false, FORDS:false, FOREL:false, FORES:false, FOREX:false, FORGE:false, FORGO:false, FORKS:false, FORKY:false, FORME:false,
    FORMS:false, FORTE:false, FORTH:false, FORTS:false, FORTY:false, FORUM:false, FORZA:false, FORZE:false, FOSSA:false, FOSSE:false,
    FOUAT:false, FOUDS:false, FOUER:false, FOUET:false, FOULE:false, FOULS:false, FOUND:false, FOUNT:false, FOURS:false, FOUTH:false,
    FOVEA:false, FOWLS:false, FOWTH:false, FOXED:false, FOXES:false, FOXIE:false, FOYER:false, FOYLE:false, FOYNE:false, FRABS:false,
    FRACK:false, FRACT:false, FRAGS:false, FRAIL:false, FRAIM:false, FRAME:false, FRANC:false, FRANK:false, FRAPE:false, FRAPS:false,
    FRASS:false, FRATE:false, FRATI:false, FRATS:false, FRAUD:false, FRAUS:false, FRAYS:false, FREAK:false, FREED:false, FREER:false,
    FREES:false, FREET:false, FREIT:false, FREMD:false, FRENA:false, FREON:false, FRERE:false, FRESH:false, FRETS:false, FRIAR:false,
    FRIBS:false, FRIED:false, FRIER:false, FRIES:false, FRIGS:false, FRILL:false, FRISE:false, FRISK:false, FRIST:false, FRITH:false,
    FRITS:false, FRITT:false, FRITZ:false, FRIZE:false, FRIZZ:false, FROCK:false, FROES:false, FROGS:false, FROND:false, FRONS:false,
    FRONT:false, FRORE:false, FRORN:false, FRORY:false, FROSH:false, FROST:false, FROTH:false, FROWN:false, FROWS:false, FROWY:false,
    FROZE:false, FRUGS:false, FRUIT:false, FRUMP:false, FRUSH:false, FRUST:false, FRYER:false, FUBAR:false, FUBBY:false, FUBSY:false,
    FUCKS:false, FUCUS:false, FUDDY:false, FUDGE:false, FUDGY:false, FUELS:false, FUERO:false, FUFFS:false, FUFFY:false, FUGAL:false,
    FUGGY:false, FUGIE:false, FUGIO:false, FUGLE:false, FUGLY:false, FUGUE:false, FUGUS:false, FUJIS:false, FULLS:false, FULLY:false,
    FUMED:false, FUMER:false, FUMES:false, FUMET:false, FUNDI:false, FUNDS:false, FUNDY:false, FUNGI:false, FUNGO:false, FUNGS:false,
    FUNKS:false, FUNKY:false, FUNNY:false, FURAL:false, FURAN:false, FURCA:false, FURLS:false, FUROL:false, FUROR:false, FURRS:false,
    FURRY:false, FURTH:false, FURZE:false, FURZY:false, FUSED:false, FUSEE:false, FUSEL:false, FUSES:false, FUSIL:false, FUSKS:false,
    FUSSY:false, FUSTS:false, FUSTY:false, FUTON:false, FUZED:false, FUZEE:false, FUZES:false, FUZIL:false, FUZZY:false, FYCES:false,
    FYKED:false, FYKES:false, FYLES:false, FYRDS:false, FYTTE:false, GABBA:false, GABBY:false, GABLE:false, GADDI:false, GADES:false,
    GADGE:false, GADID:false, GADIS:false, GADJE:false, GADJO:false, GADSO:false, GAFFE:false, GAFFS:false, GAGED:false, GAGER:false,
    GAGES:false, GAIDS:false, GAILY:false, GAINS:false, GAIRS:false, GAITA:false, GAITS:false, GAITT:false, GAJOS:false, GALAH:false,
    GALAS:false, GALAX:false, GALEA:false, GALED:false, GALES:false, GALLS:false, GALLY:false, GALOP:false, GALUT:false, GALVO:false,
    GAMAS:false, GAMAY:false, GAMBA:false, GAMBE:false, GAMBO:false, GAMBS:false, GAMED:false, GAMER:false, GAMES:false, GAMEY:false,
    GAMIC:false, GAMIN:false, GAMMA:false, GAMME:false, GAMMY:false, GAMPS:false, GAMUT:false, GANCH:false, GANDY:false, GANEF:false,
    GANEV:false, GANGS:false, GANJA:false, GANOF:false, GANTS:false, GAOLS:false, GAPED:false, GAPER:false, GAPES:false, GAPOS:false,
    GAPPY:false, GARBE:false, GARBO:false, GARBS:false, GARDA:false, GARES:false, GARIS:false, GARMS:false, GARNI:false, GARRE:false,
    GARTH:false, GARUM:false, GASES:false, GASPS:false, GASPY:false, GASSY:false, GASTS:false, GATCH:false, GATED:false, GATER:false,
    GATES:false, GATHS:false, GATOR:false, GAUCH:false, GAUCY:false, GAUDS:false, GAUDY:false, GAUGE:false, GAUJE:false, GAULT:false,
    GAUMS:false, GAUMY:false, GAUNT:false, GAUPS:false, GAURS:false, GAUSS:false, GAUZE:false, GAUZY:false, GAVEL:false, GAVOT:false,
    GAWCY:false, GAWDS:false, GAWKS:false, GAWKY:false, GAWPS:false, GAWSY:false, GAYAL:false, GAYER:false, GAYLY:false, GAZAL:false,
    GAZAR:false, GAZED:false, GAZER:false, GAZES:false, GAZON:false, GAZOO:false, GEALS:false, GEANS:false, GEARE:false, GEARS:false,
    GEATS:false, GEBUR:false, GECKO:false, GECKS:false, GEEKS:false, GEEKY:false, GEEPS:false, GEESE:false, GEEST:false, GEIST:false,
    GEITS:false, GELDS:false, GELEE:false, GELID:false, GELLY:false, GELTS:false, GEMEL:false, GEMMA:false, GEMMY:false, GEMOT:false,
    GENAL:false, GENAS:false, GENES:false, GENET:false, GENIC:false, GENIE:false, GENII:false, GENIP:false, GENNY:false, GENOA:false,
    GENOM:false, GENRE:false, GENRO:false, GENTS:false, GENTY:false, GENUA:false, GENUS:false, GEODE:false, GEOID:false, GERAH:false,
    GERBE:false, GERES:false, GERLE:false, GERMS:false, GERMY:false, GERNE:false, GESSE:false, GESSO:false, GESTE:false, GESTS:false,
    GETAS:false, GETUP:false, GEUMS:false, GEYAN:false, GEYER:false, GHAST:false, GHATS:false, GHAUT:false, GHAZI:false, GHEES:false,
    GHEST:false, GHOST:false, GHOUL:false, GHYLL:false, GIANT:false, GIBED:false, GIBEL:false, GIBER:false, GIBES:false, GIBLI:false,
    GIBUS:false, GIDDY:false, GIFTS:false, GIGAS:false, GIGHE:false, GIGOT:false, GIGUE:false, GILAS:false, GILDS:false, GILET:false,
    GILLS:false, GILLY:false, GILPY:false, GILTS:false, GIMEL:false, GIMME:false, GIMPS:false, GIMPY:false, GINCH:false, GINGE:false,
    GINGS:false, GINKS:false, GINNY:false, GINZO:false, GIPON:false, GIPPO:false, GIPPY:false, GIPSY:false, GIRDS:false, GIRLS:false,
    GIRLY:false, GIRNS:false, GIRON:false, GIROS:false, GIRRS:false, GIRSH:false, GIRTH:false, GIRTS:false, GISMO:false, GISMS:false,
    GISTS:false, GITCH:false, GITES:false, GIUST:false, GIVED:false, GIVEN:false, GIVER:false, GIVES:false, GIZMO:false, GLACE:false,
    GLADE:false, GLADS:false, GLADY:false, GLAIK:false, GLAIR:false, GLAMS:false, GLAND:false, GLANS:false, GLARE:false, GLARY:false,
    GLASS:false, GLAUM:false, GLAUR:false, GLAZE:false, GLAZY:false, GLEAM:false, GLEAN:false, GLEBA:false, GLEBE:false, GLEBY:false,
    GLEDE:false, GLEDS:false, GLEED:false, GLEEK:false, GLEES:false, GLEET:false, GLEIS:false, GLENS:false, GLENT:false, GLEYS:false,
    GLIAL:false, GLIAS:false, GLIBS:false, GLIDE:false, GLIFF:false, GLIFT:false, GLIKE:false, GLIME:false, GLIMS:false, GLINT:false,
    GLISK:false, GLITS:false, GLITZ:false, GLOAM:false, GLOAT:false, GLOBE:false, GLOBI:false, GLOBS:false, GLOBY:false, GLODE:false,
    GLOGG:false, GLOMS:false, GLOOM:false, GLOOP:false, GLOPS:false, GLORY:false, GLOSS:false, GLOST:false, GLOUT:false, GLOVE:false,
    GLOWS:false, GLOZE:false, GLUED:false, GLUER:false, GLUES:false, GLUEY:false, GLUGS:false, GLUME:false, GLUMS:false, GLUON:false,
    GLUTE:false, GLUTS:false, GLYPH:false, GNARL:false, GNARR:false, GNARS:false, GNASH:false, GNATS:false, GNAWN:false, GNAWS:false,
    GNOME:false, GNOWS:false, GOADS:false, GOAFS:false, GOALS:false, GOARY:false, GOATS:false, GOATY:false, GOBAN:false, GOBAR:false,
    GOBBI:false, GOBBO:false, GOBBY:false, GOBIS:false, GOBOS:false, GODET:false, GODLY:false, GODSO:false, GOELS:false, GOERS:false,
    GOEST:false, GOETH:false, GOETY:false, GOFER:false, GOFFS:false, GOGGA:false, GOGOS:false, GOIER:false, GOING:false, GOJIS:false,
    GOLDS:false, GOLDY:false, GOLEM:false, GOLES:false, GOLFS:false, GOLLY:false, GOLPE:false, GOLPS:false, GOMBO:false, GOMER:false,
    GOMPA:false, GONAD:false, GONCH:false, GONEF:false, GONER:false, GONGS:false, GONIA:false, GONIF:false, GONKS:false, GONNA:false,
    GONOF:false, GONYS:false, GONZO:false, GOOBY:false, GOODS:false, GOODY:false, GOOEY:false, GOOFS:false, GOOFY:false, GOOGS:false,
    GOOKS:false, GOOKY:false, GOOLD:false, GOOLS:false, GOOLY:false, GOONS:false, GOONY:false, GOOPS:false, GOOPY:false, GOORS:false,
    GOORY:false, GOOSE:false, GOOSY:false, GOPAK:false, GOPIK:false, GORAL:false, GORAS:false, GORED:false, GORES:false, GORGE:false,
    GORIS:false, GORMS:false, GORMY:false, GORPS:false, GORSE:false, GORSY:false, GOSHT:false, GOSSE:false, GOTCH:false, GOTHS:false,
    GOTHY:false, GOTTA:false, GOUCH:false, GOUGE:false, GOUKS:false, GOURA:false, GOURD:false, GOUTS:false, GOUTY:false, GOWAN:false,
    GOWDS:false, GOWFS:false, GOWKS:false, GOWLS:false, GOWNS:false, GOXES:false, GOYIM:false, GOYLE:false, GRAAL:false, GRABS:false,
    GRACE:false, GRADE:false, GRADS:false, GRAFF:false, GRAFT:false, GRAIL:false, GRAIN:false, GRAIP:false, GRAMA:false, GRAME:false,
    GRAMP:false, GRAMS:false, GRANA:false, GRAND:false, GRANS:false, GRANT:false, GRAPE:false, GRAPH:false, GRAPY:false, GRASP:false,
    GRASS:false, GRATE:false, GRAVE:false, GRAVS:false, GRAVY:false, GRAYS:false, GRAZE:false, GREAT:false, GREBE:false, GREBO:false,
    GRECE:false, GREED:false, GREEK:false, GREEN:false, GREES:false, GREET:false, GREGE:false, GREGO:false, GREIN:false, GRENS:false,
    GRESE:false, GREVE:false, GREWS:false, GREYS:false, GRICE:false, GRIDE:false, GRIDS:false, GRIEF:false, GRIFF:false, GRIFT:false,
    GRIGS:false, GRIKE:false, GRILL:false, GRIME:false, GRIMY:false, GRIND:false, GRINS:false, GRIOT:false, GRIPE:false, GRIPS:false,
    GRIPT:false, GRIPY:false, GRISE:false, GRIST:false, GRISY:false, GRITH:false, GRITS:false, GRIZE:false, GROAN:false, GROAT:false,
    GRODY:false, GROGS:false, GROIN:false, GROKS:false, GROMA:false, GRONE:false, GROOF:false, GROOM:false, GROPE:false, GROSS:false,
    GROSZ:false, GROTS:false, GROUF:false, GROUP:false, GROUT:false, GROVE:false, GROVY:false, GROWL:false, GROWN:false, GROWS:false,
    GRRLS:false, GRRRL:false, GRUBS:false, GRUED:false, GRUEL:false, GRUES:false, GRUFE:false, GRUFF:false, GRUME:false, GRUMP:false,
    GRUND:false, GRUNT:false, GRYCE:false, GRYDE:false, GRYKE:false, GRYPE:false, GRYPT:false, GUACO:false, GUANA:false, GUANO:false,
    GUANS:false, GUARD:false, GUARS:false, GUAVA:false, GUCKS:false, GUCKY:false, GUDES:false, GUESS:false, GUEST:false, GUFFS:false,
    GUGAS:false, GUIDE:false, GUIDS:false, GUILD:false, GUILE:false, GUILT:false, GUIMP:false, GUIRO:false, GUISE:false, GULAG:false,
    GULAR:false, GULAS:false, GULCH:false, GULES:false, GULET:false, GULFS:false, GULFY:false, GULLS:false, GULLY:false, GULPH:false,
    GULPS:false, GULPY:false, GUMBO:false, GUMMA:false, GUMMI:false, GUMMY:false, GUMPS:false, GUNDY:false, GUNGE:false, GUNGY:false,
    GUNKS:false, GUNKY:false, GUNNY:false, GUPPY:false, GUQIN:false, GURDY:false, GURGE:false, GURLS:false, GURLY:false, GURNS:false,
    GURRY:false, GURSH:false, GURUS:false, GUSHY:false, GUSLA:false, GUSLE:false, GUSLI:false, GUSSY:false, GUSTO:false, GUSTS:false,
    GUSTY:false, GUTSY:false, GUTTA:false, GUTTY:false, GUYED:false, GUYLE:false, GUYOT:false, GUYSE:false, GWINE:false, GYALS:false,
    GYANS:false, GYBED:false, GYBES:false, GYELD:false, GYMPS:false, GYNAE:false, GYNIE:false, GYNNY:false, GYNOS:false, GYOZA:false,
    GYPOS:false, GYPPO:false, GYPPY:false, GYPSY:false, GYRAL:false, GYRED:false, GYRES:false, GYRON:false, GYROS:false, GYRUS:false,
    GYTES:false, GYVED:false, GYVES:false, HAAFS:false, HAARS:false, HABIT:false, HABLE:false, HABUS:false, HACEK:false, HACKS:false,
    HADAL:false, HADED:false, HADES:false, HADJI:false, HADST:false, HAEMS:false, HAETS:false, HAFFS:false, HAFIZ:false, HAFTS:false,
    HAGGS:false, HAHAS:false, HAICK:false, HAIKA:false, HAIKS:false, HAIKU:false, HAILS:false, HAILY:false, HAINS:false, HAINT:false,
    HAIRS:false, HAIRY:false, HAITH:false, HAJES:false, HAJIS:false, HAJJI:false, HAKAM:false, HAKAS:false, HAKEA:false, HAKES:false,
    HAKIM:false, HAKUS:false, HALAL:false, HALED:false, HALER:false, HALES:false, HALFA:false, HALFS:false, HALID:false, HALLO:false,
    HALLS:false, HALMA:false, HALMS:false, HALON:false, HALOS:false, HALSE:false, HALTS:false, HALVA:false, HALVE:false, HALWA:false,
    HAMAL:false, HAMBA:false, HAMED:false, HAMES:false, HAMMY:false, HAMZA:false, HANAP:false, HANCE:false, HANCH:false, HANDS:false,
    HANDY:false, HANGI:false, HANGS:false, HANKS:false, HANKY:false, HANSA:false, HANSE:false, HANTS:false, HAOLE:false, HAOMA:false,
    HAPAX:false, HAPLY:false, HAPPI:false, HAPPY:false, HAPUS:false, HARAM:false, HARDS:false, HARDY:false, HARED:false, HAREM:false,
    HARES:false, HARIM:false, HARKS:false, HARLS:false, HARMS:false, HARNS:false, HAROS:false, HARPS:false, HARPY:false, HARRY:false,
    HARSH:false, HARTS:false, HASHY:false, HASKS:false, HASPS:false, HASTA:false, HASTE:false, HASTY:false, HATCH:false, HATED:false,
    HATER:false, HATES:false, HATHA:false, HAUDS:false, HAUFS:false, HAUGH:false, HAULD:false, HAULM:false, HAULS:false, HAULT:false,
    HAUNS:false, HAUNT:false, HAUSE:false, HAUTE:false, HAVEN:false, HAVER:false, HAVES:false, HAVOC:false, HAWED:false, HAWKS:false,
    HAWMS:false, HAWSE:false, HAYED:false, HAYER:false, HAYEY:false, HAYLE:false, HAZAN:false, HAZED:false, HAZEL:false, HAZER:false,
    HAZES:false, HEADS:false, HEADY:false, HEALD:false, HEALS:false, HEAME:false, HEAPS:false, HEAPY:false, HEARD:false, HEARE:false,
    HEARS:false, HEART:false, HEAST:false, HEATH:false, HEATS:false, HEAVE:false, HEAVY:false, HEBEN:false, HEBES:false, HECHT:false,
    HECKS:false, HEDER:false, HEDGE:false, HEDGY:false, HEEDS:false, HEEDY:false, HEELS:false, HEEZE:false, HEFTE:false, HEFTS:false,
    HEFTY:false, HEIDS:false, HEIGH:false, HEILS:false, HEIRS:false, HEIST:false, HEJAB:false, HEJRA:false, HELED:false, HELES:false,
    HELIO:false, HELIX:false, HELLO:false, HELLS:false, HELMS:false, HELOS:false, HELOT:false, HELPS:false, HELVE:false, HEMAL:false,
    HEMES:false, HEMIC:false, HEMIN:false, HEMPS:false, HEMPY:false, HENCE:false, HENCH:false, HENDS:false, HENGE:false, HENNA:false,
    HENNY:false, HENRY:false, HENTS:false, HEPAR:false, HERBS:false, HERBY:false, HERDS:false, HERES:false, HERLS:false, HERMA:false,
    HERMS:false, HERNS:false, HERON:false, HEROS:false, HERRY:false, HERSE:false, HERTZ:false, HERYE:false, HESPS:false, HESTS:false,
    HETES:false, HETHS:false, HEUCH:false, HEUGH:false, HEVEA:false, HEWED:false, HEWER:false, HEWGH:false, HEXAD:false, HEXED:false,
    HEXER:false, HEXES:false, HEXYL:false, HEYED:false, HIANT:false, HICKS:false, HIDED:false, HIDER:false, HIDES:false, HIEMS:false,
    HIGHS:false, HIGHT:false, HIJAB:false, HIJRA:false, HIKED:false, HIKER:false, HIKES:false, HIKOI:false, HILAR:false, HILCH:false,
    HILLO:false, HILLS:false, HILLY:false, HILTS:false, HILUM:false, HILUS:false, HIMBO:false, HINAU:false, HINDS:false, HINGE:false,
    HINGS:false, HINKY:false, HINNY:false, HINTS:false, HIOIS:false, HIPLY:false, HIPPO:false, HIPPY:false, HIRED:false, HIREE:false,
    HIRER:false, HIRES:false, HISSY:false, HISTS:false, HITCH:false, HITHE:false, HIVED:false, HIVER:false, HIVES:false, HIZEN:false,
    HOAED:false, HOAGY:false, HOARD:false, HOARS:false, HOARY:false, HOAST:false, HOBBY:false, HOBOS:false, HOCKS:false, HOCUS:false,
    HODAD:false, HODJA:false, HOERS:false, HOGAN:false, HOGEN:false, HOGGS:false, HOGHS:false, HOHED:false, HOICK:false, HOIED:false,
    HOIKS:false, HOING:false, HOISE:false, HOIST:false, HOKAS:false, HOKED:false, HOKES:false, HOKEY:false, HOKIS:false, HOKKU:false,
    HOKUM:false, HOLDS:false, HOLED:false, HOLES:false, HOLEY:false, HOLKS:false, HOLLA:false, HOLLO:false, HOLLY:false, HOLME:false,
    HOLMS:false, HOLON:false, HOLOS:false, HOLTS:false, HOMAS:false, HOMED:false, HOMER:false, HOMES:false, HOMEY:false, HOMIE:false,
    HOMME:false, HOMOS:false, HONAN:false, HONDA:false, HONDS:false, HONED:false, HONER:false, HONES:false, HONEY:false, HONGI:false,
    HONGS:false, HONKS:false, HONKY:false, HONOR:false, HOOCH:false, HOODS:false, HOODY:false, HOOEY:false, HOOFS:false, HOOKA:false,
    HOOKS:false, HOOKY:false, HOOLY:false, HOONS:false, HOOPS:false, HOORD:false, HOORS:false, HOOSH:false, HOOTS:false, HOOTY:false,
    HOOVE:false, HOPAK:false, HOPED:false, HOPER:false, HOPES:false, HOPPY:false, HORAH:false, HORAL:false, HORAS:false, HORDE:false,
    HORIS:false, HORKS:false, HORME:false, HORNS:false, HORNY:false, HORSE:false, HORST:false, HORSY:false, HOSED:false, HOSEL:false,
    HOSEN:false, HOSER:false, HOSES:false, HOSEY:false, HOSTA:false, HOSTS:false, HOTCH:false, HOTEL:false, HOTEN:false, HOTLY:false,
    HOTTY:false, HOUFF:false, HOUFS:false, HOUGH:false, HOUND:false, HOURI:false, HOURS:false, HOUSE:false, HOUTS:false, HOVEA:false,
    HOVED:false, HOVEL:false, HOVEN:false, HOVER:false, HOVES:false, HOWBE:false, HOWDY:false, HOWES:false, HOWFF:false, HOWFS:false,
    HOWKS:false, HOWLS:false, HOWRE:false, HOWSO:false, HOXED:false, HOXES:false, HOYAS:false, HOYED:false, HOYLE:false, HUBBY:false,
    HUCKS:false, HUDNA:false, HUDUD:false, HUERS:false, HUFFS:false, HUFFY:false, HUGER:false, HUGGY:false, HUHUS:false, HUIAS:false,
    HULAS:false, HULES:false, HULKS:false, HULKY:false, HULLO:false, HULLS:false, HULLY:false, HUMAN:false, HUMAS:false, HUMFS:false,
    HUMIC:false, HUMID:false, HUMOR:false, HUMPH:false, HUMPS:false, HUMPY:false, HUMUS:false, HUNCH:false, HUNKS:false, HUNKY:false,
    HUNTS:false, HURDS:false, HURLS:false, HURLY:false, HURRA:false, HURRY:false, HURST:false, HURTS:false, HUSHY:false, HUSKS:false,
    HUSKY:false, HUSOS:false, HUSSY:false, HUTCH:false, HUTIA:false, HUZZA:false, HUZZY:false, HWYLS:false, HYDRA:false, HYDRO:false,
    HYENA:false, HYENS:false, HYGGE:false, HYING:false, HYKES:false, HYLAS:false, HYLEG:false, HYLES:false, HYLIC:false, HYMEN:false,
    HYMNS:false, HYNDE:false, HYOID:false, HYPED:false, HYPER:false, HYPES:false, HYPHA:false, HYPHY:false, HYPOS:false, HYRAX:false,
    HYSON:false, HYTHE:false, IAMBI:false, IAMBS:false, IBRIK:false, ICERS:false, ICHED:false, ICHES:false, ICHOR:false, ICIER:false,
    ICILY:false, ICING:false, ICKER:false, ICKLE:false, ICONS:false, ICTAL:false, ICTIC:false, ICTUS:false, IDANT:false, IDEAL:false,
    IDEAS:false, IDEES:false, IDENT:false, IDIOM:false, IDIOT:false, IDLED:false, IDLER:false, IDLES:false, IDOLA:false, IDOLS:false,
    IDYLL:false, IDYLS:false, IFTAR:false, IGAPO:false, IGGED:false, IGLOO:false, IGLUS:false, IHRAM:false, IKANS:false, IKATS:false,
    IKONS:false, ILEAC:false, ILEAL:false, ILEUM:false, ILEUS:false, ILIAC:false, ILIAD:false, ILIAL:false, ILIUM:false, ILLER:false,
    ILLTH:false, IMAGE:false, IMAGO:false, IMAMS:false, IMARI:false, IMAUM:false, IMBAR:false, IMBED:false, IMBUE:false, IMIDE:false,
    IMIDO:false, IMIDS:false, IMINE:false, IMINO:false, IMMEW:false, IMMIT:false, IMMIX:false, IMPED:false, IMPEL:false, IMPIS:false,
    IMPLY:false, IMPOT:false, IMPRO:false, IMSHI:false, IMSHY:false, INANE:false, INAPT:false, INARM:false, INBOX:false, INBYE:false,
    INCEL:false, INCLE:false, INCOG:false, INCUR:false, INCUS:false, INCUT:false, INDEW:false, INDEX:false, INDIA:false, INDIE:false,
    INDOL:false, INDOW:false, INDRI:false, INDUE:false, INEPT:false, INERM:false, INERT:false, INFER:false, INFIX:false, INFOS:false,
    INFRA:false, INGAN:false, INGLE:false, INGOT:false, INION:false, INKED:false, INKER:false, INKLE:false, INLAY:false, INLET:false,
    INNED:false, INNER:false, INNIT:false, INORB:false, INPUT:false, INRUN:false, INSET:false, INSPO:false, INTEL:false, INTER:false,
    INTIL:false, INTIS:false, INTRA:false, INTRO:false, INULA:false, INURE:false, INURN:false, INUST:false, INVAR:false, INWIT:false,
    IODIC:false, IODID:false, IODIN:false, IONIC:false, IOTAS:false, IPPON:false, IRADE:false, IRATE:false, IRIDS:false, IRING:false,
    IRKED:false, IROKO:false, IRONE:false, IRONS:false, IRONY:false, ISBAS:false, ISHES:false, ISLED:false, ISLES:false, ISLET:false,
    ISNAE:false, ISSEI:false, ISSUE:false, ISTLE:false, ITCHY:false, ITEMS:false, ITHER:false, IVIED:false, IVIES:false, IVORY:false,
    IXIAS:false, IXNAY:false, IXORA:false, IXTLE:false, IZARD:false, IZARS:false, IZZAT:false, JAAPS:false, JABOT:false, JACAL:false,
    JACKS:false, JACKY:false, JADED:false, JADES:false, JAFAS:false, JAFFA:false, JAGAS:false, JAGER:false, JAGGS:false, JAGGY:false,
    JAGIR:false, JAGRA:false, JAILS:false, JAKER:false, JAKES:false, JAKEY:false, JALAP:false, JALOP:false, JAMBE:false, JAMBO:false,
    JAMBS:false, JAMBU:false, JAMES:false, JAMMY:false, JAMON:false, JANES:false, JANNS:false, JANNY:false, JANTY:false, JAPAN:false,
    JAPED:false, JAPER:false, JAPES:false, JARKS:false, JARLS:false, JARPS:false, JARTA:false, JARUL:false, JASEY:false, JASPE:false,
    JASPS:false, JATOS:false, JAUKS:false, JAUNT:false, JAUPS:false, JAVAS:false, JAVEL:false, JAWAN:false, JAWED:false, JAXIE:false,
    JAZZY:false, JEANS:false, JEATS:false, JEBEL:false, JEDIS:false, JEELS:false, JEELY:false, JEEPS:false, JEERS:false, JEEZE:false,
    JEFES:false, JEFFS:false, JEHAD:false, JEHUS:false, JELAB:false, JELLO:false, JELLS:false, JELLY:false, JEMBE:false, JEMMY:false,
    JENNY:false, JEONS:false, JERID:false, JERKS:false, JERKY:false, JERRY:false, JESSE:false, JESTS:false, JESUS:false, JETES:false,
    JETON:false, JETTY:false, JEUNE:false, JEWED:false, JEWEL:false, JEWIE:false, JHALA:false, JIAOS:false, JIBBA:false, JIBBS:false,
    JIBED:false, JIBER:false, JIBES:false, JIFFS:false, JIFFY:false, JIGGY:false, JIGOT:false, JIHAD:false, JILLS:false, JILTS:false,
    JIMMY:false, JIMPY:false, JINGO:false, JINKS:false, JINNE:false, JINNI:false, JINNS:false, JIRDS:false, JIRGA:false, JIRRE:false,
    JISMS:false, JIVED:false, JIVER:false, JIVES:false, JIVEY:false, JNANA:false, JOBED:false, JOBES:false, JOCKO:false, JOCKS:false,
    JOCKY:false, JOCOS:false, JODEL:false, JOEYS:false, JOHNS:false, JOINS:false, JOINT:false, JOIST:false, JOKED:false, JOKER:false,
    JOKES:false, JOKEY:false, JOKOL:false, JOLED:false, JOLES:false, JOLLS:false, JOLLY:false, JOLTS:false, JOLTY:false, JOMON:false,
    JOMOS:false, JONES:false, JONGS:false, JONTY:false, JOOKS:false, JORAM:false, JORUM:false, JOTAS:false, JOTTY:false, JOTUN:false,
    JOUAL:false, JOUGS:false, JOUKS:false, JOULE:false, JOURS:false, JOUST:false, JOWAR:false, JOWED:false, JOWLS:false, JOWLY:false,
    JOYED:false, JUBAS:false, JUBES:false, JUCOS:false, JUDAS:false, JUDGE:false, JUDGY:false, JUDOS:false, JUGAL:false, JUGUM:false,
    JUICE:false, JUICY:false, JUJUS:false, JUKED:false, JUKES:false, JUKUS:false, JULEP:false, JUMAR:false, JUMBO:false, JUMBY:false,
    JUMPS:false, JUMPY:false, JUNCO:false, JUNKS:false, JUNKY:false, JUNTA:false, JUNTO:false, JUPES:false, JUPON:false, JURAL:false,
    JURAT:false, JUREL:false, JURES:false, JUROR:false, JUSTS:false, JUTES:false, JUTTY:false, JUVES:false, JUVIE:false, KAAMA:false,
    KABAB:false, KABAR:false, KABOB:false, KACHA:false, KACKS:false, KADAI:false, KADES:false, KADIS:false, KAFIR:false, KAGOS:false,
    KAGUS:false, KAHAL:false, KAIAK:false, KAIDS:false, KAIES:false, KAIFS:false, KAIKA:false, KAIKS:false, KAILS:false, KAIMS:false,
    KAING:false, KAINS:false, KAKAS:false, KAKIS:false, KALAM:false, KALES:false, KALIF:false, KALIS:false, KALPA:false, KAMAS:false,
    KAMES:false, KAMIK:false, KAMIS:false, KAMME:false, KANAE:false, KANAS:false, KANDY:false, KANEH:false, KANES:false, KANGA:false,
    KANGS:false, KANJI:false, KANTS:false, KANZU:false, KAONS:false, KAPAS:false, KAPHS:false, KAPOK:false, KAPOW:false, KAPPA:false,
    KAPUS:false, KAPUT:false, KARAS:false, KARAT:false, KARKS:false, KARMA:false, KARNS:false, KAROO:false, KAROS:false, KARRI:false,
    KARST:false, KARSY:false, KARTS:false, KARZY:false, KASHA:false, KASME:false, KATAL:false, KATAS:false, KATIS:false, KATTI:false,
    KAUGH:false, KAURI:false, KAURU:false, KAURY:false, KAVAL:false, KAVAS:false, KAWAS:false, KAWAU:false, KAWED:false, KAYAK:false,
    KAYLE:false, KAYOS:false, KAZIS:false, KAZOO:false, KBARS:false, KEBAB:false, KEBAR:false, KEBOB:false, KECKS:false, KEDGE:false,
    KEDGY:false, KEECH:false, KEEFS:false, KEEKS:false, KEELS:false, KEEMA:false, KEENO:false, KEENS:false, KEEPS:false, KEETS:false,
    KEEVE:false, KEFIR:false, KEHUA:false, KEIRS:false, KELEP:false, KELIM:false, KELLS:false, KELLY:false, KELPS:false, KELPY:false,
    KELTS:false, KELTY:false, KEMBO:false, KEMBS:false, KEMPS:false, KEMPT:false, KEMPY:false, KENAF:false, KENCH:false, KENDO:false,
    KENOS:false, KENTE:false, KENTS:false, KEPIS:false, KERBS:false, KEREL:false, KERFS:false, KERKY:false, KERMA:false, KERNE:false,
    KERNS:false, KEROS:false, KERRY:false, KERVE:false, KESAR:false, KESTS:false, KETAS:false, KETCH:false, KETES:false, KETOL:false,
    KEVEL:false, KEVIL:false, KEXES:false, KEYED:false, KEYER:false, KHADI:false, KHAFS:false, KHAKI:false, KHANS:false, KHAPH:false,
    KHATS:false, KHAYA:false, KHAZI:false, KHEDA:false, KHETH:false, KHETS:false, KHOJA:false, KHORS:false, KHOUM:false, KHUDS:false,
    KIAAT:false, KIACK:false, KIANG:false, KIBBE:false, KIBBI:false, KIBEI:false, KIBES:false, KIBLA:false, KICKS:false, KICKY:false,
    KIDDO:false, KIDDY:false, KIDEL:false, KIDGE:false, KIEFS:false, KIERS:false, KIEVE:false, KIEVS:false, KIGHT:false, KIKES:false,
    KIKOI:false, KILEY:false, KILIM:false, KILLS:false, KILNS:false, KILOS:false, KILPS:false, KILTS:false, KILTY:false, KIMBO:false,
    KINAS:false, KINDA:false, KINDS:false, KINDY:false, KINES:false, KINGS:false, KININ:false, KINKS:false, KINKY:false, KINOS:false,
    KIORE:false, KIOSK:false, KIPES:false, KIPPA:false, KIPPS:false, KIRBY:false, KIRKS:false, KIRNS:false, KIRRI:false, KISAN:false,
    KISSY:false, KISTS:false, KITED:false, KITER:false, KITES:false, KITHE:false, KITHS:false, KITTY:false, KITUL:false, KIVAS:false,
    KIWIS:false, KLANG:false, KLAPS:false, KLETT:false, KLICK:false, KLIEG:false, KLIKS:false, KLONG:false, KLOOF:false, KLUGE:false,
    KLUTZ:false, KNACK:false, KNAGS:false, KNAPS:false, KNARL:false, KNARS:false, KNAUR:false, KNAVE:false, KNAWE:false, KNEAD:false,
    KNEED:false, KNEEL:false, KNEES:false, KNELL:false, KNELT:false, KNIFE:false, KNISH:false, KNITS:false, KNIVE:false, KNOBS:false,
    KNOCK:false, KNOLL:false, KNOPS:false, KNOSP:false, KNOTS:false, KNOUT:false, KNOWE:false, KNOWN:false, KNOWS:false, KNUBS:false,
    KNURL:false, KNURR:false, KNURS:false, KNUTS:false, KOALA:false, KOANS:false, KOAPS:false, KOBAN:false, KOBOS:false, KOELS:false,
    KOFFS:false, KOFTA:false, KOGAL:false, KOHAS:false, KOHEN:false, KOHLS:false, KOINE:false, KOJIS:false, KOKAM:false, KOKAS:false,
    KOKER:false, KOKRA:false, KOKUM:false, KOLAS:false, KOLOS:false, KOMBU:false, KONBU:false, KONDO:false, KONKS:false, KOOKS:false,
    KOOKY:false, KOORI:false, KOPEK:false, KOPHS:false, KOPJE:false, KOPPA:false, KORAI:false, KORAS:false, KORAT:false, KORES:false,
    KORMA:false, KOROS:false, KORUN:false, KORUS:false, KOSES:false, KOTCH:false, KOTOS:false, KOTOW:false, KOURA:false, KRAAL:false,
    KRABS:false, KRAFT:false, KRAIS:false, KRAIT:false, KRANG:false, KRANS:false, KRANZ:false, KRAUT:false, KRAYS:false, KREEP:false,
    KRENG:false, KREWE:false, KRILL:false, KRONA:false, KRONE:false, KROON:false, KRUBI:false, KRUNK:false, KSARS:false, KUBIE:false,
    KUDOS:false, KUDUS:false, KUDZU:false, KUFIS:false, KUGEL:false, KUIAS:false, KUKRI:false, KUKUS:false, KULAK:false, KULAN:false,
    KULAS:false, KULFI:false, KUMIS:false, KUMYS:false, KURIS:false, KURRE:false, KURTA:false, KURUS:false, KUSSO:false, KUTAS:false,
    KUTCH:false, KUTIS:false, KUTUS:false, KUZUS:false, KVASS:false, KVELL:false, KWELA:false, KYACK:false, KYAKS:false, KYANG:false,
    KYARS:false, KYATS:false, KYBOS:false, KYDST:false, KYLES:false, KYLIE:false, KYLIN:false, KYLIX:false, KYLOE:false, KYNDE:false,
    KYNDS:false, KYPES:false, KYRIE:false, KYTES:false, KYTHE:false, LAARI:false, LABDA:false, LABEL:false, LABIA:false, LABIS:false,
    LABOR:false, LABRA:false, LACED:false, LACER:false, LACES:false, LACET:false, LACEY:false, LACKS:false, LADDY:false, LADED:false,
    LADEN:false, LADER:false, LADES:false, LADLE:false, LAERS:false, LAEVO:false, LAGAN:false, LAGER:false, LAHAL:false, LAHAR:false,
    LAICH:false, LAICS:false, LAIDS:false, LAIGH:false, LAIKA:false, LAIKS:false, LAIRD:false, LAIRS:false, LAIRY:false, LAITH:false,
    LAITY:false, LAKED:false, LAKER:false, LAKES:false, LAKHS:false, LAKIN:false, LAKSA:false, LALDY:false, LALLS:false, LAMAS:false,
    LAMBS:false, LAMBY:false, LAMED:false, LAMER:false, LAMES:false, LAMIA:false, LAMMY:false, LAMPS:false, LANAI:false, LANAS:false,
    LANCE:false, LANCH:false, LANDE:false, LANDS:false, LANES:false, LANKS:false, LANKY:false, LANTS:false, LAPEL:false, LAPIN:false,
    LAPIS:false, LAPJE:false, LAPSE:false, LARCH:false, LARDS:false, LARDY:false, LAREE:false, LARES:false, LARGE:false, LARGO:false,
    LARIS:false, LARKS:false, LARKY:false, LARNS:false, LARNT:false, LARUM:false, LARVA:false, LASED:false, LASER:false, LASES:false,
    LASSI:false, LASSO:false, LASSU:false, LASSY:false, LASTS:false, LATAH:false, LATCH:false, LATED:false, LATEN:false, LATER:false,
    LATEX:false, LATHE:false, LATHI:false, LATHS:false, LATHY:false, LATKE:false, LATTE:false, LATUS:false, LAUAN:false, LAUCH:false,
    LAUDS:false, LAUFS:false, LAUGH:false, LAUND:false, LAURA:false, LAVAL:false, LAVAS:false, LAVED:false, LAVER:false, LAVES:false,
    LAVRA:false, LAVVY:false, LAWED:false, LAWER:false, LAWIN:false, LAWKS:false, LAWNS:false, LAWNY:false, LAXED:false, LAXER:false,
    LAXES:false, LAXLY:false, LAYED:false, LAYER:false, LAYIN:false, LAYUP:false, LAZAR:false, LAZED:false, LAZES:false, LAZOS:false,
    LAZZI:false, LAZZO:false, LEACH:false, LEADS:false, LEADY:false, LEAFS:false, LEAFY:false, LEAKS:false, LEAKY:false, LEAMS:false,
    LEANS:false, LEANT:false, LEANY:false, LEAPS:false, LEAPT:false, LEARE:false, LEARN:false, LEARS:false, LEARY:false, LEASE:false,
    LEASH:false, LEAST:false, LEATS:false, LEAVE:false, LEAVY:false, LEAZE:false, LEBEN:false, LECCY:false, LEDES:false, LEDGE:false,
    LEDGY:false, LEDUM:false, LEEAR:false, LEECH:false, LEEKS:false, LEEPS:false, LEERS:false, LEERY:false, LEESE:false, LEETS:false,
    LEEZE:false, LEFTE:false, LEFTS:false, LEFTY:false, LEGAL:false, LEGER:false, LEGES:false, LEGGE:false, LEGGO:false, LEGGY:false,
    LEGIT:false, LEHRS:false, LEHUA:false, LEIRS:false, LEISH:false, LEMAN:false, LEMED:false, LEMEL:false, LEMES:false, LEMMA:false,
    LEMME:false, LEMON:false, LEMUR:false, LENDS:false, LENES:false, LENGS:false, LENIS:false, LENOS:false, LENSE:false, LENTI:false,
    LENTO:false, LEONE:false, LEPER:false, LEPID:false, LEPRA:false, LEPTA:false, LERED:false, LERES:false, LERPS:false, LESBO:false,
    LESES:false, LESTS:false, LETCH:false, LETHE:false, LETUP:false, LEUCH:false, LEUCO:false, LEUDS:false, LEUGH:false, LEVAS:false,
    LEVEE:false, LEVEL:false, LEVER:false, LEVES:false, LEVIN:false, LEVIS:false, LEWIS:false, LEXES:false, LEXIS:false, LEZES:false,
    LEZZA:false, LEZZY:false, LIANA:false, LIANE:false, LIANG:false, LIARD:false, LIARS:false, LIART:false, LIBEL:false, LIBER:false,
    LIBRA:false, LIBRI:false, LICHI:false, LICHT:false, LICIT:false, LICKS:false, LIDAR:false, LIDOS:false, LIEFS:false, LIEGE:false,
    LIENS:false, LIERS:false, LIEUS:false, LIEVE:false, LIFER:false, LIFES:false, LIFTS:false, LIGAN:false, LIGER:false, LIGGE:false,
    LIGHT:false, LIGNE:false, LIKED:false, LIKEN:false, LIKER:false, LIKES:false, LIKIN:false, LILAC:false, LILLS:false, LILOS:false,
    LILTS:false, LIMAN:false, LIMAS:false, LIMAX:false, LIMBA:false, LIMBI:false, LIMBO:false, LIMBS:false, LIMBY:false, LIMED:false,
    LIMEN:false, LIMES:false, LIMEY:false, LIMIT:false, LIMMA:false, LIMNS:false, LIMOS:false, LIMPA:false, LIMPS:false, LINAC:false,
    LINCH:false, LINDS:false, LINDY:false, LINED:false, LINEN:false, LINER:false, LINES:false, LINEY:false, LINGA:false, LINGO:false,
    LINGS:false, LINGY:false, LININ:false, LINKS:false, LINKY:false, LINNS:false, LINNY:false, LINOS:false, LINTS:false, LINTY:false,
    LINUM:false, LINUX:false, LIONS:false, LIPAS:false, LIPES:false, LIPID:false, LIPIN:false, LIPOS:false, LIPPY:false, LIRAS:false,
    LIRKS:false, LIROT:false, LISKS:false, LISLE:false, LISPS:false, LISTS:false, LITAI:false, LITAS:false, LITED:false, LITER:false,
    LITES:false, LITHE:false, LITHO:false, LITHS:false, LITRE:false, LIVED:false, LIVEN:false, LIVER:false, LIVES:false, LIVID:false,
    LIVOR:false, LIVRE:false, LLAMA:false, LLANO:false, LOACH:false, LOADS:false, LOAFS:false, LOAMS:false, LOAMY:false, LOANS:false,
    LOAST:false, LOATH:false, LOAVE:false, LOBAR:false, LOBBY:false, LOBED:false, LOBES:false, LOBOS:false, LOBUS:false, LOCAL:false,
    LOCHE:false, LOCHS:false, LOCIE:false, LOCIS:false, LOCKS:false, LOCOS:false, LOCUM:false, LOCUS:false, LODEN:false, LODES:false,
    LODGE:false, LOESS:false, LOFTS:false, LOFTY:false, LOGAN:false, LOGES:false, LOGGY:false, LOGIA:false, LOGIC:false, LOGIE:false,
    LOGIN:false, LOGOI:false, LOGON:false, LOGOS:false, LOHAN:false, LOIDS:false, LOINS:false, LOIPE:false, LOIRS:false, LOKES:false,
    LOLLS:false, LOLLY:false, LOLOG:false, LOMAS:false, LOMED:false, LOMES:false, LONER:false, LONGA:false, LONGE:false, LONGS:false,
    LOOBY:false, LOOED:false, LOOEY:false, LOOFA:false, LOOFS:false, LOOIE:false, LOOKS:false, LOOKY:false, LOOMS:false, LOONS:false,
    LOONY:false, LOOPS:false, LOOPY:false, LOORD:false, LOOSE:false, LOOTS:false, LOPED:false, LOPER:false, LOPES:false, LOPPY:false,
    LORAL:false, LORAN:false, LORDS:false, LORDY:false, LOREL:false, LORES:false, LORIC:false, LORIS:false, LORRY:false, LOSED:false,
    LOSEL:false, LOSEN:false, LOSER:false, LOSES:false, LOSSY:false, LOTAH:false, LOTAS:false, LOTES:false, LOTIC:false, LOTOS:false,
    LOTSA:false, LOTTA:false, LOTTE:false, LOTTO:false, LOTUS:false, LOUED:false, LOUGH:false, LOUIE:false, LOUIS:false, LOUMA:false,
    LOUND:false, LOUNS:false, LOUPE:false, LOUPS:false, LOURE:false, LOURS:false, LOURY:false, LOUSE:false, LOUSY:false, LOUTS:false,
    LOVAT:false, LOVED:false, LOVER:false, LOVES:false, LOVEY:false, LOVIE:false, LOWAN:false, LOWED:false, LOWER:false, LOWES:false,
    LOWLY:false, LOWND:false, LOWNE:false, LOWNS:false, LOWPS:false, LOWRY:false, LOWSE:false, LOWTS:false, LOXED:false, LOXES:false,
    LOYAL:false, LOZEN:false, LUACH:false, LUAUS:false, LUBED:false, LUBES:false, LUBRA:false, LUCES:false, LUCID:false, LUCKS:false,
    LUCKY:false, LUCRE:false, LUDES:false, LUDIC:false, LUDOS:false, LUFFA:false, LUFFS:false, LUGED:false, LUGER:false, LUGES:false,
    LULLS:false, LULUS:false, LUMAS:false, LUMBI:false, LUMEN:false, LUMME:false, LUMMY:false, LUMPS:false, LUMPY:false, LUNAR:false,
    LUNAS:false, LUNCH:false, LUNES:false, LUNET:false, LUNGE:false, LUNGI:false, LUNGS:false, LUNKS:false, LUNTS:false, LUPIN:false,
    LUPUS:false, LURCH:false, LURED:false, LURER:false, LURES:false, LUREX:false, LURGI:false, LURGY:false, LURID:false, LURKS:false,
    LURRY:false, LURVE:false, LUSER:false, LUSHY:false, LUSKS:false, LUSTS:false, LUSTY:false, LUSUS:false, LUTEA:false, LUTED:false,
    LUTER:false, LUTES:false, LUVVY:false, LUXED:false, LUXER:false, LUXES:false, LWEIS:false, LYAMS:false, LYARD:false, LYART:false,
    LYASE:false, LYCEA:false, LYCEE:false, LYCRA:false, LYING:false, LYMES:false, LYMPH:false, LYNCH:false, LYNES:false, LYRES:false,
    LYRIC:false, LYSED:false, LYSES:false, LYSIN:false, LYSIS:false, LYSOL:false, LYSSA:false, LYTED:false, LYTES:false, LYTHE:false,
    LYTIC:false, LYTTA:false, MAAED:false, MAARE:false, MAARS:false, MABES:false, MACAS:false, MACAW:false, MACED:false, MACER:false,
    MACES:false, MACHE:false, MACHI:false, MACHO:false, MACHS:false, MACKS:false, MACLE:false, MACON:false, MACRO:false, MADAM:false,
    MADGE:false, MADID:false, MADLY:false, MADRE:false, MAERL:false, MAFIA:false, MAFIC:false, MAGES:false, MAGGS:false, MAGIC:false,
    MAGMA:false, MAGOT:false, MAGUS:false, MAHOE:false, MAHUA:false, MAHWA:false, MAIDS:false, MAIKO:false, MAIKS:false, MAILE:false,
    MAILL:false, MAILS:false, MAIMS:false, MAINS:false, MAIRE:false, MAIRS:false, MAISE:false, MAIST:false, MAIZE:false, MAJOR:false,
    MAKAR:false, MAKER:false, MAKES:false, MAKIS:false, MAKOS:false, MALAM:false, MALAR:false, MALAS:false, MALAX:false, MALES:false,
    MALIC:false, MALIK:false, MALIS:false, MALLS:false, MALMS:false, MALMY:false, MALTS:false, MALTY:false, MALUS:false, MALVA:false,
    MALWA:false, MAMAS:false, MAMBA:false, MAMBO:false, MAMEE:false, MAMEY:false, MAMIE:false, MAMMA:false, MAMMY:false, MANAS:false,
    MANAT:false, MANDI:false, MANEB:false, MANED:false, MANEH:false, MANES:false, MANET:false, MANGA:false, MANGE:false, MANGO:false,
    MANGS:false, MANGY:false, MANIA:false, MANIC:false, MANIS:false, MANKY:false, MANLY:false, MANNA:false, MANOR:false, MANOS:false,
    MANSE:false, MANTA:false, MANTO:false, MANTY:false, MANUL:false, MANUS:false, MAPAU:false, MAPLE:false, MAQUI:false, MARAE:false,
    MARAH:false, MARAS:false, MARCH:false, MARCS:false, MARDY:false, MARES:false, MARGE:false, MARGS:false, MARIA:false, MARID:false,
    MARKA:false, MARKS:false, MARLE:false, MARLS:false, MARLY:false, MARMS:false, MARON:false, MAROR:false, MARRA:false, MARRI:false,
    MARRY:false, MARSE:false, MARSH:false, MARTS:false, MARVY:false, MASAS:false, MASED:false, MASER:false, MASES:false, MASHY:false,
    MASKS:false, MASON:false, MASSA:false, MASSE:false, MASSY:false, MASTS:false, MASTY:false, MASUS:false, MATAI:false, MATCH:false,
    MATED:false, MATER:false, MATES:false, MATEY:false, MATHS:false, MATIN:false, MATLO:false, MATTE:false, MATTS:false, MATZA:false,
    MATZO:false, MAUBY:false, MAUDS:false, MAULS:false, MAUND:false, MAURI:false, MAUSY:false, MAUTS:false, MAUVE:false, MAUZY:false,
    MAVEN:false, MAVIE:false, MAVIN:false, MAVIS:false, MAWED:false, MAWKS:false, MAWKY:false, MAWNS:false, MAWRS:false, MAXED:false,
    MAXES:false, MAXIM:false, MAXIS:false, MAYAN:false, MAYAS:false, MAYBE:false, MAYED:false, MAYOR:false, MAYOS:false, MAYST:false,
    MAZED:false, MAZER:false, MAZES:false, MAZEY:false, MAZUT:false, MBIRA:false, MEADS:false, MEALS:false, MEALY:false, MEANE:false,
    MEANS:false, MEANT:false, MEANY:false, MEARE:false, MEASE:false, MEATH:false, MEATS:false, MEATY:false, MEBOS:false, MECCA:false,
    MECHS:false, MECKS:false, MEDAL:false, MEDIA:false, MEDIC:false, MEDII:false, MEDLE:false, MEEDS:false, MEERS:false, MEETS:false,
    MEFFS:false, MEINS:false, MEINT:false, MEINY:false, MEITH:false, MEKKA:false, MELAS:false, MELBA:false, MELDS:false, MELEE:false,
    MELIC:false, MELIK:false, MELLS:false, MELON:false, MELTS:false, MELTY:false, MEMES:false, MEMOS:false, MENAD:false, MENDS:false,
    MENED:false, MENES:false, MENGE:false, MENGS:false, MENSA:false, MENSE:false, MENSH:false, MENTA:false, MENTO:false, MENUS:false,
    MEOUS:false, MEOWS:false, MERCH:false, MERCS:false, MERCY:false, MERDE:false, MERED:false, MEREL:false, MERER:false, MERES:false,
    MERGE:false, MERIL:false, MERIS:false, MERIT:false, MERKS:false, MERLE:false, MERLS:false, MERRY:false, MERSE:false, MESAL:false,
    MESAS:false, MESEL:false, MESES:false, MESHY:false, MESIC:false, MESNE:false, MESON:false, MESSY:false, MESTO:false, METAL:false,
    METED:false, METER:false, METES:false, METHO:false, METHS:false, METIC:false, METIF:false, METIS:false, METOL:false, METRE:false,
    METRO:false, MEUSE:false, MEVED:false, MEVES:false, MEWED:false, MEWLS:false, MEYNT:false, MEZES:false, MEZZE:false, MEZZO:false,
    MHORR:false, MIAOU:false, MIAOW:false, MIASM:false, MIAUL:false, MICAS:false, MICHE:false, MICHT:false, MICKS:false, MICKY:false,
    MICOS:false, MICRA:false, MICRO:false, MIDDY:false, MIDGE:false, MIDGY:false, MIDIS:false, MIDST:false, MIENS:false, MIEVE:false,
    MIFFS:false, MIFFY:false, MIFTY:false, MIGGS:false, MIGHT:false, MIHAS:false, MIHIS:false, MIKED:false, MIKES:false, MIKRA:false,
    MIKVA:false, MILCH:false, MILDS:false, MILER:false, MILES:false, MILFS:false, MILIA:false, MILKO:false, MILKS:false, MILKY:false,
    MILLE:false, MILLS:false, MILOR:false, MILOS:false, MILPA:false, MILTS:false, MILTY:false, MILTZ:false, MIMED:false, MIMEO:false,
    MIMER:false, MIMES:false, MIMIC:false, MIMSY:false, MINAE:false, MINAR:false, MINAS:false, MINCE:false, MINCY:false, MINDS:false,
    MINED:false, MINER:false, MINES:false, MINGE:false, MINGS:false, MINGY:false, MINIM:false, MINIS:false, MINKE:false, MINKS:false,
    MINNY:false, MINOR:false, MINOS:false, MINTS:false, MINTY:false, MINUS:false, MIRED:false, MIRES:false, MIREX:false, MIRID:false,
    MIRIN:false, MIRKS:false, MIRKY:false, MIRLY:false, MIROS:false, MIRTH:false, MIRVS:false, MIRZA:false, MISCH:false, MISDO:false,
    MISER:false, MISES:false, MISGO:false, MISOS:false, MISSA:false, MISSY:false, MISTS:false, MISTY:false, MITCH:false, MITER:false,
    MITES:false, MITIS:false, MITRE:false, MITTS:false, MIXED:false, MIXEN:false, MIXER:false, MIXES:false, MIXTE:false, MIXUP:false,
    MIZEN:false, MIZZY:false, MNEME:false, MOANS:false, MOATS:false, MOBBY:false, MOBES:false, MOBEY:false, MOBIE:false, MOBLE:false,
    MOCHA:false, MOCHI:false, MOCHS:false, MOCHY:false, MOCKS:false, MODAL:false, MODEL:false, MODEM:false, MODER:false, MODES:false,
    MODGE:false, MODII:false, MODUS:false, MOERS:false, MOFOS:false, MOGGY:false, MOGUL:false, MOHEL:false, MOHOS:false, MOHRS:false,
    MOHUA:false, MOHUR:false, MOILE:false, MOILS:false, MOIRA:false, MOIRE:false, MOIST:false, MOITS:false, MOJOS:false, MOKES:false,
    MOKIS:false, MOKOS:false, MOLAL:false, MOLAR:false, MOLAS:false, MOLDS:false, MOLDY:false, MOLED:false, MOLES:false, MOLLA:false,
    MOLLS:false, MOLLY:false, MOLTO:false, MOLTS:false, MOLYS:false, MOMES:false, MOMMA:false, MOMMY:false, MOMUS:false, MONAD:false,
    MONAL:false, MONAS:false, MONDE:false, MONDO:false, MONER:false, MONEY:false, MONGO:false, MONGS:false, MONIC:false, MONIE:false,
    MONKS:false, MONOS:false, MONTE:false, MONTH:false, MONTY:false, MOOBS:false, MOOCH:false, MOODS:false, MOODY:false, MOOED:false,
    MOOKS:false, MOOLA:false, MOOLI:false, MOOLS:false, MOOLY:false, MOONG:false, MOONS:false, MOONY:false, MOOPS:false, MOORS:false,
    MOORY:false, MOOSE:false, MOOTS:false, MOOVE:false, MOPED:false, MOPER:false, MOPES:false, MOPEY:false, MOPPY:false, MOPSY:false,
    MOPUS:false, MORAE:false, MORAL:false, MORAS:false, MORAT:false, MORAY:false, MOREL:false, MORES:false, MORIA:false, MORNE:false,
    MORNS:false, MORON:false, MORPH:false, MORRA:false, MORRO:false, MORSE:false, MORTS:false, MOSED:false, MOSES:false, MOSEY:false,
    MOSKS:false, MOSSO:false, MOSSY:false, MOSTE:false, MOSTS:false, MOTED:false, MOTEL:false, MOTEN:false, MOTES:false, MOTET:false,
    MOTEY:false, MOTHS:false, MOTHY:false, MOTIF:false, MOTIS:false, MOTOR:false, MOTTE:false, MOTTO:false, MOTTS:false, MOTTY:false,
    MOTUS:false, MOTZA:false, MOUCH:false, MOUES:false, MOULD:false, MOULS:false, MOULT:false, MOUND:false, MOUNT:false, MOUPS:false,
    MOURN:false, MOUSE:false, MOUST:false, MOUSY:false, MOUTH:false, MOVED:false, MOVER:false, MOVES:false, MOVIE:false, MOWAS:false,
    MOWED:false, MOWER:false, MOWRA:false, MOXAS:false, MOXIE:false, MOYAS:false, MOYLE:false, MOYLS:false, MOZED:false, MOZES:false,
    MOZOS:false, MPRET:false, MUCHO:false, MUCIC:false, MUCID:false, MUCIN:false, MUCKS:false, MUCKY:false, MUCOR:false, MUCRO:false,
    MUCUS:false, MUDDY:false, MUDGE:false, MUDIR:false, MUDRA:false, MUFFS:false, MUFTI:false, MUGGA:false, MUGGS:false, MUGGY:false,
    MUHLY:false, MUIDS:false, MUILS:false, MUIRS:false, MUIST:false, MUJIK:false, MULCH:false, MULCT:false, MULED:false, MULES:false,
    MULEY:false, MULGA:false, MULIE:false, MULLA:false, MULLS:false, MULSE:false, MULSH:false, MUMMS:false, MUMMY:false, MUMPS:false,
    MUMSY:false, MUMUS:false, MUNCH:false, MUNGA:false, MUNGE:false, MUNGO:false, MUNGS:false, MUNIS:false, MUNTS:false, MUNTU:false,
    MUONS:false, MURAL:false, MURAS:false, MURED:false, MURES:false, MUREX:false, MURID:false, MURKS:false, MURKY:false, MURLS:false,
    MURLY:false, MURRA:false, MURRE:false, MURRI:false, MURRS:false, MURRY:false, MURTI:false, MURVA:false, MUSAR:false, MUSCA:false,
    MUSED:false, MUSER:false, MUSES:false, MUSET:false, MUSHA:false, MUSHY:false, MUSIC:false, MUSIT:false, MUSKS:false, MUSKY:false,
    MUSOS:false, MUSSE:false, MUSSY:false, MUSTH:false, MUSTS:false, MUSTY:false, MUTCH:false, MUTED:false, MUTER:false, MUTES:false,
    MUTHA:false, MUTIS:false, MUTON:false, MUTTS:false, MUXED:false, MUXES:false, MUZAK:false, MUZZY:false, MVULE:false, MYALL:false,
    MYLAR:false, MYNAH:false, MYNAS:false, MYOID:false, MYOMA:false, MYOPE:false, MYOPS:false, MYOPY:false, MYRRH:false, MYSID:false,
    MYTHI:false, MYTHS:false, MYTHY:false, MYXOS:false, MZEES:false, NAAMS:false, NAANS:false, NABES:false, NABIS:false, NABKS:false,
    NABLA:false, NABOB:false, NACHE:false, NACHO:false, NACRE:false, NADAS:false, NADIR:false, NAEVE:false, NAEVI:false, NAFFS:false,
    NAGAS:false, NAGGY:false, NAGOR:false, NAHAL:false, NAIAD:false, NAIFS:false, NAIKS:false, NAILS:false, NAIRA:false, NAIRU:false,
    NAIVE:false, NAKED:false, NAKER:false, NAKFA:false, NALAS:false, NALED:false, NALLA:false, NAMED:false, NAMER:false, NAMES:false,
    NAMMA:false, NAMUS:false, NANAS:false, NANCE:false, NANCY:false, NANDU:false, NANNA:false, NANNY:false, NANOS:false, NANUA:false,
    NAPAS:false, NAPED:false, NAPES:false, NAPOO:false, NAPPA:false, NAPPE:false, NAPPY:false, NARAS:false, NARCO:false, NARCS:false,
    NARDS:false, NARES:false, NARIC:false, NARIS:false, NARKS:false, NARKY:false, NARRE:false, NASAL:false, NASHI:false, NASTY:false,
    NATAL:false, NATCH:false, NATES:false, NATIS:false, NATTY:false, NAUCH:false, NAUNT:false, NAVAL:false, NAVAR:false, NAVEL:false,
    NAVES:false, NAVEW:false, NAVVY:false, NAWAB:false, NAZES:false, NAZIR:false, NAZIS:false, NDUJA:false, NEAFE:false, NEALS:false,
    NEAPS:false, NEARS:false, NEATH:false, NEATS:false, NEBEK:false, NEBEL:false, NECKS:false, NEDDY:false, NEEDS:false, NEEDY:false,
    NEELD:false, NEELE:false, NEEMB:false, NEEMS:false, NEEPS:false, NEESE:false, NEEZE:false, NEGRO:false, NEGUS:false, NEIFS:false,
    NEIGH:false, NEIST:false, NEIVE:false, NELIS:false, NELLY:false, NEMAS:false, NEMNS:false, NEMPT:false, NENES:false, NEONS:false,
    NEPER:false, NEPIT:false, NERAL:false, NERDS:false, NERDY:false, NERKA:false, NERKS:false, NEROL:false, NERTS:false, NERTZ:false,
    NERVE:false, NERVY:false, NESTS:false, NETES:false, NETOP:false, NETTS:false, NETTY:false, NEUKS:false, NEUME:false, NEUMS:false,
    NEVEL:false, NEVER:false, NEVES:false, NEVUS:false, NEWBS:false, NEWED:false, NEWEL:false, NEWER:false, NEWIE:false, NEWLY:false,
    NEWSY:false, NEWTS:false, NEXTS:false, NEXUS:false, NGAIO:false, NGANA:false, NGATI:false, NGOMA:false, NGWEE:false, NICAD:false,
    NICER:false, NICHE:false, NICHT:false, NICKS:false, NICOL:false, NIDAL:false, NIDED:false, NIDES:false, NIDOR:false, NIDUS:false,
    NIECE:false, NIEFS:false, NIEVE:false, NIFES:false, NIFFS:false, NIFFY:false, NIFTY:false, NIGER:false, NIGHS:false, NIGHT:false,
    NIHIL:false, NIKAB:false, NIKAH:false, NIKAU:false, NILLS:false, NIMBI:false, NIMBS:false, NIMPS:false, NINER:false, NINES:false,
    NINJA:false, NINNY:false, NINON:false, NINTH:false, NIPAS:false, NIPPY:false, NIQAB:false, NIRLS:false, NIRLY:false, NISEI:false,
    NISSE:false, NISUS:false, NITER:false, NITES:false, NITID:false, NITON:false, NITRE:false, NITRO:false, NITRY:false, NITTY:false,
    NIVAL:false, NIXED:false, NIXER:false, NIXES:false, NIXIE:false, NIZAM:false, NKOSI:false, NOAHS:false, NOBBY:false, NOBLE:false,
    NOBLY:false, NOCKS:false, NODAL:false, NODDY:false, NODES:false, NODUS:false, NOELS:false, NOGGS:false, NOHOW:false, NOILS:false,
    NOILY:false, NOINT:false, NOIRS:false, NOISE:false, NOISY:false, NOLES:false, NOLLS:false, NOLOS:false, NOMAD:false, NOMAS:false,
    NOMEN:false, NOMES:false, NOMIC:false, NOMOI:false, NOMOS:false, NONAS:false, NONCE:false, NONES:false, NONET:false, NONGS:false,
    NONIS:false, NONNY:false, NONYL:false, NOOBS:false, NOOIT:false, NOOKS:false, NOOKY:false, NOONS:false, NOOPS:false, NOOSE:false,
    NOPAL:false, NORIA:false, NORIS:false, NORKS:false, NORMA:false, NORMS:false, NORTH:false, NOSED:false, NOSER:false, NOSES:false,
    NOSEY:false, NOTAL:false, NOTCH:false, NOTED:false, NOTER:false, NOTES:false, NOTUM:false, NOULD:false, NOULE:false, NOULS:false,
    NOUNS:false, NOUNY:false, NOUPS:false, NOVAE:false, NOVAS:false, NOVEL:false, NOVUM:false, NOWAY:false, NOWED:false, NOWLS:false,
    NOWTS:false, NOWTY:false, NOXAL:false, NOXES:false, NOYAU:false, NOYED:false, NOYES:false, NUBBY:false, NUBIA:false, NUCHA:false,
    NUDDY:false, NUDER:false, NUDES:false, NUDGE:false, NUDIE:false, NUDZH:false, NUFFS:false, NUGAE:false, NUKED:false, NUKES:false,
    NULLA:false, NULLS:false, NUMBS:false, NUMEN:false, NUMMY:false, NUNNY:false, NURDS:false, NURDY:false, NURLS:false, NURRS:false,
    NURSE:false, NUTSO:false, NUTSY:false, NUTTY:false, NYAFF:false, NYALA:false, NYING:false, NYLON:false, NYMPH:false, NYSSA:false,
    OAKED:false, OAKEN:false, OAKER:false, OAKUM:false, OARED:false, OASES:false, OASIS:false, OASTS:false, OATEN:false, OATER:false,
    OATHS:false, OAVES:false, OBANG:false, OBEAH:false, OBELI:false, OBESE:false, OBEYS:false, OBIAS:false, OBIED:false, OBIIT:false,
    OBITS:false, OBJET:false, OBOES:false, OBOLE:false, OBOLI:false, OBOLS:false, OCCAM:false, OCCUR:false, OCEAN:false, OCHER:false,
    OCHES:false, OCHRE:false, OCHRY:false, OCKER:false, OCREA:false, OCTAD:false, OCTAL:false, OCTAN:false, OCTAS:false, OCTET:false,
    OCTYL:false, OCULI:false, ODAHS:false, ODALS:false, ODDER:false, ODDLY:false, ODEON:false, ODEUM:false, ODISM:false, ODIST:false,
    ODIUM:false, ODORS:false, ODOUR:false, ODYLE:false, ODYLS:false, OFAYS:false, OFFAL:false, OFFED:false, OFFER:false, OFFIE:false,
    OFLAG:false, OFTEN:false, OFTER:false, OGAMS:false, OGEED:false, OGEES:false, OGGIN:false, OGHAM:false, OGIVE:false, OGLED:false,
    OGLER:false, OGLES:false, OGMIC:false, OGRES:false, OHIAS:false, OHING:false, OHMIC:false, OHONE:false, OIDIA:false, OILED:false,
    OILER:false, OINKS:false, OINTS:false, OJIME:false, OKAPI:false, OKAYS:false, OKEHS:false, OKRAS:false, OKTAS:false, OLDEN:false,
    OLDER:false, OLDIE:false, OLEIC:false, OLEIN:false, OLENT:false, OLEOS:false, OLEUM:false, OLIOS:false, OLIVE:false, OLLAS:false,
    OLLAV:false, OLLER:false, OLLIE:false, OLOGY:false, OLPAE:false, OLPES:false, OMASA:false, OMBER:false, OMBRE:false, OMBUS:false,
    OMEGA:false, OMENS:false, OMERS:false, OMITS:false, OMLAH:false, OMOVS:false, OMRAH:false, ONCER:false, ONCES:false, ONCET:false,
    ONCUS:false, ONELY:false, ONERS:false, ONERY:false, ONION:false, ONIUM:false, ONKUS:false, ONLAY:false, ONNED:false, ONSET:false,
    ONTIC:false, OOBIT:false, OOHED:false, OOMPH:false, OONTS:false, OOPED:false, OORIE:false, OOSES:false, OOTID:false, OOZED:false,
    OOZES:false, OPAHS:false, OPALS:false, OPENS:false, OPEPE:false, OPERA:false, OPINE:false, OPING:false, OPIUM:false, OPPOS:false,
    OPSIN:false, OPTED:false, OPTER:false, OPTIC:false, ORACH:false, ORACY:false, ORALS:false, ORANG:false, ORANT:false, ORATE:false,
    ORBED:false, ORBIT:false, ORCAS:false, ORCIN:false, ORDER:false, ORDOS:false, OREAD:false, ORFES:false, ORGAN:false, ORGIA:false,
    ORGIC:false, ORGUE:false, ORIBI:false, ORIEL:false, ORIXA:false, ORLES:false, ORLON:false, ORLOP:false, ORMER:false, ORNIS:false,
    ORPIN:false, ORRIS:false, ORTHO:false, ORVAL:false, ORZOS:false, OSCAR:false, OSHAC:false, OSIER:false, OSMIC:false, OSMOL:false,
    OSSIA:false, OSTIA:false, OTAKU:false, OTARY:false, OTHER:false, OTTAR:false, OTTER:false, OTTOS:false, OUBIT:false, OUCHT:false,
    OUENS:false, OUGHT:false, OUIJA:false, OULKS:false, OUMAS:false, OUNCE:false, OUNDY:false, OUPAS:false, OUPED:false, OUPHE:false,
    OUPHS:false, OURIE:false, OUSEL:false, OUSTS:false, OUTBY:false, OUTDO:false, OUTED:false, OUTER:false, OUTGO:false, OUTRE:false,
    OUTRO:false, OUTTA:false, OUZEL:false, OUZOS:false, OVALS:false, OVARY:false, OVATE:false, OVELS:false, OVENS:false, OVERS:false,
    OVERT:false, OVINE:false, OVIST:false, OVOID:false, OVOLI:false, OVOLO:false, OVULE:false, OWCHE:false, OWIES:false, OWING:false,
    OWLED:false, OWLER:false, OWLET:false, OWNED:false, OWNER:false, OWRES:false, OWRIE:false, OWSEN:false, OXBOW:false, OXERS:false,
    OXEYE:false, OXIDE:false, OXIDS:false, OXIES:false, OXIME:false, OXIMS:false, OXLIP:false, OXTER:false, OYERS:false, OZEKI:false,
    OZONE:false, OZZIE:false, PAALS:false, PAANS:false, PACAS:false, PACED:false, PACER:false, PACES:false, PACEY:false, PACHA:false,
    PACKS:false, PACOS:false, PACTA:false, PACTS:false, PADDY:false, PADIS:false, PADLE:false, PADMA:false, PADRE:false, PADRI:false,
    PAEAN:false, PAEDO:false, PAEON:false, PAGAN:false, PAGED:false, PAGER:false, PAGES:false, PAGLE:false, PAGOD:false, PAGRI:false,
    PAIKS:false, PAILS:false, PAINS:false, PAINT:false, PAIRE:false, PAIRS:false, PAISA:false, PAISE:false, PAKKA:false, PALAS:false,
    PALAY:false, PALEA:false, PALED:false, PALER:false, PALES:false, PALET:false, PALIS:false, PALKI:false, PALLA:false, PALLS:false,
    PALLY:false, PALMS:false, PALMY:false, PALPI:false, PALPS:false, PALSA:false, PALSY:false, PAMPA:false, PANAX:false, PANCE:false,
    PANDA:false, PANDS:false, PANDY:false, PANED:false, PANEL:false, PANES:false, PANGA:false, PANGS:false, PANIC:false, PANIM:false,
    PANKO:false, PANNE:false, PANNI:false, PANSY:false, PANTO:false, PANTS:false, PANTY:false, PAOLI:false, PAOLO:false, PAPAL:false,
    PAPAS:false, PAPAW:false, PAPER:false, PAPES:false, PAPPI:false, PAPPY:false, PARAE:false, PARAS:false, PARCH:false, PARDI:false,
    PARDS:false, PARDY:false, PARED:false, PAREN:false, PAREO:false, PARER:false, PARES:false, PAREU:false, PAREV:false, PARGE:false,
    PARGO:false, PARIS:false, PARKA:false, PARKI:false, PARKS:false, PARKY:false, PARLE:false, PARLY:false, PARMA:false, PAROL:false,
    PARPS:false, PARRA:false, PARRS:false, PARRY:false, PARSE:false, PARTI:false, PARTS:false, PARTY:false, PARVE:false, PARVO:false,
    PASEO:false, PASES:false, PASHA:false, PASHM:false, PASKA:false, PASPY:false, PASSE:false, PASTA:false, PASTE:false, PASTS:false,
    PASTY:false, PATCH:false, PATED:false, PATEN:false, PATER:false, PATES:false, PATHS:false, PATIN:false, PATIO:false, PATKA:false,
    PATLY:false, PATSY:false, PATTE:false, PATTY:false, PATUS:false, PAUAS:false, PAULS:false, PAUSE:false, PAVAN:false, PAVED:false,
    PAVEN:false, PAVER:false, PAVES:false, PAVID:false, PAVIN:false, PAVIS:false, PAWAS:false, PAWAW:false, PAWED:false, PAWER:false,
    PAWKS:false, PAWKY:false, PAWLS:false, PAWNS:false, PAXES:false, PAYED:false, PAYEE:false, PAYER:false, PAYOR:false, PAYSD:false,
    PEACE:false, PEACH:false, PEAGE:false, PEAGS:false, PEAKS:false, PEAKY:false, PEALS:false, PEANS:false, PEARE:false, PEARL:false,
    PEARS:false, PEART:false, PEASE:false, PEATS:false, PEATY:false, PEAVY:false, PEAZE:false, PEBAS:false, PECAN:false, PECHS:false,
    PECKE:false, PECKS:false, PECKY:false, PEDAL:false, PEDES:false, PEDIS:false, PEDRO:false, PEECE:false, PEEKS:false, PEELS:false,
    PEENS:false, PEEOY:false, PEEPE:false, PEEPS:false, PEERS:false, PEERY:false, PEEVE:false, PEGGY:false, PEGHS:false, PEINS:false,
    PEISE:false, PEIZE:false, PEKAN:false, PEKES:false, PEKIN:false, PEKOE:false, PELAS:false, PELAU:false, PELES:false, PELFS:false,
    PELLS:false, PELMA:false, PELON:false, PELTA:false, PELTS:false, PENAL:false, PENCE:false, PENDS:false, PENDU:false, PENED:false,
    PENES:false, PENGO:false, PENIE:false, PENIS:false, PENKS:false, PENNA:false, PENNE:false, PENNI:false, PENNY:false, PENTS:false,
    PEONS:false, PEONY:false, PEPLA:false, PEPOS:false, PEPPY:false, PEPSI:false, PERAI:false, PERCE:false, PERCH:false, PERCS:false,
    PERDU:false, PERDY:false, PEREA:false, PERES:false, PERIL:false, PERIS:false, PERKS:false, PERKY:false, PERMS:false, PERNS:false,
    PEROG:false, PERPS:false, PERRY:false, PERSE:false, PERST:false, PERTS:false, PERVE:false, PERVO:false, PERVS:false, PERVY:false,
    PESKY:false, PESOS:false, PESTO:false, PESTS:false, PESTY:false, PETAL:false, PETAR:false, PETER:false, PETIT:false, PETRE:false,
    PETRI:false, PETTI:false, PETTO:false, PETTY:false, PEWEE:false, PEWIT:false, PEYSE:false, PHAGE:false, PHANG:false, PHARE:false,
    PHARM:false, PHASE:false, PHEER:false, PHENE:false, PHEON:false, PHESE:false, PHIAL:false, PHISH:false, PHIZZ:false, PHLOX:false,
    PHOCA:false, PHONE:false, PHONO:false, PHONS:false, PHONY:false, PHOTO:false, PHOTS:false, PHPHT:false, PHUTS:false, PHYLA:false,
    PHYLE:false, PIANI:false, PIANO:false, PIANS:false, PIBAL:false, PICAL:false, PICAS:false, PICCY:false, PICKS:false, PICKY:false,
    PICOT:false, PICRA:false, PICUL:false, PIECE:false, PIEND:false, PIERS:false, PIERT:false, PIETA:false, PIETS:false, PIETY:false,
    PIEZO:false, PIGGY:false, PIGHT:false, PIGMY:false, PIING:false, PIKAS:false, PIKAU:false, PIKED:false, PIKER:false, PIKES:false,
    PIKEY:false, PIKIS:false, PIKUL:false, PILAE:false, PILAF:false, PILAO:false, PILAR:false, PILAU:false, PILAW:false, PILCH:false,
    PILEA:false, PILED:false, PILEI:false, PILER:false, PILES:false, PILIS:false, PILLS:false, PILOT:false, PILOW:false, PILUM:false,
    PILUS:false, PIMAS:false, PIMPS:false, PINAS:false, PINCH:false, PINED:false, PINES:false, PINEY:false, PINGO:false, PINGS:false,
    PINKO:false, PINKS:false, PINKY:false, PINNA:false, PINNY:false, PINON:false, PINOT:false, PINTA:false, PINTO:false, PINTS:false,
    PINUP:false, PIONS:false, PIONY:false, PIOUS:false, PIOYE:false, PIOYS:false, PIPAL:false, PIPAS:false, PIPED:false, PIPER:false,
    PIPES:false, PIPET:false, PIPIS:false, PIPIT:false, PIPPY:false, PIPUL:false, PIQUE:false, PIRAI:false, PIRLS:false, PIRNS:false,
    PIROG:false, PISCO:false, PISES:false, PISKY:false, PISOS:false, PISSY:false, PISTE:false, PITAS:false, PITCH:false, PITHS:false,
    PITHY:false, PITON:false, PITOT:false, PITTA:false, PIUMS:false, PIVOT:false, PIXEL:false, PIXES:false, PIXIE:false, PIZED:false,
    PIZES:false, PIZZA:false, PLAAS:false, PLACE:false, PLACK:false, PLAGE:false, PLAID:false, PLAIN:false, PLAIT:false, PLANE:false,
    PLANK:false, PLANS:false, PLANT:false, PLAPS:false, PLASH:false, PLASM:false, PLAST:false, PLATE:false, PLATS:false, PLATT:false,
    PLATY:false, PLAYA:false, PLAYS:false, PLAZA:false, PLEAD:false, PLEAS:false, PLEAT:false, PLEBE:false, PLEBS:false, PLENA:false,
    PLEON:false, PLESH:false, PLEWS:false, PLICA:false, PLIED:false, PLIER:false, PLIES:false, PLIMS:false, PLING:false, PLINK:false,
    PLOAT:false, PLODS:false, PLONG:false, PLONK:false, PLOOK:false, PLOPS:false, PLOTS:false, PLOTZ:false, PLOUK:false, PLOWS:false,
    PLOYE:false, PLOYS:false, PLUCK:false, PLUES:false, PLUFF:false, PLUGS:false, PLUMB:false, PLUME:false, PLUMP:false, PLUMS:false,
    PLUMY:false, PLUNK:false, PLUOT:false, PLUSH:false, PLUTO:false, PLYER:false, POACH:false, POAKA:false, POAKE:false, POBOY:false,
    POCKS:false, POCKY:false, PODAL:false, PODDY:false, PODEX:false, PODGE:false, PODGY:false, PODIA:false, POEMS:false, POEPS:false,
    POESY:false, POETS:false, POGEY:false, POGGE:false, POGOS:false, POHED:false, POILU:false, POIND:false, POINT:false, POISE:false,
    POKAL:false, POKED:false, POKER:false, POKES:false, POKEY:false, POKIE:false, POLAR:false, POLED:false, POLER:false, POLES:false,
    POLEY:false, POLIO:false, POLIS:false, POLJE:false, POLKA:false, POLKS:false, POLLS:false, POLLY:false, POLOS:false, POLTS:false,
    POLYP:false, POLYS:false, POMBE:false, POMES:false, POMMY:false, POMOS:false, POMPS:false, PONCE:false, PONCY:false, PONDS:false,
    PONES:false, PONEY:false, PONGA:false, PONGO:false, PONGS:false, PONGY:false, PONKS:false, PONTS:false, PONTY:false, PONZU:false,
    POOCH:false, POODS:false, POOED:false, POOFS:false, POOFY:false, POOHS:false, POOJA:false, POOKA:false, POOKS:false, POOLS:false,
    POONS:false, POOPS:false, POOPY:false, POORI:false, POORT:false, POOTS:false, POOVE:false, POOVY:false, POPES:false, POPPA:false,
    POPPY:false, POPSY:false, PORAE:false, PORAL:false, PORCH:false, PORED:false, PORER:false, PORES:false, PORGE:false, PORGY:false,
    PORIN:false, PORKS:false, PORKY:false, PORNO:false, PORNS:false, PORNY:false, PORTA:false, PORTS:false, PORTY:false, POSED:false,
    POSER:false, POSES:false, POSEY:false, POSHO:false, POSIT:false, POSSE:false, POSTS:false, POTAE:false, POTCH:false, POTED:false,
    POTES:false, POTIN:false, POTOO:false, POTSY:false, POTTO:false, POTTS:false, POTTY:false, POUCH:false, POUFF:false, POUFS:false,
    POUKE:false, POUKS:false, POULE:false, POULP:false, POULT:false, POUND:false, POUPE:false, POUPT:false, POURS:false, POUTS:false,
    POUTY:false, POWAN:false, POWER:false, POWIN:false, POWND:false, POWNS:false, POWNY:false, POWRE:false, POXED:false, POXES:false,
    POYNT:false, POYOU:false, POYSE:false, POZZY:false, PRAAM:false, PRADS:false, PRAHU:false, PRAMS:false, PRANA:false, PRANG:false,
    PRANK:false, PRAOS:false, PRASE:false, PRATE:false, PRATS:false, PRATT:false, PRATY:false, PRAUS:false, PRAWN:false, PRAYS:false,
    PREDY:false, PREED:false, PREEN:false, PREES:false, PREIF:false, PREMS:false, PREMY:false, PRENT:false, PREON:false, PREOP:false,
    PREPS:false, PRESA:false, PRESE:false, PRESS:false, PREST:false, PREVE:false, PREXY:false, PREYS:false, PRIAL:false, PRICE:false,
    PRICY:false, PRIDE:false, PRIED:false, PRIEF:false, PRIER:false, PRIES:false, PRIGS:false, PRILL:false, PRIMA:false, PRIME:false,
    PRIMI:false, PRIMO:false, PRIMP:false, PRIMS:false, PRIMY:false, PRINK:false, PRINT:false, PRION:false, PRIOR:false, PRISE:false,
    PRISM:false, PRISS:false, PRIVY:false, PRIZE:false, PROAS:false, PROBE:false, PROBS:false, PRODS:false, PROEM:false, PROFS:false,
    PROGS:false, PROIN:false, PROKE:false, PROLE:false, PROLL:false, PROMO:false, PROMS:false, PRONE:false, PRONG:false, PRONK:false,
    PROOF:false, PROPS:false, PRORE:false, PROSE:false, PROSO:false, PROSS:false, PROST:false, PROSY:false, PROTO:false, PROUD:false,
    PROUL:false, PROVE:false, PROWL:false, PROWS:false, PROXY:false, PROYN:false, PRUDE:false, PRUNE:false, PRUNT:false, PRUTA:false,
    PRYER:false, PRYSE:false, PSALM:false, PSEUD:false, PSHAW:false, PSION:false, PSOAE:false, PSOAI:false, PSOAS:false, PSORA:false,
    PSYCH:false, PSYOP:false, PUBCO:false, PUBES:false, PUBIC:false, PUBIS:false, PUCAN:false, PUCER:false, PUCES:false, PUCKA:false,
    PUCKS:false, PUDDY:false, PUDGE:false, PUDGY:false, PUDIC:false, PUDOR:false, PUDSY:false, PUDUS:false, PUERS:false, PUFFA:false,
    PUFFS:false, PUFFY:false, PUGGY:false, PUGIL:false, PUHAS:false, PUJAH:false, PUJAS:false, PUKAS:false, PUKED:false, PUKER:false,
    PUKES:false, PUKEY:false, PUKKA:false, PUKUS:false, PULAO:false, PULAS:false, PULED:false, PULER:false, PULES:false, PULIK:false,
    PULIS:false, PULKA:false, PULKS:false, PULLI:false, PULLS:false, PULLY:false, PULMO:false, PULPS:false, PULPY:false, PULSE:false,
    PULUS:false, PUMAS:false, PUMIE:false, PUMPS:false, PUNAS:false, PUNCE:false, PUNCH:false, PUNGA:false, PUNGS:false, PUNJI:false,
    PUNKA:false, PUNKS:false, PUNKY:false, PUNNY:false, PUNTO:false, PUNTS:false, PUNTY:false, PUPAE:false, PUPAL:false, PUPAS:false,
    PUPIL:false, PUPPY:false, PUPUS:false, PURDA:false, PURED:false, PUREE:false, PURER:false, PURES:false, PURGE:false, PURIN:false,
    PURIS:false, PURLS:false, PURPY:false, PURRS:false, PURSE:false, PURSY:false, PURTY:false, PUSES:false, PUSHY:false, PUSLE:false,
    PUSSY:false, PUTID:false, PUTON:false, PUTTI:false, PUTTO:false, PUTTS:false, PUTTY:false, PUZEL:false, PWNED:false, PYATS:false,
    PYETS:false, PYGAL:false, PYGMY:false, PYINS:false, PYLON:false, PYNED:false, PYNES:false, PYOID:false, PYOTS:false, PYRAL:false,
    PYRAN:false, PYRES:false, PYREX:false, PYRIC:false, PYROS:false, PYXED:false, PYXES:false, PYXIE:false, PYXIS:false, PZAZZ:false,
    QADIS:false, QAIDS:false, QAJAQ:false, QANAT:false, QAPIK:false, QIBLA:false, QOPHS:false, QORMA:false, QUACK:false, QUADS:false,
    QUAFF:false, QUAGS:false, QUAIL:false, QUAIR:false, QUAIS:false, QUAKE:false, QUAKY:false, QUALE:false, QUALM:false, QUANT:false,
    QUARE:false, QUARK:false, QUART:false, QUASH:false, QUASI:false, QUASS:false, QUATE:false, QUATS:false, QUAYD:false, QUAYS:false,
    QUBIT:false, QUEAN:false, QUEEN:false, QUEER:false, QUELL:false, QUEME:false, QUENA:false, QUERN:false, QUERY:false, QUEST:false,
    QUEUE:false, QUEYN:false, QUEYS:false, QUICH:false, QUICK:false, QUIDS:false, QUIET:false, QUIFF:false, QUILL:false, QUILT:false,
    QUIMS:false, QUINA:false, QUINE:false, QUINO:false, QUINS:false, QUINT:false, QUIPO:false, QUIPS:false, QUIPU:false, QUIRE:false,
    QUIRK:false, QUIRT:false, QUIST:false, QUITE:false, QUITS:false, QUOAD:false, QUODS:false, QUOIF:false, QUOIN:false, QUOIT:false,
    QUOLL:false, QUONK:false, QUOPS:false, QUOTA:false, QUOTE:false, QUOTH:false, QURSH:false, QUYTE:false, RABAT:false, RABBI:false,
    RABIC:false, RABID:false, RABIS:false, RACED:false, RACER:false, RACES:false, RACHE:false, RACKS:false, RACON:false, RADAR:false,
    RADGE:false, RADII:false, RADIO:false, RADIX:false, RADON:false, RAFFS:false, RAFTS:false, RAGAS:false, RAGDE:false, RAGED:false,
    RAGEE:false, RAGER:false, RAGES:false, RAGGA:false, RAGGS:false, RAGGY:false, RAGIS:false, RAGUS:false, RAHED:false, RAHUI:false,
    RAIAS:false, RAIDS:false, RAIKS:false, RAILE:false, RAILS:false, RAINE:false, RAINS:false, RAINY:false, RAIRD:false, RAISE:false,
    RAITA:false, RAITS:false, RAJAH:false, RAJAS:false, RAJES:false, RAKED:false, RAKEE:false, RAKER:false, RAKES:false, RAKIA:false,
    RAKIS:false, RAKUS:false, RALES:false, RALLY:false, RALPH:false, RAMAL:false, RAMEE:false, RAMEN:false, RAMET:false, RAMIE:false,
    RAMIN:false, RAMIS:false, RAMMY:false, RAMPS:false, RAMUS:false, RANAS:false, RANCE:false, RANCH:false, RANDS:false, RANDY:false,
    RANEE:false, RANGA:false, RANGE:false, RANGI:false, RANGS:false, RANGY:false, RANID:false, RANIS:false, RANKE:false, RANKS:false,
    RANTS:false, RAPED:false, RAPER:false, RAPES:false, RAPHE:false, RAPID:false, RAPPE:false, RARED:false, RAREE:false, RARER:false,
    RARES:false, RARKS:false, RASED:false, RASER:false, RASES:false, RASPS:false, RASPY:false, RASSE:false, RASTA:false, RATAL:false,
    RATAN:false, RATAS:false, RATCH:false, RATED:false, RATEL:false, RATER:false, RATES:false, RATHA:false, RATHE:false, RATHS:false,
    RATIO:false, RATOO:false, RATOS:false, RATTY:false, RATUS:false, RAUNS:false, RAUPO:false, RAVED:false, RAVEL:false, RAVEN:false,
    RAVER:false, RAVES:false, RAVEY:false, RAVIN:false, RAWER:false, RAWIN:false, RAWLY:false, RAWNS:false, RAXED:false, RAXES:false,
    RAYAH:false, RAYAS:false, RAYED:false, RAYLE:false, RAYNE:false, RAYON:false, RAZED:false, RAZEE:false, RAZER:false, RAZES:false,
    RAZOO:false, RAZOR:false, REACH:false, REACT:false, READD:false, READS:false, READY:false, REAIS:false, REAKS:false, REALM:false,
    REALO:false, REALS:false, REAME:false, REAMS:false, REAMY:false, REANS:false, REAPS:false, REARM:false, REARS:false, REAST:false,
    REATA:false, REATE:false, REAVE:false, REBAR:false, REBBE:false, REBEC:false, REBEL:false, REBID:false, REBIT:false, REBOP:false,
    REBUS:false, REBUT:false, REBUY:false, RECAL:false, RECAP:false, RECCE:false, RECCO:false, RECCY:false, RECIT:false, RECKS:false,
    RECON:false, RECTA:false, RECTI:false, RECTO:false, RECUR:false, RECUT:false, REDAN:false, REDDS:false, REDDY:false, REDED:false,
    REDES:false, REDIA:false, REDID:false, REDIP:false, REDLY:false, REDON:false, REDOS:false, REDOX:false, REDRY:false, REDUB:false,
    REDUX:false, REDYE:false, REECH:false, REEDE:false, REEDS:false, REEDY:false, REEFS:false, REEFY:false, REEKS:false, REEKY:false,
    REELS:false, REENS:false, REEST:false, REEVE:false, REFED:false, REFEL:false, REFER:false, REFFO:false, REFIS:false, REFIT:false,
    REFIX:false, REFLY:false, REFRY:false, REGAL:false, REGAR:false, REGES:false, REGGO:false, REGIE:false, REGMA:false, REGNA:false,
    REGOS:false, REGUR:false, REHAB:false, REHEM:false, REIFS:false, REIFY:false, REIGN:false, REIKI:false, REIKS:false, REINK:false,
    REINS:false, REIRD:false, REIST:false, REIVE:false, REJIG:false, REJON:false, REKED:false, REKES:false, REKEY:false, RELAX:false,
    RELAY:false, RELET:false, RELIC:false, RELIE:false, RELIT:false, RELLO:false, REMAN:false, REMAP:false, REMEN:false, REMET:false,
    REMEX:false, REMIT:false, REMIX:false, RENAL:false, RENAY:false, RENDS:false, RENEW:false, RENEY:false, RENGA:false, RENIG:false,
    RENIN:false, RENNE:false, RENOS:false, RENTE:false, RENTS:false, REOIL:false, REORG:false, REPAY:false, REPEG:false, REPEL:false,
    REPIN:false, REPLA:false, REPLY:false, REPOS:false, REPOT:false, REPPS:false, REPRO:false, RERAN:false, RERIG:false, RERUN:false,
    RESAT:false, RESAW:false, RESAY:false, RESEE:false, RESES:false, RESET:false, RESEW:false, RESID:false, RESIN:false, RESIT:false,
    RESOD:false, RESOW:false, RESTO:false, RESTS:false, RESTY:false, RESUS:false, RETAG:false, RETAX:false, RETCH:false, RETEM:false,
    RETIA:false, RETIE:false, RETOX:false, RETRO:false, RETRY:false, REUSE:false, REVEL:false, REVET:false, REVIE:false, REVUE:false,
    REWAN:false, REWAX:false, REWED:false, REWET:false, REWIN:false, REWON:false, REWTH:false, REXES:false, REZES:false, RHEAS:false,
    RHEME:false, RHEUM:false, RHIES:false, RHIME:false, RHINE:false, RHINO:false, RHODY:false, RHOMB:false, RHONE:false, RHUMB:false,
    RHYME:false, RHYNE:false, RHYTA:false, RIADS:false, RIALS:false, RIANT:false, RIATA:false, RIBAS:false, RIBBY:false, RIBES:false,
    RICED:false, RICER:false, RICES:false, RICEY:false, RICHT:false, RICIN:false, RICKS:false, RIDER:false, RIDES:false, RIDGE:false,
    RIDGY:false, RIDIC:false, RIELS:false, RIEMS:false, RIEVE:false, RIFER:false, RIFFS:false, RIFLE:false, RIFTE:false, RIFTS:false,
    RIFTY:false, RIGGS:false, RIGHT:false, RIGID:false, RIGOL:false, RIGOR:false, RILED:false, RILES:false, RILEY:false, RILLE:false,
    RILLS:false, RIMAE:false, RIMED:false, RIMER:false, RIMES:false, RIMUS:false, RINDS:false, RINDY:false, RINES:false, RINGS:false,
    RINKS:false, RINSE:false, RIOJA:false, RIOTS:false, RIPED:false, RIPEN:false, RIPER:false, RIPES:false, RIPPS:false, RISEN:false,
    RISER:false, RISES:false, RISHI:false, RISKS:false, RISKY:false, RISPS:false, RISUS:false, RITES:false, RITTS:false, RITZY:false,
    RIVAL:false, RIVAS:false, RIVED:false, RIVEL:false, RIVEN:false, RIVER:false, RIVES:false, RIVET:false, RIYAL:false, RIZAS:false,
    ROACH:false, ROADS:false, ROAMS:false, ROANS:false, ROARS:false, ROARY:false, ROAST:false, ROATE:false, ROBED:false, ROBES:false,
    ROBIN:false, ROBLE:false, ROBOT:false, ROCKS:false, ROCKY:false, RODED:false, RODEO:false, RODES:false, ROGER:false, ROGUE:false,
    ROGUY:false, ROHES:false, ROIDS:false, ROILS:false, ROILY:false, ROINS:false, ROIST:false, ROJAK:false, ROJIS:false, ROKED:false,
    ROKER:false, ROKES:false, ROLAG:false, ROLES:false, ROLFS:false, ROLLS:false, ROMAL:false, ROMAN:false, ROMEO:false, ROMPS:false,
    RONDE:false, RONDO:false, RONEO:false, RONES:false, RONIN:false, RONNE:false, RONTE:false, RONTS:false, ROODS:false, ROOFS:false,
    ROOFY:false, ROOKS:false, ROOKY:false, ROOMS:false, ROOMY:false, ROONS:false, ROOPS:false, ROOPY:false, ROOSA:false, ROOSE:false,
    ROOST:false, ROOTS:false, ROOTY:false, ROPED:false, ROPER:false, ROPES:false, ROPEY:false, ROQUE:false, RORAL:false, RORES:false,
    RORIC:false, RORID:false, RORIE:false, RORTS:false, RORTY:false, ROSED:false, ROSES:false, ROSET:false, ROSHI:false, ROSIN:false,
    ROSIT:false, ROSTI:false, ROSTS:false, ROTAL:false, ROTAN:false, ROTAS:false, ROTCH:false, ROTED:false, ROTES:false, ROTIS:false,
    ROTLS:false, ROTON:false, ROTOR:false, ROTOS:false, ROTTE:false, ROUEN:false, ROUES:false, ROUGE:false, ROUGH:false, ROULE:false,
    ROULS:false, ROUMS:false, ROUND:false, ROUPS:false, ROUPY:false, ROUSE:false, ROUST:false, ROUTE:false, ROUTH:false, ROUTS:false,
    ROVED:false, ROVEN:false, ROVER:false, ROVES:false, ROWAN:false, ROWDY:false, ROWED:false, ROWEL:false, ROWEN:false, ROWER:false,
    ROWIE:false, ROWME:false, ROWND:false, ROWTH:false, ROWTS:false, ROYAL:false, ROYNE:false, ROYST:false, ROZET:false, ROZIT:false,
    RUANA:false, RUBAI:false, RUBBY:false, RUBEL:false, RUBES:false, RUBIN:false, RUBLE:false, RUBLI:false, RUBUS:false, RUCHE:false,
    RUCKS:false, RUDAS:false, RUDDS:false, RUDDY:false, RUDER:false, RUDES:false, RUDIE:false, RUDIS:false, RUEDA:false, RUERS:false,
    RUFFE:false, RUFFS:false, RUGAE:false, RUGAL:false, RUGBY:false, RUGGY:false, RUING:false, RUINS:false, RUKHS:false, RULED:false,
    RULER:false, RULES:false, RUMAL:false, RUMBA:false, RUMBO:false, RUMEN:false, RUMES:false, RUMLY:false, RUMMY:false, RUMOR:false,
    RUMPO:false, RUMPS:false, RUMPY:false, RUNCH:false, RUNDS:false, RUNED:false, RUNES:false, RUNGS:false, RUNIC:false, RUNNY:false,
    RUNTS:false, RUNTY:false, RUPEE:false, RUPIA:false, RURAL:false, RURPS:false, RURUS:false, RUSAS:false, RUSES:false, RUSHY:false,
    RUSKS:false, RUSMA:false, RUSSE:false, RUSTS:false, RUSTY:false, RUTHS:false, RUTIN:false, RUTTY:false, RYALS:false, RYBAT:false,
    RYKED:false, RYKES:false, RYMME:false, RYNDS:false, RYOTS:false, RYPER:false, SAAGS:false, SABAL:false, SABED:false, SABER:false,
    SABES:false, SABHA:false, SABIN:false, SABIR:false, SABLE:false, SABOT:false, SABRA:false, SABRE:false, SACKS:false, SACRA:false,
    SADDO:false, SADES:false, SADHE:false, SADHU:false, SADIS:false, SADLY:false, SADOS:false, SADZA:false, SAFED:false, SAFER:false,
    SAFES:false, SAGAS:false, SAGER:false, SAGES:false, SAGGY:false, SAGOS:false, SAGUM:false, SAHEB:false, SAHIB:false, SAICE:false,
    SAICK:false, SAICS:false, SAIDS:false, SAIGA:false, SAILS:false, SAIMS:false, SAINE:false, SAINS:false, SAINT:false, SAIRS:false,
    SAIST:false, SAITH:false, SAJOU:false, SAKAI:false, SAKER:false, SAKES:false, SAKIA:false, SAKIS:false, SAKTI:false, SALAD:false,
    SALAL:false, SALAT:false, SALEP:false, SALES:false, SALET:false, SALIC:false, SALIX:false, SALLE:false, SALLY:false, SALMI:false,
    SALOL:false, SALON:false, SALOP:false, SALPA:false, SALPS:false, SALSA:false, SALSE:false, SALTO:false, SALTS:false, SALTY:false,
    SALUE:false, SALUT:false, SALVE:false, SALVO:false, SAMAN:false, SAMAS:false, SAMBA:false, SAMBO:false, SAMEK:false, SAMEL:false,
    SAMEN:false, SAMES:false, SAMEY:false, SAMFU:false, SAMMY:false, SAMPI:false, SAMPS:false, SANDS:false, SANDY:false, SANED:false,
    SANER:false, SANES:false, SANGA:false, SANGH:false, SANGO:false, SANGS:false, SANKO:false, SANSA:false, SANTO:false, SANTS:false,
    SAOLA:false, SAPAN:false, SAPID:false, SAPOR:false, SAPPY:false, SARAN:false, SARDS:false, SARED:false, SAREE:false, SARGE:false,
    SARGO:false, SARIN:false, SARIS:false, SARKS:false, SARKY:false, SAROD:false, SAROS:false, SARUS:false, SASER:false, SASIN:false,
    SASSE:false, SASSY:false, SATAI:false, SATAY:false, SATED:false, SATEM:false, SATES:false, SATIN:false, SATIS:false, SATYR:false,
    SAUBA:false, SAUCE:false, SAUCH:false, SAUCY:false, SAUGH:false, SAULS:false, SAULT:false, SAUNA:false, SAUNT:false, SAURY:false,
    SAUTE:false, SAUTS:false, SAVED:false, SAVER:false, SAVES:false, SAVEY:false, SAVIN:false, SAVOR:false, SAVOY:false, SAVVY:false,
    SAWAH:false, SAWED:false, SAWER:false, SAXES:false, SAYED:false, SAYER:false, SAYID:false, SAYNE:false, SAYON:false, SAYST:false,
    SAZES:false, SCABS:false, SCADS:false, SCAFF:false, SCAGS:false, SCAIL:false, SCALA:false, SCALD:false, SCALE:false, SCALL:false,
    SCALP:false, SCALY:false, SCAMP:false, SCAMS:false, SCAND:false, SCANS:false, SCANT:false, SCAPA:false, SCAPE:false, SCAPI:false,
    SCARE:false, SCARF:false, SCARP:false, SCARS:false, SCART:false, SCARY:false, SCATH:false, SCATS:false, SCATT:false, SCAUD:false,
    SCAUP:false, SCAUR:false, SCAWS:false, SCEAT:false, SCENA:false, SCEND:false, SCENE:false, SCENT:false, SCHAV:false, SCHMO:false,
    SCHUL:false, SCHWA:false, SCION:false, SCLIM:false, SCODY:false, SCOFF:false, SCOGS:false, SCOLD:false, SCONE:false, SCOOG:false,
    SCOOP:false, SCOOT:false, SCOPA:false, SCOPE:false, SCOPS:false, SCORE:false, SCORN:false, SCOTS:false, SCOUG:false, SCOUP:false,
    SCOUR:false, SCOUT:false, SCOWL:false, SCOWP:false, SCOWS:false, SCRAB:false, SCRAE:false, SCRAG:false, SCRAM:false, SCRAN:false,
    SCRAP:false, SCRAT:false, SCRAW:false, SCRAY:false, SCREE:false, SCREW:false, SCRIM:false, SCRIP:false, SCROB:false, SCROD:false,
    SCROG:false, SCROW:false, SCRUB:false, SCRUM:false, SCUBA:false, SCUDI:false, SCUDO:false, SCUDS:false, SCUFF:false, SCUFT:false,
    SCUGS:false, SCULK:false, SCULL:false, SCULP:false, SCULS:false, SCUMS:false, SCUPS:false, SCURF:false, SCURS:false, SCUSE:false,
    SCUTA:false, SCUTE:false, SCUTS:false, SCUZZ:false, SCYES:false, SDAYN:false, SDEIN:false, SEALS:false, SEAME:false, SEAMS:false,
    SEAMY:false, SEANS:false, SEARE:false, SEARS:false, SEASE:false, SEATS:false, SEAZE:false, SEBUM:false, SECCO:false, SECHS:false,
    SECTS:false, SEDAN:false, SEDER:false, SEDES:false, SEDGE:false, SEDGY:false, SEDUM:false, SEEDS:false, SEEDY:false, SEEKS:false,
    SEELD:false, SEELS:false, SEELY:false, SEEMS:false, SEEPS:false, SEEPY:false, SEERS:false, SEFER:false, SEGAR:false, SEGNI:false,
    SEGNO:false, SEGOL:false, SEGOS:false, SEGUE:false, SEHRI:false, SEIFS:false, SEILS:false, SEINE:false, SEIRS:false, SEISE:false,
    SEISM:false, SEITY:false, SEIZA:false, SEIZE:false, SEKOS:false, SEKTS:false, SELAH:false, SELES:false, SELFS:false, SELLA:false,
    SELLE:false, SELLS:false, SELVA:false, SEMEE:false, SEMEN:false, SEMES:false, SEMIE:false, SEMIS:false, SENAS:false, SENDS:false,
    SENES:false, SENGI:false, SENNA:false, SENOR:false, SENSA:false, SENSE:false, SENSI:false, SENTE:false, SENTI:false, SENTS:false,
    SENVY:false, SENZA:false, SEPAD:false, SEPAL:false, SEPIA:false, SEPIC:false, SEPOY:false, SEPTA:false, SEPTS:false, SERAC:false,
    SERAI:false, SERAL:false, SERED:false, SERER:false, SERES:false, SERFS:false, SERGE:false, SERIC:false, SERIF:false, SERIN:false,
    SERKS:false, SERON:false, SEROW:false, SERRA:false, SERRE:false, SERRS:false, SERRY:false, SERUM:false, SERVE:false, SERVO:false,
    SESEY:false, SESSA:false, SETAE:false, SETAL:false, SETON:false, SETTS:false, SETUP:false, SEVEN:false, SEVER:false, SEWAN:false,
    SEWAR:false, SEWED:false, SEWEL:false, SEWEN:false, SEWER:false, SEWIN:false, SEXED:false, SEXER:false, SEXES:false, SEXTO:false,
    SEXTS:false, SEYEN:false, SHACK:false, SHADE:false, SHADS:false, SHADY:false, SHAFT:false, SHAGS:false, SHAHS:false, SHAKE:false,
    SHAKO:false, SHAKT:false, SHAKY:false, SHALE:false, SHALL:false, SHALM:false, SHALT:false, SHALY:false, SHAMA:false, SHAME:false,
    SHAMS:false, SHAND:false, SHANK:false, SHANS:false, SHAPE:false, SHAPS:false, SHARD:false, SHARE:false, SHARK:false, SHARN:false,
    SHARP:false, SHASH:false, SHAUL:false, SHAVE:false, SHAWL:false, SHAWM:false, SHAWN:false, SHAWS:false, SHAYA:false, SHAYS:false,
    SHCHI:false, SHEAF:false, SHEAL:false, SHEAR:false, SHEAS:false, SHEDS:false, SHEEL:false, SHEEN:false, SHEEP:false, SHEER:false,
    SHEET:false, SHEIK:false, SHELF:false, SHELL:false, SHEND:false, SHENT:false, SHEOL:false, SHERD:false, SHERE:false, SHERO:false,
    SHETS:false, SHEVA:false, SHEWN:false, SHEWS:false, SHIAI:false, SHIED:false, SHIEL:false, SHIER:false, SHIES:false, SHIFT:false,
    SHILL:false, SHILY:false, SHIMS:false, SHINE:false, SHINS:false, SHINY:false, SHIPS:false, SHIRE:false, SHIRK:false, SHIRR:false,
    SHIRS:false, SHIRT:false, SHISH:false, SHISO:false, SHIST:false, SHITE:false, SHITS:false, SHIUR:false, SHIVA:false, SHIVE:false,
    SHIVS:false, SHLEP:false, SHLUB:false, SHMEK:false, SHMOE:false, SHOAL:false, SHOAT:false, SHOCK:false, SHOED:false, SHOER:false,
    SHOES:false, SHOGI:false, SHOGS:false, SHOJI:false, SHOJO:false, SHOLA:false, SHONE:false, SHOOK:false, SHOOL:false, SHOON:false,
    SHOOS:false, SHOOT:false, SHOPE:false, SHOPS:false, SHORE:false, SHORL:false, SHORN:false, SHORT:false, SHOTE:false, SHOTS:false,
    SHOTT:false, SHOUT:false, SHOVE:false, SHOWD:false, SHOWN:false, SHOWS:false, SHOWY:false, SHOYU:false, SHRED:false, SHREW:false,
    SHRIS:false, SHROW:false, SHRUB:false, SHRUG:false, SHTIK:false, SHTUM:false, SHTUP:false, SHUCK:false, SHULE:false, SHULN:false,
    SHULS:false, SHUNS:false, SHUNT:false, SHURA:false, SHUSH:false, SHUTE:false, SHUTS:false, SHWAS:false, SHYER:false, SHYLY:false,
    SIALS:false, SIBBS:false, SIBYL:false, SICES:false, SICHT:false, SICKO:false, SICKS:false, SICKY:false, SIDAS:false, SIDED:false,
    SIDER:false, SIDES:false, SIDHA:false, SIDHE:false, SIDLE:false, SIEGE:false, SIELD:false, SIENS:false, SIENT:false, SIETH:false,
    SIEUR:false, SIEVE:false, SIFTS:false, SIGHS:false, SIGHT:false, SIGIL:false, SIGLA:false, SIGMA:false, SIGNA:false, SIGNS:false,
    SIJOS:false, SIKAS:false, SIKER:false, SIKES:false, SILDS:false, SILED:false, SILEN:false, SILER:false, SILES:false, SILEX:false,
    SILKS:false, SILKY:false, SILLS:false, SILLY:false, SILOS:false, SILTS:false, SILTY:false, SILVA:false, SIMAR:false, SIMAS:false,
    SIMBA:false, SIMIS:false, SIMPS:false, SIMUL:false, SINCE:false, SINDS:false, SINED:false, SINES:false, SINEW:false, SINGE:false,
    SINGS:false, SINHS:false, SINKS:false, SINKY:false, SINUS:false, SIPED:false, SIPES:false, SIPPY:false, SIRED:false, SIREE:false,
    SIREN:false, SIRES:false, SIRIH:false, SIRIS:false, SIROC:false, SIRRA:false, SIRUP:false, SISAL:false, SISES:false, SISSY:false,
    SISTA:false, SISTS:false, SITAR:false, SITED:false, SITES:false, SITHE:false, SITKA:false, SITUP:false, SITUS:false, SIVER:false,
    SIXER:false, SIXES:false, SIXMO:false, SIXTE:false, SIXTH:false, SIXTY:false, SIZAR:false, SIZED:false, SIZEL:false, SIZER:false,
    SIZES:false, SKAGS:false, SKAIL:false, SKALD:false, SKANK:false, SKART:false, SKATE:false, SKATS:false, SKATT:false, SKAWS:false,
    SKEAN:false, SKEAR:false, SKEDS:false, SKEED:false, SKEEF:false, SKEEN:false, SKEER:false, SKEES:false, SKEET:false, SKEGG:false,
    SKEGS:false, SKEIN:false, SKELF:false, SKELL:false, SKELM:false, SKELP:false, SKENE:false, SKENS:false, SKEOS:false, SKEPS:false,
    SKERS:false, SKETS:false, SKEWS:false, SKIDS:false, SKIED:false, SKIER:false, SKIES:false, SKIEY:false, SKIFF:false, SKILL:false,
    SKIMO:false, SKIMP:false, SKIMS:false, SKINK:false, SKINS:false, SKINT:false, SKIOS:false, SKIPS:false, SKIRL:false, SKIRR:false,
    SKIRT:false, SKITE:false, SKITS:false, SKIVE:false, SKIVY:false, SKLIM:false, SKOAL:false, SKODY:false, SKOFF:false, SKOGS:false,
    SKOLS:false, SKOOL:false, SKORT:false, SKOSH:false, SKRAN:false, SKRIK:false, SKUAS:false, SKUGS:false, SKULK:false, SKULL:false,
    SKUNK:false, SKYED:false, SKYER:false, SKYEY:false, SKYFS:false, SKYRE:false, SKYRS:false, SKYTE:false, SLABS:false, SLACK:false,
    SLADE:false, SLAES:false, SLAGS:false, SLAID:false, SLAIN:false, SLAKE:false, SLAMS:false, SLANE:false, SLANG:false, SLANK:false,
    SLANT:false, SLAPS:false, SLART:false, SLASH:false, SLATE:false, SLATS:false, SLATY:false, SLAVE:false, SLAWS:false, SLAYS:false,
    SLEBS:false, SLEDS:false, SLEEK:false, SLEEP:false, SLEER:false, SLEET:false, SLEPT:false, SLEWS:false, SLEYS:false, SLICE:false,
    SLICK:false, SLIDE:false, SLIER:false, SLILY:false, SLIME:false, SLIMS:false, SLIMY:false, SLING:false, SLINK:false, SLIPE:false,
    SLIPS:false, SLIPT:false, SLISH:false, SLITS:false, SLIVE:false, SLOAN:false, SLOBS:false, SLOES:false, SLOGS:false, SLOID:false,
    SLOJD:false, SLOMO:false, SLOOM:false, SLOOP:false, SLOOT:false, SLOPE:false, SLOPS:false, SLOPY:false, SLORM:false, SLOSH:false,
    SLOTH:false, SLOTS:false, SLOVE:false, SLOWS:false, SLOYD:false, SLUBB:false, SLUBS:false, SLUED:false, SLUES:false, SLUFF:false,
    SLUGS:false, SLUIT:false, SLUMP:false, SLUMS:false, SLUNG:false, SLUNK:false, SLURB:false, SLURP:false, SLURS:false, SLUSE:false,
    SLUSH:false, SLUTS:false, SLYER:false, SLYLY:false, SLYPE:false, SMAAK:false, SMACK:false, SMAIK:false, SMALL:false, SMALM:false,
    SMALT:false, SMARM:false, SMART:false, SMASH:false, SMAZE:false, SMEAR:false, SMEEK:false, SMEES:false, SMEIK:false, SMEKE:false,
    SMELL:false, SMELT:false, SMERK:false, SMEWS:false, SMILE:false, SMIRK:false, SMIRR:false, SMIRS:false, SMITE:false, SMITH:false,
    SMITS:false, SMOCK:false, SMOGS:false, SMOKE:false, SMOKO:false, SMOKY:false, SMOLT:false, SMOOR:false, SMOOT:false, SMORE:false,
    SMORG:false, SMOTE:false, SMOUT:false, SMOWT:false, SMUGS:false, SMURS:false, SMUSH:false, SMUTS:false, SNABS:false, SNACK:false,
    SNAFU:false, SNAGS:false, SNAIL:false, SNAKE:false, SNAKY:false, SNAPS:false, SNARE:false, SNARF:false, SNARK:false, SNARL:false,
    SNARS:false, SNARY:false, SNASH:false, SNATH:false, SNAWS:false, SNEAD:false, SNEAK:false, SNEAP:false, SNEBS:false, SNECK:false,
    SNEDS:false, SNEED:false, SNEER:false, SNEES:false, SNELL:false, SNIBS:false, SNICK:false, SNIDE:false, SNIES:false, SNIFF:false,
    SNIFT:false, SNIGS:false, SNIPE:false, SNIPS:false, SNIPY:false, SNIRT:false, SNITS:false, SNOBS:false, SNODS:false, SNOEK:false,
    SNOEP:false, SNOGS:false, SNOKE:false, SNOOD:false, SNOOK:false, SNOOL:false, SNOOP:false, SNOOT:false, SNORE:false, SNORT:false,
    SNOTS:false, SNOUT:false, SNOWK:false, SNOWS:false, SNOWY:false, SNUBS:false, SNUCK:false, SNUFF:false, SNUGS:false, SNUSH:false,
    SNYES:false, SOAKS:false, SOAPS:false, SOAPY:false, SOARE:false, SOARS:false, SOAVE:false, SOBAS:false, SOBER:false, SOCAS:false,
    SOCES:false, SOCKO:false, SOCKS:false, SOCLE:false, SODAS:false, SODDY:false, SODIC:false, SODOM:false, SOFAR:false, SOFAS:false,
    SOFTA:false, SOFTS:false, SOFTY:false, SOGER:false, SOGGY:false, SOHUR:false, SOILS:false, SOILY:false, SOJAS:false, SOJUS:false,
    SOKAH:false, SOKEN:false, SOKES:false, SOKOL:false, SOLAH:false, SOLAN:false, SOLAR:false, SOLAS:false, SOLDE:false, SOLDI:false,
    SOLDO:false, SOLDS:false, SOLED:false, SOLEI:false, SOLER:false, SOLES:false, SOLID:false, SOLON:false, SOLOS:false, SOLUM:false,
    SOLUS:false, SOLVE:false, SOMAN:false, SOMAS:false, SONAR:false, SONCE:false, SONDE:false, SONES:false, SONGS:false, SONIC:false,
    SONLY:false, SONNE:false, SONNY:false, SONSE:false, SONSY:false, SOOEY:false, SOOKS:false, SOOKY:false, SOOLE:false, SOOLS:false,
    SOOMS:false, SOOPS:false, SOOTE:false, SOOTH:false, SOOTS:false, SOOTY:false, SOPHS:false, SOPHY:false, SOPOR:false, SOPPY:false,
    SOPRA:false, SORAL:false, SORAS:false, SORBO:false, SORBS:false, SORDA:false, SORDO:false, SORDS:false, SORED:false, SOREE:false,
    SOREL:false, SORER:false, SORES:false, SOREX:false, SORGO:false, SORNS:false, SORRA:false, SORRY:false, SORTA:false, SORTS:false,
    SORUS:false, SOTHS:false, SOTOL:false, SOUCE:false, SOUCT:false, SOUGH:false, SOUKS:false, SOULS:false, SOUMS:false, SOUND:false,
    SOUPS:false, SOUPY:false, SOURS:false, SOUSE:false, SOUTH:false, SOUTS:false, SOWAR:false, SOWCE:false, SOWED:false, SOWER:false,
    SOWFF:false, SOWFS:false, SOWLE:false, SOWLS:false, SOWMS:false, SOWND:false, SOWNE:false, SOWPS:false, SOWSE:false, SOWTH:false,
    SOYAS:false, SOYLE:false, SOYUZ:false, SOZIN:false, SPACE:false, SPACY:false, SPADE:false, SPADO:false, SPAED:false, SPAER:false,
    SPAES:false, SPAGS:false, SPAHI:false, SPAIL:false, SPAIN:false, SPAIT:false, SPAKE:false, SPALD:false, SPALE:false, SPALL:false,
    SPALT:false, SPAMS:false, SPANE:false, SPANG:false, SPANK:false, SPANS:false, SPARD:false, SPARE:false, SPARK:false, SPARS:false,
    SPART:false, SPASM:false, SPATE:false, SPATS:false, SPAUL:false, SPAWL:false, SPAWN:false, SPAWS:false, SPAYD:false, SPAYS:false,
    SPAZA:false, SPAZZ:false, SPEAK:false, SPEAL:false, SPEAN:false, SPEAR:false, SPEAT:false, SPECK:false, SPECS:false, SPECT:false,
    SPEED:false, SPEEL:false, SPEER:false, SPEIL:false, SPEIR:false, SPEKS:false, SPELD:false, SPELK:false, SPELL:false, SPELT:false,
    SPEND:false, SPENT:false, SPEOS:false, SPERM:false, SPETS:false, SPEUG:false, SPEWS:false, SPEWY:false, SPIAL:false, SPICA:false,
    SPICE:false, SPICK:false, SPICS:false, SPICY:false, SPIDE:false, SPIED:false, SPIEL:false, SPIER:false, SPIES:false, SPIFF:false,
    SPIFS:false, SPIKE:false, SPIKS:false, SPIKY:false, SPILE:false, SPILL:false, SPILT:false, SPIMS:false, SPINA:false, SPINE:false,
    SPINK:false, SPINS:false, SPINY:false, SPIRE:false, SPIRT:false, SPIRY:false, SPITE:false, SPITS:false, SPITZ:false, SPIVS:false,
    SPLAT:false, SPLAY:false, SPLIT:false, SPLOG:false, SPODE:false, SPODS:false, SPOIL:false, SPOKE:false, SPOOF:false, SPOOK:false,
    SPOOL:false, SPOOM:false, SPOON:false, SPOOR:false, SPOOT:false, SPORE:false, SPORK:false, SPORT:false, SPOSH:false, SPOTS:false,
    SPOUT:false, SPRAD:false, SPRAG:false, SPRAT:false, SPRAY:false, SPRED:false, SPREE:false, SPREW:false, SPRIG:false, SPRIT:false,
    SPROD:false, SPROG:false, SPRUE:false, SPRUG:false, SPUDS:false, SPUED:false, SPUER:false, SPUES:false, SPUGS:false, SPULE:false,
    SPUME:false, SPUMY:false, SPUNK:false, SPURN:false, SPURS:false, SPURT:false, SPUTA:false, SPYAL:false, SPYRE:false, SQUAB:false,
    SQUAD:false, SQUAT:false, SQUAW:false, SQUEG:false, SQUIB:false, SQUID:false, SQUIT:false, SQUIZ:false, STABS:false, STACK:false,
    STADE:false, STAFF:false, STAGE:false, STAGS:false, STAGY:false, STAID:false, STAIG:false, STAIN:false, STAIR:false, STAKE:false,
    STALE:false, STALK:false, STALL:false, STAMP:false, STAND:false, STANE:false, STANG:false, STANK:false, STAPH:false, STAPS:false,
    STARE:false, STARK:false, STARN:false, STARR:false, STARS:false, START:false, STASH:false, STATE:false, STATS:false, STAUN:false,
    STAVE:false, STAWS:false, STAYS:false, STEAD:false, STEAK:false, STEAL:false, STEAM:false, STEAN:false, STEAR:false, STEDD:false,
    STEDE:false, STEDS:false, STEED:false, STEEK:false, STEEL:false, STEEM:false, STEEN:false, STEEP:false, STEER:false, STEIL:false,
    STEIN:false, STELA:false, STELE:false, STELL:false, STEME:false, STEMS:false, STEND:false, STENO:false, STENS:false, STENT:false,
    STEPS:false, STEPT:false, STERE:false, STERN:false, STETS:false, STEWS:false, STEWY:false, STEYS:false, STICH:false, STICK:false,
    STIED:false, STIES:false, STIFF:false, STILB:false, STILE:false, STILL:false, STILT:false, STIME:false, STIMS:false, STIMY:false,
    STING:false, STINK:false, STINT:false, STIPA:false, STIPE:false, STIRE:false, STIRK:false, STIRP:false, STIRS:false, STIVE:false,
    STIVY:false, STOAE:false, STOAI:false, STOAS:false, STOAT:false, STOBS:false, STOCK:false, STOEP:false, STOGY:false, STOIC:false,
    STOIT:false, STOKE:false, STOLE:false, STOLN:false, STOMA:false, STOMP:false, STOND:false, STONE:false, STONG:false, STONK:false,
    STONN:false, STONY:false, STOOD:false, STOOK:false, STOOL:false, STOOP:false, STOOR:false, STOPE:false, STOPS:false, STOPT:false,
    STORE:false, STORK:false, STORM:false, STORY:false, STOSS:false, STOTS:false, STOTT:false, STOUN:false, STOUP:false, STOUR:false,
    STOUT:false, STOVE:false, STOWN:false, STOWP:false, STOWS:false, STRAD:false, STRAE:false, STRAG:false, STRAK:false, STRAP:false,
    STRAW:false, STRAY:false, STREP:false, STREW:false, STRIA:false, STRIG:false, STRIM:false, STRIP:false, STROP:false, STROW:false,
    STROY:false, STRUM:false, STRUT:false, STUBS:false, STUCK:false, STUDE:false, STUDS:false, STUDY:false, STUFF:false, STULL:false,
    STULM:false, STUMM:false, STUMP:false, STUMS:false, STUNG:false, STUNK:false, STUNS:false, STUNT:false, STUPA:false, STUPE:false,
    STURE:false, STURT:false, STYED:false, STYES:false, STYLE:false, STYLI:false, STYLO:false, STYME:false, STYMY:false, STYRE:false,
    STYTE:false, SUAVE:false, SUBAH:false, SUBAS:false, SUBBY:false, SUBER:false, SUBHA:false, SUCCI:false, SUCKS:false, SUCKY:false,
    SUCRE:false, SUDDS:false, SUDOR:false, SUDSY:false, SUEDE:false, SUENT:false, SUERS:false, SUETE:false, SUETS:false, SUETY:false,
    SUGAN:false, SUGAR:false, SUGHS:false, SUGOS:false, SUHUR:false, SUIDS:false, SUING:false, SUINT:false, SUITE:false, SUITS:false,
    SUJEE:false, SUKHS:false, SUKUK:false, SULCI:false, SULFA:false, SULFO:false, SULKS:false, SULKY:false, SULLY:false, SULPH:false,
    SULUS:false, SUMAC:false, SUMIS:false, SUMMA:false, SUMOS:false, SUMPH:false, SUMPS:false, SUNIS:false, SUNKS:false, SUNNA:false,
    SUNNS:false, SUNNY:false, SUNUP:false, SUPER:false, SUPES:false, SUPRA:false, SURAH:false, SURAL:false, SURAS:false, SURAT:false,
    SURDS:false, SURED:false, SURER:false, SURES:false, SURFS:false, SURFY:false, SURGE:false, SURGY:false, SURLY:false, SURRA:false,
    SUSED:false, SUSES:false, SUSHI:false, SUSUS:false, SUTOR:false, SUTRA:false, SUTTA:false, SWABS:false, SWACK:false, SWADS:false,
    SWAGE:false, SWAGS:false, SWAIL:false, SWAIN:false, SWALE:false, SWALY:false, SWAMI:false, SWAMP:false, SWAMY:false, SWANG:false,
    SWANK:false, SWANS:false, SWAPS:false, SWAPT:false, SWARD:false, SWARE:false, SWARF:false, SWARM:false, SWART:false, SWASH:false,
    SWATH:false, SWATS:false, SWAYL:false, SWAYS:false, SWEAL:false, SWEAR:false, SWEAT:false, SWEDE:false, SWEED:false, SWEEL:false,
    SWEEP:false, SWEER:false, SWEES:false, SWEET:false, SWEIR:false, SWELL:false, SWELT:false, SWEPT:false, SWERF:false, SWEYS:false,
    SWIES:false, SWIFT:false, SWIGS:false, SWILE:false, SWILL:false, SWIMS:false, SWINE:false, SWING:false, SWINK:false, SWIPE:false,
    SWIRE:false, SWIRL:false, SWISH:false, SWISS:false, SWITH:false, SWITS:false, SWIVE:false, SWIZZ:false, SWOBS:false, SWOLE:false,
    SWOLN:false, SWOON:false, SWOOP:false, SWOPS:false, SWOPT:false, SWORD:false, SWORE:false, SWORN:false, SWOTS:false, SWOUN:false,
    SWUNG:false, SYBBE:false, SYBIL:false, SYBOE:false, SYBOW:false, SYCEE:false, SYCES:false, SYCON:false, SYENS:false, SYKER:false,
    SYKES:false, SYLIS:false, SYLPH:false, SYLVA:false, SYMAR:false, SYNCH:false, SYNCS:false, SYNDS:false, SYNED:false, SYNES:false,
    SYNOD:false, SYNTH:false, SYPED:false, SYPES:false, SYPHS:false, SYRAH:false, SYREN:false, SYRUP:false, SYSOP:false, SYTHE:false,
    SYVER:false, TAALS:false, TAATA:false, TABBY:false, TABER:false, TABES:false, TABID:false, TABIS:false, TABLA:false, TABLE:false,
    TABOO:false, TABOR:false, TABUN:false, TABUS:false, TACAN:false, TACES:false, TACET:false, TACHE:false, TACHO:false, TACHS:false,
    TACIT:false, TACKS:false, TACKY:false, TACOS:false, TACTS:false, TAELS:false, TAFFY:false, TAFIA:false, TAGGY:false, TAGMA:false,
    TAHAS:false, TAHRS:false, TAIGA:false, TAIGS:false, TAIKO:false, TAILS:false, TAINS:false, TAINT:false, TAIRA:false, TAISH:false,
    TAITS:false, TAJES:false, TAKAS:false, TAKEN:false, TAKER:false, TAKES:false, TAKHI:false, TAKIN:false, TAKIS:false, TAKKY:false,
    TALAK:false, TALAQ:false, TALAR:false, TALAS:false, TALCS:false, TALCY:false, TALEA:false, TALER:false, TALES:false, TALKS:false,
    TALKY:false, TALLS:false, TALLY:false, TALMA:false, TALON:false, TALPA:false, TALUK:false, TALUS:false, TAMAL:false, TAMED:false,
    TAMER:false, TAMES:false, TAMIN:false, TAMIS:false, TAMMY:false, TAMPS:false, TANAS:false, TANGA:false, TANGI:false, TANGO:false,
    TANGS:false, TANGY:false, TANHS:false, TANKA:false, TANKS:false, TANKY:false, TANNA:false, TANSY:false, TANTI:false, TANTO:false,
    TANTY:false, TAPAS:false, TAPED:false, TAPEN:false, TAPER:false, TAPES:false, TAPET:false, TAPIR:false, TAPIS:false, TAPPA:false,
    TAPUS:false, TARAS:false, TARDO:false, TARDY:false, TARED:false, TARES:false, TARGA:false, TARGE:false, TARNS:false, TAROC:false,
    TAROK:false, TAROS:false, TAROT:false, TARPS:false, TARRE:false, TARRY:false, TARSI:false, TARTS:false, TARTY:false, TASAR:false,
    TASED:false, TASER:false, TASES:false, TASKS:false, TASSA:false, TASSE:false, TASSO:false, TASTE:false, TASTY:false, TATAR:false,
    TATER:false, TATES:false, TATHS:false, TATIE:false, TATOU:false, TATTS:false, TATTY:false, TATUS:false, TAUBE:false, TAULD:false,
    TAUNT:false, TAUON:false, TAUPE:false, TAUTS:false, TAVAH:false, TAVAS:false, TAVER:false, TAWAI:false, TAWAS:false, TAWED:false,
    TAWER:false, TAWIE:false, TAWNY:false, TAWSE:false, TAWTS:false, TAXED:false, TAXER:false, TAXES:false, TAXIS:false, TAXOL:false,
    TAXON:false, TAXOR:false, TAXUS:false, TAYRA:false, TAZZA:false, TAZZE:false, TEACH:false, TEADE:false, TEADS:false, TEAED:false,
    TEAKS:false, TEALS:false, TEAMS:false, TEARS:false, TEARY:false, TEASE:false, TEATS:false, TEAZE:false, TECHS:false, TECHY:false,
    TECTA:false, TEDDY:false, TEELS:false, TEEMS:false, TEEND:false, TEENE:false, TEENS:false, TEENY:false, TEERS:false, TEETH:false,
    TEFFS:false, TEGGS:false, TEGUA:false, TEGUS:false, TEHRS:false, TEIID:false, TEILS:false, TEIND:false, TEINS:false, TELAE:false,
    TELCO:false, TELES:false, TELEX:false, TELIA:false, TELIC:false, TELLS:false, TELLY:false, TELOI:false, TELOS:false, TEMED:false,
    TEMES:false, TEMPI:false, TEMPO:false, TEMPS:false, TEMPT:false, TEMSE:false, TENCH:false, TENDS:false, TENDU:false, TENES:false,
    TENET:false, TENGE:false, TENIA:false, TENNE:false, TENNO:false, TENNY:false, TENON:false, TENOR:false, TENSE:false, TENTH:false,
    TENTS:false, TENTY:false, TENUE:false, TEPAL:false, TEPAS:false, TEPEE:false, TEPID:false, TEPOY:false, TERAI:false, TERAS:false,
    TERCE:false, TEREK:false, TERES:false, TERFE:false, TERFS:false, TERGA:false, TERMS:false, TERNE:false, TERNS:false, TERRA:false,
    TERRY:false, TERSE:false, TERTS:false, TESLA:false, TESTA:false, TESTE:false, TESTS:false, TESTY:false, TETES:false, TETHS:false,
    TETRA:false, TETRI:false, TEUCH:false, TEUGH:false, TEWED:false, TEWEL:false, TEWIT:false, TEXAS:false, TEXES:false, TEXTS:false,
    THACK:false, THAGI:false, THAIM:false, THALE:false, THALI:false, THANA:false, THANE:false, THANG:false, THANK:false, THANS:false,
    THANX:false, THARM:false, THARS:false, THAWS:false, THAWY:false, THEBE:false, THECA:false, THEED:false, THEEK:false, THEES:false,
    THEFT:false, THEGN:false, THEIC:false, THEIN:false, THEIR:false, THELF:false, THEMA:false, THEME:false, THENS:false, THEOW:false,
    THERE:false, THERM:false, THESE:false, THESP:false, THETA:false, THETE:false, THEWS:false, THEWY:false, THICK:false, THIEF:false,
    THIGH:false, THIGS:false, THILK:false, THILL:false, THINE:false, THING:false, THINK:false, THINS:false, THIOL:false, THIRD:false,
    THIRL:false, THOFT:false, THOLE:false, THOLI:false, THONG:false, THORN:false, THORO:false, THORP:false, THOSE:false, THOUS:false,
    THOWL:false, THRAE:false, THRAW:false, THREE:false, THREW:false, THRID:false, THRIP:false, THROB:false, THROE:false, THROW:false,
    THRUM:false, THUDS:false, THUGS:false, THUJA:false, THUMB:false, THUMP:false, THUNK:false, THURL:false, THUYA:false, THYME:false,
    THYMI:false, THYMY:false, TIANS:false, TIARA:false, TIARS:false, TIBIA:false, TICAL:false, TICCA:false, TICED:false, TICES:false,
    TICHY:false, TICKS:false, TICKY:false, TIDAL:false, TIDDY:false, TIDED:false, TIDES:false, TIERS:false, TIFFS:false, TIFOS:false,
    TIFTS:false, TIGER:false, TIGES:false, TIGHT:false, TIGON:false, TIKAS:false, TIKES:false, TIKIS:false, TIKKA:false, TILAK:false,
    TILDE:false, TILED:false, TILER:false, TILES:false, TILLS:false, TILLY:false, TILTH:false, TILTS:false, TIMBO:false, TIMED:false,
    TIMER:false, TIMES:false, TIMID:false, TIMON:false, TIMPS:false, TINAS:false, TINCT:false, TINDS:false, TINEA:false, TINED:false,
    TINES:false, TINGE:false, TINGS:false, TINKS:false, TINNY:false, TINTS:false, TINTY:false, TIPIS:false, TIPPY:false, TIPSY:false,
    TIRED:false, TIRES:false, TIRLS:false, TIROS:false, TIRRS:false, TITAN:false, TITCH:false, TITER:false, TITHE:false, TITIS:false,
    TITLE:false, TITRE:false, TITTY:false, TITUP:false, TIYIN:false, TIYNS:false, TIZES:false, TIZZY:false, TOADS:false, TOADY:false,
    TOAST:false, TOAZE:false, TOCKS:false, TOCKY:false, TOCOS:false, TODAY:false, TODDE:false, TODDY:false, TOEAS:false, TOFFS:false,
    TOFFY:false, TOFTS:false, TOFUS:false, TOGAE:false, TOGAS:false, TOGED:false, TOGES:false, TOGUE:false, TOHOS:false, TOILE:false,
    TOILS:false, TOING:false, TOISE:false, TOITS:false, TOKAY:false, TOKED:false, TOKEN:false, TOKER:false, TOKES:false, TOKOS:false,
    TOLAN:false, TOLAR:false, TOLAS:false, TOLED:false, TOLES:false, TOLLS:false, TOLLY:false, TOLTS:false, TOLUS:false, TOLYL:false,
    TOMAN:false, TOMBS:false, TOMES:false, TOMIA:false, TOMMY:false, TOMOS:false, TONAL:false, TONDI:false, TONDO:false, TONED:false,
    TONER:false, TONES:false, TONEY:false, TONGA:false, TONGS:false, TONIC:false, TONKA:false, TONKS:false, TONNE:false, TONUS:false,
    TOOLS:false, TOOMS:false, TOONS:false, TOOTH:false, TOOTS:false, TOPAZ:false, TOPED:false, TOPEE:false, TOPEK:false, TOPER:false,
    TOPES:false, TOPHE:false, TOPHI:false, TOPHS:false, TOPIC:false, TOPIS:false, TOPOI:false, TOPOS:false, TOPPY:false, TOQUE:false,
    TORAH:false, TORAN:false, TORAS:false, TORCH:false, TORCS:false, TORES:false, TORIC:false, TORII:false, TOROS:false, TOROT:false,
    TORRS:false, TORSE:false, TORSI:false, TORSK:false, TORSO:false, TORTA:false, TORTE:false, TORTS:false, TORUS:false, TOSAS:false,
    TOSED:false, TOSES:false, TOSHY:false, TOSSY:false, TOTAL:false, TOTED:false, TOTEM:false, TOTER:false, TOTES:false, TOTTY:false,
    TOUCH:false, TOUGH:false, TOUKS:false, TOUNS:false, TOURS:false, TOUSE:false, TOUSY:false, TOUTS:false, TOUZE:false, TOUZY:false,
    TOWED:false, TOWEL:false, TOWER:false, TOWIE:false, TOWNS:false, TOWNY:false, TOWSE:false, TOWSY:false, TOWTS:false, TOWZE:false,
    TOWZY:false, TOXIC:false, TOXIN:false, TOYED:false, TOYER:false, TOYON:false, TOYOS:false, TOZED:false, TOZES:false, TOZIE:false,
    TRABS:false, TRACE:false, TRACK:false, TRACT:false, TRADE:false, TRADS:false, TRAGI:false, TRAIK:false, TRAIL:false, TRAIN:false,
    TRAIT:false, TRAMP:false, TRAMS:false, TRANK:false, TRANQ:false, TRANS:false, TRANT:false, TRAPE:false, TRAPS:false, TRAPT:false,
    TRASH:false, TRASS:false, TRATS:false, TRATT:false, TRAVE:false, TRAWL:false, TRAYF:false, TRAYS:false, TREAD:false, TREAT:false,
    TRECK:false, TREED:false, TREEN:false, TREES:false, TREFA:false, TREIF:false, TREKS:false, TREMA:false, TREMS:false, TREND:false,
    TRESS:false, TREST:false, TRETS:false, TREWS:false, TREYF:false, TREYS:false, TRIAC:false, TRIAD:false, TRIAL:false, TRIBE:false,
    TRICE:false, TRICK:false, TRIDE:false, TRIED:false, TRIER:false, TRIES:false, TRIFF:false, TRIGO:false, TRIGS:false, TRIKE:false,
    TRILD:false, TRILL:false, TRIMS:false, TRINE:false, TRINS:false, TRIOL:false, TRIOR:false, TRIOS:false, TRIPE:false, TRIPS:false,
    TRIPY:false, TRIST:false, TRITE:false, TROAD:false, TROAK:false, TROAT:false, TROCK:false, TRODE:false, TRODS:false, TROGS:false,
    TROIS:false, TROKE:false, TROLL:false, TROMP:false, TRONA:false, TRONC:false, TRONE:false, TRONK:false, TRONS:false, TROOP:false,
    TROOZ:false, TROPE:false, TROTH:false, TROTS:false, TROUT:false, TROVE:false, TROWS:false, TROYS:false, TRUCE:false, TRUCK:false,
    TRUED:false, TRUER:false, TRUES:false, TRUGO:false, TRUGS:false, TRULL:false, TRULY:false, TRUMP:false, TRUNK:false, TRUSS:false,
    TRUST:false, TRUTH:false, TRYER:false, TRYKE:false, TRYMA:false, TRYPS:false, TRYST:false, TSADE:false, TSADI:false, TSARS:false,
    TSKED:false, TSUBA:false, TSUBO:false, TUANS:false, TUART:false, TUATH:false, TUBAE:false, TUBAL:false, TUBAR:false, TUBAS:false,
    TUBBY:false, TUBED:false, TUBER:false, TUBES:false, TUCKS:false, TUFAS:false, TUFFE:false, TUFFS:false, TUFTS:false, TUFTY:false,
    TUGRA:false, TUILE:false, TUINA:false, TUISM:false, TUKTU:false, TULES:false, TULIP:false, TULLE:false, TULPA:false, TULSI:false,
    TUMID:false, TUMMY:false, TUMOR:false, TUMPS:false, TUMPY:false, TUNAS:false, TUNDS:false, TUNED:false, TUNER:false, TUNES:false,
    TUNGS:false, TUNIC:false, TUNNY:false, TUPEK:false, TUPIK:false, TUPLE:false, TUQUE:false, TURBO:false, TURDS:false, TURFS:false,
    TURFY:false, TURKS:false, TURME:false, TURMS:false, TURNS:false, TURNT:false, TURPS:false, TURRS:false, TUSHY:false, TUSKS:false,
    TUSKY:false, TUTEE:false, TUTOR:false, TUTTI:false, TUTTY:false, TUTUS:false, TUXES:false, TUYER:false, TWAES:false, TWAIN:false,
    TWALS:false, TWANG:false, TWANK:false, TWATS:false, TWAYS:false, TWEAK:false, TWEED:false, TWEEL:false, TWEEN:false, TWEEP:false,
    TWEER:false, TWEET:false, TWERK:false, TWERP:false, TWICE:false, TWIER:false, TWIGS:false, TWILL:false, TWILT:false, TWINE:false,
    TWINK:false, TWINS:false, TWINY:false, TWIRE:false, TWIRL:false, TWIRP:false, TWIST:false, TWITE:false, TWITS:false, TWIXT:false,
    TWOER:false, TWYER:false, TYEES:false, TYERS:false, TYING:false, TYIYN:false, TYKES:false, TYLER:false, TYMPS:false, TYNDE:false,
    TYNED:false, TYNES:false, TYPAL:false, TYPED:false, TYPES:false, TYPEY:false, TYPIC:false, TYPOS:false, TYPPS:false, TYPTO:false,
    TYRAN:false, TYRED:false, TYRES:false, TYROS:false, TYTHE:false, TZARS:false, UDALS:false, UDDER:false, UDONS:false, UGALI:false,
    UGGED:false, UHLAN:false, UHURU:false, UKASE:false, ULAMA:false, ULANS:false, ULCER:false, ULEMA:false, ULMIN:false, ULNAD:false,
    ULNAE:false, ULNAR:false, ULNAS:false, ULPAN:false, ULTRA:false, ULVAS:false, ULYIE:false, ULZIE:false, UMAMI:false, UMBEL:false,
    UMBER:false, UMBLE:false, UMBOS:false, UMBRA:false, UMBRE:false, UMIAC:false, UMIAK:false, UMIAQ:false, UMMAH:false, UMMAS:false,
    UMMED:false, UMPED:false, UMPHS:false, UMPIE:false, UMPTY:false, UMRAH:false, UMRAS:false, UNAIS:false, UNAPT:false, UNARM:false,
    UNARY:false, UNAUS:false, UNBAG:false, UNBAN:false, UNBAR:false, UNBED:false, UNBID:false, UNBOX:false, UNCAP:false, UNCES:false,
    UNCIA:false, UNCLE:false, UNCOS:false, UNCOY:false, UNCUS:false, UNCUT:false, UNDAM:false, UNDEE:false, UNDER:false, UNDID:false,
    UNDOS:false, UNDUE:false, UNDUG:false, UNETH:false, UNFED:false, UNFIT:false, UNFIX:false, UNGAG:false, UNGET:false, UNGOD:false,
    UNGOT:false, UNGUM:false, UNHAT:false, UNHIP:false, UNICA:false, UNIFY:false, UNION:false, UNITE:false, UNITS:false, UNITY:false,
    UNJAM:false, UNKED:false, UNKET:false, UNKID:false, UNLAW:false, UNLAY:false, UNLED:false, UNLET:false, UNLID:false, UNLIT:false,
    UNMAN:false, UNMET:false, UNMEW:false, UNMIX:false, UNPAY:false, UNPEG:false, UNPEN:false, UNPIN:false, UNRED:false, UNRID:false,
    UNRIG:false, UNRIP:false, UNSAW:false, UNSAY:false, UNSEE:false, UNSET:false, UNSEW:false, UNSEX:false, UNSOD:false, UNTAX:false,
    UNTIE:false, UNTIL:false, UNTIN:false, UNWED:false, UNWET:false, UNWIT:false, UNWON:false, UNZIP:false, UPBOW:false, UPBYE:false,
    UPDOS:false, UPDRY:false, UPEND:false, UPJET:false, UPLAY:false, UPLED:false, UPLIT:false, UPPED:false, UPPER:false, UPRAN:false,
    UPRUN:false, UPSEE:false, UPSET:false, UPSEY:false, UPTAK:false, UPTER:false, UPTIE:false, URAEI:false, URALI:false, URAOS:false,
    URARE:false, URARI:false, URASE:false, URATE:false, URBAN:false, URBEX:false, URBIA:false, URDEE:false, UREAL:false, UREAS:false,
    UREDO:false, UREIC:false, URENA:false, URENT:false, URGED:false, URGER:false, URGES:false, URIAL:false, URINE:false, URITE:false,
    URMAN:false, URNAL:false, URNED:false, URPED:false, URSAE:false, URSID:false, URSON:false, URUBU:false, URVAS:false, USAGE:false,
    USERS:false, USHER:false, USING:false, USNEA:false, USQUE:false, USUAL:false, USURE:false, USURP:false, USURY:false, UTERI:false,
    UTILE:false, UTTER:false, UVEAL:false, UVEAS:false, UVULA:false, VACUA:false, VADED:false, VADES:false, VAGAL:false, VAGUE:false,
    VAGUS:false, VAILS:false, VAIRE:false, VAIRS:false, VAIRY:false, VAKAS:false, VAKIL:false, VALES:false, VALET:false, VALID:false,
    VALIS:false, VALOR:false, VALSE:false, VALUE:false, VALVE:false, VAMPS:false, VAMPY:false, VANDA:false, VANED:false, VANES:false,
    VANGS:false, VANTS:false, VAPED:false, VAPER:false, VAPES:false, VAPID:false, VAPOR:false, VARAN:false, VARAS:false, VARDY:false,
    VAREC:false, VARES:false, VARIA:false, VARIX:false, VARNA:false, VARUS:false, VARVE:false, VASAL:false, VASES:false, VASTS:false,
    VASTY:false, VATIC:false, VATUS:false, VAUCH:false, VAULT:false, VAUNT:false, VAUTE:false, VAUTS:false, VAWTE:false, VAXES:false,
    VEALE:false, VEALS:false, VEALY:false, VEENA:false, VEEPS:false, VEERS:false, VEERY:false, VEGAN:false, VEGAS:false, VEGES:false,
    VEGIE:false, VEGOS:false, VEHME:false, VEILS:false, VEILY:false, VEINS:false, VEINY:false, VELAR:false, VELDS:false, VELDT:false,
    VELES:false, VELLS:false, VELUM:false, VENAE:false, VENAL:false, VENDS:false, VENDU:false, VENEY:false, VENGE:false, VENIN:false,
    VENOM:false, VENTS:false, VENUE:false, VENUS:false, VERBS:false, VERGE:false, VERRA:false, VERRY:false, VERSE:false, VERSO:false,
    VERST:false, VERTS:false, VERTU:false, VERVE:false, VESPA:false, VESTA:false, VESTS:false, VETCH:false, VEXED:false, VEXER:false,
    VEXES:false, VEXIL:false, VEZIR:false, VIALS:false, VIAND:false, VIBES:false, VIBEX:false, VIBEY:false, VICAR:false, VICED:false,
    VICES:false, VICHY:false, VIDEO:false, VIERS:false, VIEWS:false, VIEWY:false, VIFDA:false, VIFFS:false, VIGAS:false, VIGIA:false,
    VIGIL:false, VIGOR:false, VILDE:false, VILER:false, VILLA:false, VILLI:false, VILLS:false, VIMEN:false, VINAL:false, VINAS:false,
    VINCA:false, VINED:false, VINER:false, VINES:false, VINEW:false, VINIC:false, VINOS:false, VINTS:false, VINYL:false, VIOLA:false,
    VIOLD:false, VIOLS:false, VIPER:false, VIRAL:false, VIRED:false, VIREO:false, VIRES:false, VIRGA:false, VIRGE:false, VIRID:false,
    VIRLS:false, VIRTU:false, VIRUS:false, VISAS:false, VISED:false, VISES:false, VISIE:false, VISIT:false, VISNE:false, VISON:false,
    VISOR:false, VISTA:false, VISTO:false, VITAE:false, VITAL:false, VITAS:false, VITEX:false, VITRO:false, VITTA:false, VIVAS:false,
    VIVAT:false, VIVDA:false, VIVER:false, VIVES:false, VIVID:false, VIXEN:false, VIZIR:false, VIZOR:false, VLEIS:false, VLIES:false,
    VLOGS:false, VOARS:false, VOCAB:false, VOCAL:false, VOCES:false, VODDY:false, VODKA:false, VODOU:false, VODUN:false, VOEMA:false,
    VOGIE:false, VOGUE:false, VOICE:false, VOIDS:false, VOILA:false, VOILE:false, VOIPS:false, VOLAE:false, VOLAR:false, VOLED:false,
    VOLES:false, VOLET:false, VOLKS:false, VOLTA:false, VOLTE:false, VOLTI:false, VOLTS:false, VOLVA:false, VOLVE:false, VOMER:false,
    VOMIT:false, VOTED:false, VOTER:false, VOTES:false, VOUCH:false, VOUGE:false, VOULU:false, VOWED:false, VOWEL:false, VOWER:false,
    VOXEL:false, VOZHD:false, VRAIC:false, VRILS:false, VROOM:false, VROUS:false, VROUW:false, VROWS:false, VUGGS:false, VUGGY:false,
    VUGHS:false, VUGHY:false, VULGO:false, VULNS:false, VULVA:false, VUTTY:false, VYING:false, WAACS:false, WACKE:false, WACKO:false,
    WACKS:false, WACKY:false, WADDS:false, WADDY:false, WADED:false, WADER:false, WADES:false, WADGE:false, WADIS:false, WADTS:false,
    WAFER:false, WAFFS:false, WAFTS:false, WAGED:false, WAGER:false, WAGES:false, WAGGA:false, WAGON:false, WAGYU:false, WAHOO:false,
    WAIDE:false, WAIFS:false, WAIFT:false, WAILS:false, WAINS:false, WAIRS:false, WAIST:false, WAITE:false, WAITS:false, WAIVE:false,
    WAKAS:false, WAKED:false, WAKEN:false, WAKER:false, WAKES:false, WAKFS:false, WALDO:false, WALDS:false, WALED:false, WALER:false,
    WALES:false, WALIE:false, WALIS:false, WALKS:false, WALLA:false, WALLS:false, WALLY:false, WALTY:false, WALTZ:false, WAMED:false,
    WAMES:false, WAMUS:false, WANDS:false, WANED:false, WANES:false, WANEY:false, WANGS:false, WANKS:false, WANKY:false, WANLE:false,
    WANLY:false, WANNA:false, WANTS:false, WANTY:false, WANZE:false, WAQFS:false, WARBS:false, WARBY:false, WARDS:false, WARED:false,
    WARES:false, WAREZ:false, WARKS:false, WARMS:false, WARNS:false, WARPS:false, WARRE:false, WARST:false, WARTS:false, WARTY:false,
    WASES:false, WASHY:false, WASMS:false, WASPS:false, WASPY:false, WASTE:false, WASTS:false, WATAP:false, WATCH:false, WATER:false,
    WATTS:false, WAUFF:false, WAUGH:false, WAUKS:false, WAULK:false, WAULS:false, WAURS:false, WAVED:false, WAVER:false, WAVES:false,
    WAVEY:false, WAWAS:false, WAWES:false, WAWLS:false, WAXED:false, WAXEN:false, WAXER:false, WAXES:false, WAYED:false, WAZIR:false,
    WAZOO:false, WEALD:false, WEALS:false, WEAMB:false, WEANS:false, WEARS:false, WEARY:false, WEAVE:false, WEBBY:false, WEBER:false,
    WECHT:false, WEDEL:false, WEDGE:false, WEDGY:false, WEEDS:false, WEEDY:false, WEEKE:false, WEEKS:false, WEELS:false, WEEMS:false,
    WEENS:false, WEENY:false, WEEPS:false, WEEPY:false, WEEST:false, WEETE:false, WEETS:false, WEFTE:false, WEFTS:false, WEIDS:false,
    WEIGH:false, WEILS:false, WEIRD:false, WEIRS:false, WEISE:false, WEIZE:false, WEKAS:false, WELCH:false, WELDS:false, WELKE:false,
    WELKS:false, WELKT:false, WELLS:false, WELLY:false, WELSH:false, WELTS:false, WEMBS:false, WENCH:false, WENDS:false, WENGE:false,
    WENNY:false, WENTS:false, WEROS:false, WERSH:false, WESTS:false, WETAS:false, WETLY:false, WEXED:false, WEXES:false, WHACK:false,
    WHALE:false, WHAMO:false, WHAMS:false, WHANG:false, WHAPS:false, WHARE:false, WHARF:false, WHATA:false, WHATS:false, WHAUP:false,
    WHAUR:false, WHEAL:false, WHEAR:false, WHEAT:false, WHEEL:false, WHEEN:false, WHEEP:false, WHEFT:false, WHELK:false, WHELM:false,
    WHELP:false, WHENS:false, WHERE:false, WHETS:false, WHEWS:false, WHEYS:false, WHICH:false, WHIDS:false, WHIFF:false, WHIFT:false,
    WHIGS:false, WHILE:false, WHILK:false, WHIMS:false, WHINE:false, WHINS:false, WHINY:false, WHIOS:false, WHIPS:false, WHIPT:false,
    WHIRL:false, WHIRR:false, WHIRS:false, WHISH:false, WHISK:false, WHISS:false, WHIST:false, WHITE:false, WHITS:false, WHITY:false,
    WHIZZ:false, WHOLE:false, WHOMP:false, WHOOF:false, WHOOP:false, WHOOT:false, WHOPS:false, WHORE:false, WHORL:false, WHORT:false,
    WHOSE:false, WHOSO:false, WHOWS:false, WHUMP:false, WHUPS:false, WHYDA:false, WICCA:false, WICKS:false, WICKY:false, WIDDY:false,
    WIDEN:false, WIDER:false, WIDES:false, WIDOW:false, WIDTH:false, WIELD:false, WIELS:false, WIFED:false, WIFES:false, WIFEY:false,
    WIFIE:false, WIFTY:false, WIGAN:false, WIGGA:false, WIGGY:false, WIGHT:false, WIKIS:false, WILCO:false, WILDS:false, WILED:false,
    WILES:false, WILGA:false, WILIS:false, WILJA:false, WILLS:false, WILLY:false, WILTS:false, WIMPS:false, WIMPY:false, WINCE:false,
    WINCH:false, WINDS:false, WINDY:false, WINED:false, WINES:false, WINEY:false, WINGE:false, WINGS:false, WINGY:false, WINKS:false,
    WINNA:false, WINNS:false, WINOS:false, WINZE:false, WIPED:false, WIPER:false, WIPES:false, WIRED:false, WIRER:false, WIRES:false,
    WIRRA:false, WISED:false, WISER:false, WISES:false, WISHA:false, WISHT:false, WISPS:false, WISPY:false, WISTS:false, WITAN:false,
    WITCH:false, WITED:false, WITES:false, WITHE:false, WITHS:false, WITHY:false, WITTY:false, WIVED:false, WIVER:false, WIVES:false,
    WIZEN:false, WIZES:false, WOADS:false, WOALD:false, WOCKS:false, WODGE:false, WOFUL:false, WOJUS:false, WOKEN:false, WOKER:false,
    WOKKA:false, WOLDS:false, WOLFS:false, WOLLY:false, WOLVE:false, WOMAN:false, WOMBS:false, WOMBY:false, WOMEN:false, WOMYN:false,
    WONGA:false, WONGI:false, WONKS:false, WONKY:false, WONTS:false, WOODS:false, WOODY:false, WOOED:false, WOOER:false, WOOFS:false,
    WOOFY:false, WOOLD:false, WOOLS:false, WOOLY:false, WOONS:false, WOOPS:false, WOOPY:false, WOOSE:false, WOOSH:false, WOOTZ:false,
    WOOZY:false, WORDS:false, WORDY:false, WORKS:false, WORLD:false, WORMS:false, WORMY:false, WORRY:false, WORSE:false, WORST:false,
    WORTH:false, WORTS:false, WOULD:false, WOUND:false, WOVEN:false, WOWED:false, WOWEE:false, WOXEN:false, WRACK:false, WRANG:false,
    WRAPS:false, WRAPT:false, WRAST:false, WRATE:false, WRATH:false, WRAWL:false, WREAK:false, WRECK:false, WRENS:false, WREST:false,
    WRICK:false, WRIED:false, WRIER:false, WRIES:false, WRING:false, WRIST:false, WRITE:false, WRITS:false, WROKE:false, WRONG:false,
    WROOT:false, WROTE:false, WROTH:false, WRUNG:false, WRYER:false, WRYLY:false, WUDDY:false, WUDUS:false, WULLS:false, WURST:false,
    WUSES:false, WUSHU:false, WUSSY:false, WUXIA:false, WYLED:false, WYLES:false, WYNDS:false, WYNNS:false, WYTED:false, WYTES:false,
    XEBEC:false, XENIA:false, XENIC:false, XENON:false, XERIC:false, XEROX:false, XERUS:false, XOANA:false, XRAYS:false, XYLAN:false,
    XYLEM:false, XYLIC:false, XYLOL:false, XYLYL:false, XYSTI:false, XYSTS:false, YAARS:false, YABAS:false, YABBA:false, YABBY:false,
    YACCA:false, YACHT:false, YACKA:false, YACKS:false, YAFFS:false, YAGER:false, YAGES:false, YAGIS:false, YAHOO:false, YAIRD:false,
    YAKKA:false, YAKOW:false, YALES:false, YAMEN:false, YAMPY:false, YAMUN:false, YANGS:false, YANKS:false, YAPOK:false, YAPON:false,
    YAPPS:false, YAPPY:false, YARAK:false, YARCO:false, YARDS:false, YARER:false, YARFA:false, YARKS:false, YARNS:false, YARRS:false,
    YARTA:false, YARTO:false, YATES:false, YAUDS:false, YAULD:false, YAUPS:false, YAWED:false, YAWEY:false, YAWLS:false, YAWNS:false,
    YAWNY:false, YAWPS:false, YBORE:false, YCLAD:false, YCLED:false, YCOND:false, YDRAD:false, YDRED:false, YEADS:false, YEAHS:false,
    YEALM:false, YEANS:false, YEARD:false, YEARN:false, YEARS:false, YEAST:false, YECCH:false, YECHS:false, YECHY:false, YEDES:false,
    YEEDS:false, YEESH:false, YEGGS:false, YELKS:false, YELLS:false, YELMS:false, YELPS:false, YELTS:false, YENTA:false, YENTE:false,
    YERBA:false, YERDS:false, YERKS:false, YESES:false, YESKS:false, YESTS:false, YESTY:false, YETIS:false, YETTS:false, YEUKS:false,
    YEUKY:false, YEVEN:false, YEVES:false, YEWEN:false, YEXED:false, YEXES:false, YFERE:false, YIELD:false, YIKED:false, YIKES:false,
    YILLS:false, YINCE:false, YIPES:false, YIPPY:false, YIRDS:false, YIRKS:false, YIRRS:false, YIRTH:false, YITES:false, YITIE:false,
    YLEMS:false, YLIKE:false, YLKES:false, YMOLT:false, YMPES:false, YOBBO:false, YOBBY:false, YOCKS:false, YODEL:false, YODHS:false,
    YODLE:false, YOGAS:false, YOGEE:false, YOGHS:false, YOGIC:false, YOGIN:false, YOGIS:false, YOICK:false, YOJAN:false, YOKED:false,
    YOKEL:false, YOKER:false, YOKES:false, YOKUL:false, YOLKS:false, YOLKY:false, YOMIM:false, YOMPS:false, YONIC:false, YONIS:false,
    YONKS:false, YOOFS:false, YOOPS:false, YORES:false, YORKS:false, YORPS:false, YOUKS:false, YOUNG:false, YOURN:false, YOURS:false,
    YOURT:false, YOUSE:false, YOUTH:false, YOWED:false, YOWES:false, YOWIE:false, YOWLS:false, YOWZA:false, YRAPT:false, YRENT:false,
    YRIVD:false, YRNEH:false, YSAME:false, YTOST:false, YUANS:false, YUCAS:false, YUCCA:false, YUCCH:false, YUCKO:false, YUCKS:false,
    YUCKY:false, YUFTS:false, YUGAS:false, YUKED:false, YUKES:false, YUKKY:false, YUKOS:false, YULAN:false, YULES:false, YUMMO:false,
    YUMMY:false, YUMPS:false, YUPON:false, YUPPY:false, YURTA:false, YURTS:false, YUZUS:false, ZABRA:false, ZACKS:false, ZAIDA:false,
    ZAIDY:false, ZAIRE:false, ZAKAT:false, ZAMAN:false, ZAMBO:false, ZAMIA:false, ZANJA:false, ZANTE:false, ZANZA:false, ZANZE:false,
    ZAPPY:false, ZARFS:false, ZARIS:false, ZATIS:false, ZAXES:false, ZAYIN:false, ZAZEN:false, ZEALS:false, ZEBEC:false, ZEBRA:false,
    ZEBUB:false, ZEBUS:false, ZEDAS:false, ZEINS:false, ZENDO:false, ZERDA:false, ZERKS:false, ZEROS:false, ZESTS:false, ZESTY:false,
    ZETAS:false, ZEXES:false, ZEZES:false, ZHOMO:false, ZIBET:false, ZIFFS:false, ZIGAN:false, ZILAS:false, ZILCH:false, ZILLA:false,
    ZILLS:false, ZIMBI:false, ZIMBS:false, ZINCO:false, ZINCS:false, ZINCY:false, ZINEB:false, ZINES:false, ZINGS:false, ZINGY:false,
    ZINKE:false, ZINKY:false, ZIPPO:false, ZIPPY:false, ZIRAM:false, ZITIS:false, ZIZEL:false, ZIZIT:false, ZLOTE:false, ZLOTY:false,
    ZOAEA:false, ZOBOS:false, ZOBUS:false, ZOCCO:false, ZOEAE:false, ZOEAL:false, ZOEAS:false, ZOISM:false, ZOIST:false, ZOMBI:false,
    ZONAE:false, ZONAL:false, ZONDA:false, ZONED:false, ZONER:false, ZONES:false, ZONKS:false, ZOOEA:false, ZOOEY:false, ZOOID:false,
    ZOOKS:false, ZOOMS:false, ZOONS:false, ZOOTY:false, ZOPPA:false, ZOPPO:false, ZORIL:false, ZORIS:false, ZORRO:false, ZOUKS:false,
    ZOWEE:false, ZOWIE:false, ZULUS:false, ZUPAN:false, ZUPAS:false, ZUPPA:false, ZURFS:false, ZUZIM:false, ZYGAL:false, ZYGON:false,
    ZYMES:false, ZYMIC:false,
}

const CDate = new Date()

// If it is a new day, reset game.
if (localStorage.getItem("date") !== CDate.getDate().toString()){
    
    localStorage.setItem("date", CDate.getDate())

    localStorage.setItem("gameIsOver", false)
    localStorage.setItem("currentAnswerIndex", 0)
    localStorage.setItem("currentRow", 0)
    localStorage.setItem("remainingGuesses", 20)
    localStorage.setItem("dailyPoints", 0)

    // Clears board-state
    for (let r = 1; r <= 6; r++){
        for (let c = 1; c <= 5; c++){
            localStorage.setItem(`BoardSquare(${r},${c})`, "")
            localStorage.setItem(`BoardSquare(${r},${c})-color`, "")
        }
    }

    // Clears displayed words in rundown modal
    for (let i = 1; i <= 6; i++){
    localStorage.setItem(`rundownWord(${i})`, "")
    localStorage.setItem(`rundownWord(${i})-data-guessed`, "")
    localStorage.setItem(`rundownPoints(${i})`, "")
    }

    // If this is the first time loading the game...
    if (localStorage.getItem("totalDaysPlayed") == null){
        localStorage.setItem("totalDaysPlayed", 0)
        localStorage.setItem("calcAvgDaily", 0)
        localStorage.setItem("calcAvgWeekly", 0)
        showRules()
    }
    
    // If this is a new week...
    const SDate = new Date('2022, 0, 2')
    
    if (localStorage.getItem("week") !== (Math.floor((CDate - SDate) / 604800000))){

        localStorage.setItem("week", Math.floor((CDate - SDate) / 604800000))

        localStorage.setItem("totalPoints", "0")

        for (let i = 1; i <= 7; i++){
            localStorage.setItem(`Day(${i})Points`, "")
        }

        localStorage.setItem("displayAvgWeekly", localStorage.getItem("calcAvgWeekly"))
        localStorage.setItem("calcAvgWeekly", 0)
        localStorage.setItem("playedThisWeek", false)
        localStorage.setItem("daysPlayedThisWeek", 0)
    }

    localStorage.setItem("playedToday", false)
    localStorage.setItem("displayAvgDaily", localStorage.getItem("calcAvgDaily"))
    localStorage.setItem("calcAvgDaily", 0)
}

// Defines and defaults board values as blank:
const boardRows = [
    ['', '', '', '', ''],
    ['', '', '', '', ''],
    ['', '', '', '', ''],
    ['', '', '', '', ''],
    ['', '', '', '', ''],
    ['', '', '', '', '']
]

generateGame()

let gameIsOver = localStorage.getItem("gameIsOver")
let currentAnswerIndex = parseInt(localStorage.getItem("currentAnswerIndex"))
let currentRow = parseInt(localStorage.getItem("currentRow"))
let currentSquare = 0
let remainingGuesses = parseInt(localStorage.getItem("remainingGuesses"))
statsButton.textContent = remainingGuesses
rundownTurns.textContent = remainingGuesses
let dailyPoints = parseInt(localStorage.getItem("dailyPoints"))
let totalPoints =  parseInt(localStorage.getItem("totalPoints"))
let calcAvgDaily = parseInt(localStorage.getItem("calcAvgDaily"))
let displayAvgDaily = parseInt(localStorage.getItem("displayAvgDaily"))
let totalDaysPlayed = parseInt(localStorage.getItem("totalDaysPlayed"))
let playedToday = localStorage.getItem("playedToday")
let daysPlayedThisWeek = parseInt(localStorage.getItem("daysPlayedThisWeek"))
let playedThisWeek = localStorage.getItem("playedThisWeek")


setGamestateDisplay()

// Creates gameboard, keyboard, and other display items for the game:
function generateGame(){
    // Creates the board's squares:
    boardRows.forEach((row, rowIndex) =>{
        const boardRow = document.createElement('div')
        boardRow.setAttribute('id', `Row(${rowIndex + 1})`)
        boardRow.classList.add('boardRow')
        row.forEach((_c, squareIndex) =>{
            const boardSquare = document.createElement('div')
            boardSquare.setAttribute('id', `BoardSquare(${rowIndex + 1},${squareIndex + 1})`)
            boardSquare.classList.add('boardSquare')
            boardRow.append(boardSquare)
        })
        board.append(boardRow)
    })

    // Creates the buttons for the keyboard:
    keyboardKeys.forEach((row, rowIndex) =>{
        const keyRow = document.createElement('div')
        keyRow.setAttribute('id', `Keyboard Row(${rowIndex + 1})`)
        keyRow.classList.add('keyRow')
        row.forEach(key => {
            if (key !== 'SPACER'){
                const keyboardKey = document.createElement('button')
                keyboardKey.setAttribute('id', key)
                keyboardKey.addEventListener('click', () => injectKey(key))
                
                if (key == 'BACKSPACE'){
                    keyboardKey.textContent = '⌦'
                }
                
                else{
                keyboardKey.textContent = key
                }
                keyRow.append(keyboardKey)
            }
            else{
                const spacer = document.createElement('div')
                spacer.classList.add('spacer')
                keyRow.append(spacer)
            }
        })
        keyboard.append(keyRow)
    /*'ᗏ⟪⋘⊠⇍⌫⌦⦴⏎⮑⮐'␡*/
    })

    // Allows typing on physical keyboard and filters for valid inputs:
    document.addEventListener('keydown', (l) =>{
        let typedKey = l.key.toUpperCase()
        if (typedKey === 'ENTER' ||
            typedKey === 'BACKSPACE' ||
            (typedKey.length == 1 &&
            /[A-Z]/.test(typedKey))){
                injectKey(typedKey)
            }
    })

    // Allows buttons within the banner to open their modals
    rulesButton.addEventListener('click', () => showRules())
    statsButton.addEventListener('click', () => showStats())

    // Creates slots for missed and guessed words and gained points in rundown modal:
    for (let i = 1; i <= 6; i++){
        let word = document.createElement('dailyWord')
        word.setAttribute('id', `Word(${i})`)
        displayedWords.append(word)

        let point = document.createElement('dailyPoints')
        point.setAttribute('id', `Points(${i})`)
        gainedPoints.append(point)
    }

    totalThisWeek = document.createElement('div')
    totalThisWeek.setAttribute('id', 'TotalPoints')
    totalPointContainer.append(totalThisWeek)

    daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

    daysOfWeek.forEach(d =>{
        let day = document.createElement('day')
        day.textContent = d
        days.append(day)
    })

    for (let i = 1; i <= 7; i++){
        let points = document.createElement('pointsForDay')
        points.setAttribute('id', `Day(${i})Points`)
        points.textContent = localStorage.getItem(`Day(${i})Points`)
        pointsOfWeek.append(points)

        if ((i - 1) === CDate.getDay()){
            todaysPoints = document.getElementById(`Day(${i})Points`)
            todaysPoints.classList.add('today')
        }
    }
}

function setGamestateDisplay(){

    for (let r = 1; r <= 6; r++){
        for (let c = 1; c <= 5; c++){
            let boardSquare = document.getElementById(`BoardSquare(${r},${c})`)
            boardSquare.textContent = localStorage.getItem(`BoardSquare(${r},${c})`)
            if (localStorage.getItem(`BoardSquare(${r},${c})-color`) !== ""){
                boardSquare.classList.add(localStorage.getItem(`BoardSquare(${r},${c})-color`))
            }
        }
    }

    for (let i = 1; i <= 6; i++){
        let word = document.getElementById(`Word(${i})`)
        word.textContent = localStorage.getItem(`rundownWord(${i})`)
        word.setAttribute('data-guessed', localStorage.getItem(`rundownWord(${i})-data-guessed`))

        let points = document.getElementById(`Points(${i})`)
        points.textContent = localStorage.getItem(`rundownPoints(${i})`)
        }

    let totalDisplay = document.getElementById('TotalPoints')
    totalDisplay.textContent = totalPoints

    let statsDailyAvg = document.getElementById('DailyAverage')
    statsDailyAvg.textContent = localStorage.getItem("displayAvgDaily")

    let statsWeekyAvg = document.getElementById('WeeklyAverage')
    statsWeekyAvg.textContent = localStorage.getItem("displayAvgWeekly")
}

// Handles inputs from keystrokes / buttons:
function injectKey(key){
    if (gameIsOver === "false"){
        if (key === 'BACKSPACE') {
            if (currentSquare > 0) {
                backspace()
            }
        }
        else if (key === 'ENTER') {
            const guess = boardRows[currentRow].join('')

            if (currentSquare === 5 && typeof allWords[ guess ] != 'undefined'){
                submitGuess(guess)
            }
            else{
                const row = document.getElementById('Row(' + (currentRow+1) + ')')
                row.classList.add('shake')
                
                setTimeout(() => {
                    row.classList.remove('shake')
                }, 600);
            }
        }
        else if (currentSquare < 5) {
            typeLetter(key)
        }
    }
}

function typeLetter(letter){
    let boardSquare = document.getElementById(`BoardSquare(${currentRow + 1},${currentSquare + 1})`)
    boardRows[currentRow][currentSquare] = letter
    boardSquare.textContent = letter
    boardSquare.setAttribute('data', letter)
    boardSquare.classList.add('filled')
    boardSquare.setAttribute('data-animation', 'filled')
    currentSquare++
}

function backspace(){
    currentSquare--
    const boardSquare = document.getElementById('BoardSquare(' + (currentRow+1) + ',' + (currentSquare+1) + ')')
    boardRows[currentRow][currentSquare] = ''
    boardSquare.textContent = ''
    boardSquare.setAttribute('data', '')
    boardSquare.classList.remove('filled')
}

function submitGuess(guess){
    localStorage.setItem("remainingGuesses", --remainingGuesses)
    statsButton.textContent = remainingGuesses
    rundownTurns.textContent = remainingGuesses
      

    if (guess != answers[currentAnswerIndex]){
        wrongGuess()
    }
    else{
        correctGuess()
    }

    if (remainingGuesses === 0){
        gameOver()
    }

    if (playedToday === "false"){
        playedToday = true
        localStorage.setItem("playedToday", "true")

        totalDaysPlayed++
        localStorage.setItem("totalDaysPlayed", totalDaysPlayed)

        daysPlayedThisWeek++
        localStorage.setItem("daysPlayedThisWeek", daysPlayedThisWeek)

        if (daysPlayedThisWeek >= 4 && playedThisWeek === "false"){
            playedThisWeek = "true"
            localStorage.setItem("playedThisWeek", true)
        }
    }
}

function wrongGuess(){
    showHint()

    if (currentRow < 5){
        currentSquare = 0
        currentRow++
        localStorage.setItem("currentRow", currentRow)
    }
    else{
        if (currentAnswerIndex < 5){
            const rundownWord = document.getElementById('Word(' + (currentAnswerIndex+1) + ')')
            rundownWord.textContent = answers[currentAnswerIndex]
            localStorage.setItem("rundownWord", answers[currentAnswerIndex])
            rundownWord.setAttribute('data-guessed', 'missed')
            localStorage.setItem("rundownWord-guessed", "missed")

            setTimeout(() => {
                transition()
            }, 1600)
        }
        else{
            gameOver()
        }
    }
}

function correctGuess(){
    const boardSquares = document.getElementById('Row(' + (currentRow+1) + ')').childNodes

    addPoints()

    showHint()

    //Wave animation
    boardSquares.forEach((boardSquare, letterIndex) =>{
        setTimeout(() => {
            boardSquare.setAttribute('data-animation', '')
            boardSquare.classList.add('correct')
        }, (1500 + (100 * letterIndex)))
    })

    setTimeout(() => {
        updatePoints()
    }, 1600)
 
    if (currentAnswerIndex < 5){
        setTimeout(() => {
            transition()
        }, 2500);
    }
    
    else{
        gameOver()
    }
}

function addPoints(){
    let pointsGranted = [5, 5, 5, 4, 3, 2]

    dailyPoints = dailyPoints + pointsGranted[currentRow]
    console.log(dailyPoints)
    totalPoints = totalPoints + pointsGranted[currentRow]
    localStorage.setItem("dailyPoints", dailyPoints)
    localStorage.setItem("totalPoints", totalPoints)

    avgDaily()

    localStorage.setItem(`rundownWord(${currentAnswerIndex + 1})`, answers[currentAnswerIndex])
    localStorage.setItem(`rundownWord(${currentAnswerIndex + 1})-data-guessed`, "correct")
    localStorage.setItem(`rundownPoints(${currentAnswerIndex + 1})`, `+${pointsGranted[currentRow]}`)
    localStorage.setItem(`Day(${CDate.getDay()+1})Points`, dailyPoints)
}

function updatePoints(){
    let rundownWord = document.getElementById('Word(' + (currentAnswerIndex+1) + ')')
    let rundownPoints = document.getElementById('Points(' + (currentAnswerIndex+1) + ')')
    let todaysPoints = document.getElementById(`Day(${CDate.getDay()+1})Points`)
    let totalDisplay = document.getElementById('TotalPoints')

    rundownWord.textContent = answers[currentAnswerIndex]
    rundownWord.setAttribute('data-guessed', 'correct')
    rundownPoints.textContent = localStorage.getItem(`rundownPoints(${currentAnswerIndex + 1})`)
    todaysPoints.textContent = dailyPoints
    totalDisplay.textContent = totalPoints
}

function showHint(){
    let currentAnswer = answers[currentAnswerIndex]
    const guessWord = boardRows[currentRow]
    const boardSquares = document.getElementById(`Row(${currentRow + 1})`).childNodes

    guessWord.forEach((boardSquare, letterIndex) => {
        guessWord[letterIndex] = { letter: boardSquare, hint: 'grayHint' }
    })

    guessWord.forEach((boardSquare, letterIndex) => {
        if (boardSquare.letter == currentAnswer[letterIndex]) {
            currentAnswer = currentAnswer.replace(boardSquare.letter, '_')
            boardSquare.hint = 'greenHint'
        }
    })

    guessWord.forEach((boardSquare) => {
        if (currentAnswer.includes(boardSquare.letter)) {
            currentAnswer = currentAnswer.replace(boardSquare.letter, '_')
            boardSquare.hint = 'yellowHint'
        }
    })

    boardSquares.forEach((boardSquare, i) => {
        localStorage.setItem(`BoardSquare(${currentRow + 1},${i+1})`, boardSquare.textContent)
        // boardSquare.setAttribute('data', letter)
        localStorage.setItem(`BoardSquare(${currentRow + 1},${i+1})-color`, guessWord[i].hint)
        // maybe store key hint
    })

    boardSquares.forEach((boardSquare, letterIndex) => {
        setTimeout(() => {
            boardSquare.setAttribute('data-animation', 'flip1')
            setTimeout(() => {
                boardSquare.classList.remove('filled')
                boardSquare.setAttribute('data-animation', 'flip2')
                boardSquare.classList.add(guessWord[letterIndex].hint)
                const key = document.getElementById(guessWord[letterIndex].letter)
                key.classList.add(guessWord[letterIndex].hint)
            }, 250)
        }, 250 * letterIndex)
    })
}

function transition(){
    currentRow = 0
    currentSquare = 0
    currentAnswerIndex++

    localStorage.setItem("currentRow", 0)
    localStorage.setItem("currentAnswerIndex", currentAnswerIndex)
    for (let r = 1; r <= 6; r++){
        for (let c = 1; c <= 5; c++){
            localStorage.setItem(`BoardSquare(${r},${c})`, "")
            localStorage.setItem(`BoardSquare(${r},${c})-color`, "")
        }
    }

    for (let c = 5; c > 0; c--){
        setTimeout(() => {
            const column = [
                document.getElementById('BoardSquare(1,' + c + ')'),
                document.getElementById('BoardSquare(2,' + c + ')'),
                document.getElementById('BoardSquare(3,' + c + ')'),
                document.getElementById('BoardSquare(4,' + c + ')'),
                document.getElementById('BoardSquare(5,' + c + ')'),
                document.getElementById('BoardSquare(6,' + c + ')')
            ]
    
            column.forEach(boardSquare =>{
                boardSquare.classList.remove('correct')
                boardSquare.setAttribute('data-animation', 'transition1')

                setTimeout(() => {
                    boardSquare.textContent = ''
                    boardSquare.setAttribute('data', '')
                    boardSquare.classList.remove('greenHint')
                    boardSquare.classList.remove('yellowHint')
                    boardSquare.classList.remove('grayHint')
                    boardSquare.setAttribute('data-animation', 'transition2')
                    setTimeout(() => {
                        boardSquare.setAttribute('data-animation', '')
                    }, 251);
                }, 250)
            })
        }, (1250 - (250 * c)))
    }

    keyboardKeys.forEach((_r, row) => {
        const keyRow = keyboardKeys[row]
        keyRow.forEach(letter => {
            const key = document.getElementById(letter)
            if (letter !== 'SPACER'){
                key.classList.remove('greenHint')
                key.classList.remove('yellowHint')
                key.classList.remove('grayHint')
            }
        })
    })
}

function gameOver(){
    gameIsOver = true
    localStorage.setItem("gameIsOver", true)

    dailyPoints = dailyPoints + remainingGuesses
    localStorage.setItem("dailyPoints", dailyPoints)
    remainingGuesses = 0
    localStorage.setItem("remainingGuesses", remainingGuesses)
    statsButton.textContent = remainingGuesses
    rundownTurns.textContent = remainingGuesses

    UpdateAverages()

    setTimeout(() => {
        keyboardKeys.forEach((_r, row) => {
            const keyRow = keyboardKeys[row]
            keyRow.forEach(letter => {
                const key = document.getElementById(letter)
                if (letter !== 'SPACER'){
                    key.classList.remove('greenHint')
                    key.classList.remove('yellowHint')
                    key.classList.add('grayHint')
                }
            })
        })
    }, 1600)

    setTimeout(() => {
        showStats()
    }, 2500);
}

function showRules(){
    const overlay = document.getElementById('overlay')
    const rules = document.getElementById('rulesContainer')
    overlay.setAttribute('data-viewable', 'yes')
    rules.setAttribute('data-viewable', 'yes')
    overlay.addEventListener('click', () =>{
        overlay.setAttribute('data-viewable', 'closing')
        rules.setAttribute('data-viewable', 'closing')
        setTimeout(() => {
            overlay.setAttribute('data-viewable', 'no')
            rules.setAttribute('data-viewable', 'no') 
        }, 199)
    }, {once: true})
}

function showStats(){
    const overlay = document.getElementById('overlay')
    const stats = document.getElementById('statsContainer')
    overlay.setAttribute('data-viewable', 'yes')
    stats.setAttribute('data-viewable', 'yes')
    overlay.addEventListener('click', () =>{
        overlay.setAttribute('data-viewable', 'closing')
        stats.setAttribute('data-viewable', 'closing')
        setTimeout(() => {
            overlay.setAttribute('data-viewable', 'no')
            stats.setAttribute('data-viewable', 'no') 
        }, 199)
    }, {once: true})
}

function avgDaily(){
    calcAvgDaily = Math.ceil(((((totalDaysPlayed - 1) * displayAvgDaily) + dailyPoints) / totalDaysPlayed) * 100) / 100
    console.log(calcAvgDaily)
    localStorage.setItem("calcAvgDaily", calcAvgDaily)

    if (playedThisWeek === "true"){
        calcAvgWeekly = Math.ceil(((((totalWeeksPlayed - 1) * displayAvgWeekly) + totalPoints) / totalWeeksPlayed) * 100) / 100
        localStorage.setItem("calcAvgDaily", calcAvgDaily)
    }
}

function UpdateAverages(){
    localStorage.setItem("displayAvgDaily", calcAvgDaily)

    let statsDailyAvg = document.getElementById('DailyAverage')
    statsDailyAvg.textContent = calcAvgDaily

    if (CDate.getDay() == 6){
        localStorage.setItem("displayAvgWeekly", calcAvgWeekly)

        let statsWeekyAvg = document.getElementById('WeeklyAverage')
        statsWeekyAvg.textContent = calcAvgWeekly
    }
}